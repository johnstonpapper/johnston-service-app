﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace JohnstonApi.Controllers
{
    public class AdminServiceController : ApiController
    {
        string adminCS = ConfigurationManager.ConnectionStrings["ApiConntectionAzure"].ConnectionString;

        // GET: api/AdminService
        public string Get()
        {
            var completedAuditToday = DateTime.Now.Date.AddDays(-1);
            DataTable dt = new DataTable();
            var serviceAuditCollection = $"SELECT [DrainAudit].[CustNo], [Customers].[AccountName], [DrainAudit].[WorkOrder], [DrainAudit].[Visit], [DrainAudit].[AuditTech], 'Drain Audit' as 'AuditType' ,[DrainAudit].[AuditCreation], [DrainAudit].[AuditPdf] FROM Customers INNER JOIN DrainAudit ON [Customers].CustNo = [DrainAudit].[CustNo] INNER JOIN [ServiceTechs] ON [DrainAudit].[AuditTech] = [ServiceTechs].[Name] WHERE Convert(date, [AuditCreation]) = '{completedAuditToday}' UNION ALL " +
                $"SELECT [LaundryAudit].[CustNo], [Customers].[AccountName], [LaundryAudit].[WorkOrder], [LaundryAudit].[Visit], [LaundryAudit].[AuditTech], 'Laundry Audit' as 'AuditType', [LaundryAudit].[AuditCreation], [LaundryAudit].[AuditPdf] FROM [Customers] INNER JOIN [LaundryAudit] ON [Customers].[CustNo] = [LaundryAudit].[CustNo] INNER JOIN [ServiceTechs] ON [LaundryAudit].[AuditTech] = [ServiceTechs].[Name] WHERE Convert(date, [AuditCreation]) = '{completedAuditToday}' UNION ALL " +
                $"SELECT [WarewashAudit].[CustNo], [Customers].[AccountName], [WarewashAudit].[WorkOrder], [WarewashAudit].[Visit], [WarewashAudit].[AuditTech], 'Warewash Audit' as 'AuditType', [WarewashAudit].[AuditCreation], [WarewashAudit].[AuditPdf] FROM [Customers] INNER JOIN [WarewashAudit] ON [Customers].[CustNo] = [WarewashAudit].[CustNo] INNER JOIN [ServiceTechs] ON [WarewashAudit].[AuditTech] = [ServiceTechs].[Name] WHERE Convert(date, [AuditCreation])  = '{completedAuditToday}'";
            using (SqlConnection connection = new SqlConnection(adminCS))
            {
                try
                {
                    connection.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(serviceAuditCollection, connection))
                    {
                        da.Fill(dt);
                    }

                    serviceAuditCollection = JsonConvert.SerializeObject(dt, Formatting.Indented);
                } catch (Exception ex)
                {
                    serviceAuditCollection = ex.Message;
                }
                
            }
            return serviceAuditCollection;
        }

        // GET: api/AdminService/5
        public string Get(string name)
        {
            DataTable dt = new DataTable();
            var serviceAuditCollection = $"SELECT [DrainAudit].[CustNo], [Customers].[AccountName], [DrainAudit].[WorkOrder], [DrainAudit].[AuditTech], 'Drain Audit' as 'AuditType' ,[DrainAudit].[AuditCreation], [DrainAudit].[AuditPdf] FROM Customers INNER JOIN DrainAudit ON [Customers].CustNo = [DrainAudit].[CustNo] INNER JOIN [ServiceTechs] ON [DrainAudit].[AuditTech] = [ServiceTechs].[Name] WHERE TechId = '{name}' UNION ALL " +
                $"SELECT [LaundryAudit].[CustNo], [Customers].[AccountName], [LaundryAudit].[WorkOrder], [LaundryAudit].[AuditTech], 'Laundry Audit' as 'AuditType', [LaundryAudit].[AuditCreation], [LaundryAudit].[AuditPdf] FROM[Customers] INNER JOIN[LaundryAudit] ON[Customers].[CustNo] = [LaundryAudit].[CustNo] INNER JOIN[ServiceTechs] ON[LaundryAudit].[AuditTech] = [ServiceTechs].[Name] WHERE TechId = '{name}' UNION ALL " +
                $"SELECT [WarewashAudit].[CustNo], [Customers].[AccountName], [WarewashAudit].[WorkOrder], [WarewashAudit].[AuditTech], 'Warewash Audit' as 'AuditType', [WarewashAudit].[AuditCreation], [WarewashAudit].[AuditPdf] FROM[Customers] INNER JOIN[WarewashAudit] ON[Customers].[CustNo] = [WarewashAudit].[CustNo] INNER JOIN[ServiceTechs] ON[WarewashAudit].[AuditTech] = [ServiceTechs].[Name] WHERE TechId = '{name}' ORDER BY AuditCreation DESC  ";
            using (SqlConnection connection = new SqlConnection(adminCS))
            {
                try
                {
                    connection.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(serviceAuditCollection, connection))
                    {
                        da.Fill(dt);
                    }

                    serviceAuditCollection = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }
                catch (Exception ex)
                {
                    serviceAuditCollection = ex.Message;
                }
            }
            return serviceAuditCollection;
        }
        
        

        // POST: api/AdminService
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/AdminService/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/AdminService/5
        public void Delete(int id)
        {
        }
    }
}
