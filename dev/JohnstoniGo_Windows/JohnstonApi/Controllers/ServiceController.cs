﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JohnstonApi.Controllers
{
    public class ServiceController : ApiController
    {
        string pegasusDB = ConfigurationManager.ConnectionStrings["ApiConntectionAzure"].ConnectionString;
        //string pegasusDB = ConfigurationManager.ConnectionStrings["ApiInternalConntection"].ConnectionString;

        // GET: api/JohnstonService/5
        public string GetSqlContent(string name)
        {
            var serviceContent = $"SELECT * FROM {name}";
            DataTable dt = new DataTable();

            var sqlResponse = string.Empty;
            using (SqlConnection connection = new SqlConnection(pegasusDB))
            {
                connection.Open();
                try
                {
                    //DataSet ds = new DataSet();
                    using (SqlDataAdapter da = new SqlDataAdapter(serviceContent, connection))
                    {
                        da.Fill(dt);
                    }

                    sqlResponse = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }
                catch (Exception ex)
                {
                    sqlResponse = ex.Message;
                }
            }

            return sqlResponse;
        }

        // POST: api/JohnstonService
        public string PostServiceUpdate(string name, [FromBody] JToken jsonCollection)
        {
            var sqlJsonResponse = string.Empty;
            var commaDelimited = string.Empty;
            var sqlParamaters = string.Empty;
            var responseMsg = string.Empty;

            var recordExist = $"SELECT * FROM [JP_Service].[dbo].{name}";

            StringBuilder sb = new StringBuilder();
            List<string> sqlSyntax = new List<string>();
            List<string> sqlParams = new List<string>();
            DataTable dt = new DataTable();
            
            using (SqlConnection connection = new SqlConnection(pegasusDB))
            {
                try
                {
                    connection.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(recordExist, connection))
                    {
                        da.Fill(dt);
                    }
                    var classObj = $"JohnstonApi.{name}";
                    Type type = Type.GetType(classObj, true);
                    var objName = Activator.CreateInstance(type);

                    foreach (var c in objName.GetType().GetProperties())
                    {
                        sqlSyntax.Add($"{c.Name}");
                        sqlParams.Add($"@{c.Name}");
                    }
                    sqlSyntax.RemoveAt(0);
                    sqlParams.RemoveAt(0);
                    commaDelimited = string.Join(",", sqlSyntax);
                    sqlParamaters = string.Join(",", sqlParams);
                    sb.Append($"INSERT INTO {name} ({commaDelimited}) VALUES ({sqlParamaters})");

                    switch (name)
                    {
                        case "Customers":
                            var jsonParsedCust = jsonCollection.ToObject<Customers[]>();
                            foreach (var parsedJson in jsonParsedCust)
                            {
                                sqlParamaters = $"UPDATE [dbo].[{name}] SET [Nickname] = @Nickname WHERE CustNo = '{parsedJson.CustNo}' ";

                                using (SqlCommand command = new SqlCommand(sqlParamaters, connection))
                                {
                                    command.Parameters.AddWithValue("@Nickname", parsedJson.Nickname);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                            break;
                        case "Contacts":
                            var jsonParsedContact = jsonCollection.ToObject<Contacts[]>();
                            
                            foreach (var parsedJson in jsonParsedContact)
                            {
                                DataRow[] nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND Contact = '{parsedJson.Contact}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@Contact", parsedJson.Contact);
                                        command.Parameters.AddWithValue("@Email", parsedJson.Email);
                                        command.Parameters.AddWithValue("@Department", parsedJson.Department);
                                        command.Parameters.AddWithValue("@Title", parsedJson.Title);
                                        command.Parameters.AddWithValue("@Phone", parsedJson.Phone);
                                        command.Parameters.AddWithValue("@ChemicalType", parsedJson.ChemicalType);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "MachineMedia":
                            var jsonParsedMedia = jsonCollection.ToObject<MachineMedia[]>();

                            foreach (var parsedJson in jsonParsedMedia)
                            {
                                DataRow[] nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND SerialNo = '{parsedJson.SerialNo}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@MachineNo", parsedJson.MachineNo);
                                        command.Parameters.AddWithValue("@Name", parsedJson.Name);
                                        command.Parameters.AddWithValue("@MediaType", parsedJson.MediaType);
                                        command.Parameters.AddWithValue("@Creation", parsedJson.Creation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "DrainAudit":
                            var jsonParsedDrain = jsonCollection.ToObject<DrainAudit[]>();

                            foreach (var parsedJson in jsonParsedDrain)
                            {
                                DataRow[] nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND WorkOrder = '{parsedJson.WorkOrder}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@WorkOrder", parsedJson.WorkOrder);
                                        command.Parameters.AddWithValue("@Visit", parsedJson.Visit);
                                        command.Parameters.AddWithValue("@Pump", parsedJson.Pump);
                                        command.Parameters.AddWithValue("@TubeChangeLast", parsedJson.TubeChangeLast);
                                        command.Parameters.AddWithValue("@BatteryChangeLast", parsedJson.BatteryChangeLast);
                                        command.Parameters.AddWithValue("@Pail", parsedJson.Pail);
                                        command.Parameters.AddWithValue("@Odors", parsedJson.Odors);
                                        command.Parameters.AddWithValue("@Blockage", parsedJson.Blockage);
                                        command.Parameters.AddWithValue("@Floor", parsedJson.Floor);
                                        command.Parameters.AddWithValue("@Sink", parsedJson.Sink);
                                        command.Parameters.AddWithValue("@Notes", parsedJson.Notes);
                                        command.Parameters.AddWithValue("@AuditPdf", parsedJson.AuditPdf);
                                        command.Parameters.AddWithValue("@AuditTech", parsedJson.AuditTech);
                                        command.Parameters.AddWithValue("@AuditContact", parsedJson.AuditContact);
                                        command.Parameters.AddWithValue("@TechSignature", parsedJson.TechSignature);
                                        command.Parameters.AddWithValue("@CustSignature", parsedJson.CustSignature);
                                        command.Parameters.AddWithValue("@AuditCreation", parsedJson.AuditCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "LaundryAudit":
                            var jsonParsedLaundry = jsonCollection.ToObject<LaundryAudit[]>();

                            foreach (var parsedJson in jsonParsedLaundry)
                            {
                                DataRow[] nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND WorkOrder = '{parsedJson.WorkOrder}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@WorkOrder", parsedJson.WorkOrder);
                                        command.Parameters.AddWithValue("@Visit", parsedJson.Visit);
                                        command.Parameters.AddWithValue("@FabricAppearance", parsedJson.FabricAppearance);
                                        command.Parameters.AddWithValue("@FabricFinal", parsedJson.FabricFinal);
                                        command.Parameters.AddWithValue("@FabricOdor", parsedJson.FabricOdor);
                                        command.Parameters.AddWithValue("@FabricFeel", parsedJson.FabricFeel);
                                        command.Parameters.AddWithValue("@FabricStain", parsedJson.FabricStain);
                                        command.Parameters.AddWithValue("@FabricColor", parsedJson.FabricColor);
                                        command.Parameters.AddWithValue("@ProcedureCollection", parsedJson.ProcedureCollection);
                                        command.Parameters.AddWithValue("@ProcedureSorting", parsedJson.ProcedureSorting);
                                        command.Parameters.AddWithValue("@ProcedureLoading", parsedJson.ProcedureLoading);
                                        command.Parameters.AddWithValue("@ProcedureReclaim", parsedJson.ProcedureReclaim);
                                        command.Parameters.AddWithValue("@ProcedureReject", parsedJson.ProcedureReject);
                                        command.Parameters.AddWithValue("@ProcedurePre", parsedJson.ProcedurePre);
                                        command.Parameters.AddWithValue("@AuditPdf", parsedJson.AuditPdf);
                                        command.Parameters.AddWithValue("@Notes", parsedJson.Notes);
                                        command.Parameters.AddWithValue("@AuditTech", parsedJson.AuditTech);
                                        command.Parameters.AddWithValue("@AuditContact", parsedJson.AuditContact);
                                        command.Parameters.AddWithValue("@TechSignature", parsedJson.TechSignature);
                                        command.Parameters.AddWithValue("@CustSignature", parsedJson.CustSignature);
                                        command.Parameters.AddWithValue("@AuditCreation", parsedJson.AuditCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "WarewashAudit":
                            var jsonParsedWarewash = jsonCollection.ToObject<WarewashAudit[]>();

                            foreach (var parsedJson in jsonParsedWarewash)
                            {
                                DataRow[] nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND WorkOrder = '{parsedJson.WorkOrder}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@WorkOrder", parsedJson.WorkOrder);
                                        command.Parameters.AddWithValue("@Visit", parsedJson.Visit);
                                        command.Parameters.AddWithValue("@SerialNo", parsedJson.SerialNo);
                                        command.Parameters.AddWithValue("@WashArms", parsedJson.WashArms);
                                        command.Parameters.AddWithValue("@RinseJets", parsedJson.RinseJets);
                                        command.Parameters.AddWithValue("@RinseValves", parsedJson.RinseValves);
                                        command.Parameters.AddWithValue("@OverFlow", parsedJson.OverFlow);
                                        command.Parameters.AddWithValue("@ByPass", parsedJson.ByPass);
                                        command.Parameters.AddWithValue("@FinalRinsePsi", parsedJson.FinalRinsePsi);
                                        command.Parameters.AddWithValue("@PumpMotor", parsedJson.PumpMotor);
                                        command.Parameters.AddWithValue("@FillValve", parsedJson.FillValve);
                                        command.Parameters.AddWithValue("@Drains", parsedJson.Drains);
                                        command.Parameters.AddWithValue("@TempGauges", parsedJson.TempGauges);
                                        command.Parameters.AddWithValue("@Curtains", parsedJson.Curtains);
                                        command.Parameters.AddWithValue("@Racks", parsedJson.Racks);
                                        command.Parameters.AddWithValue("@Doors", parsedJson.Doors);
                                        command.Parameters.AddWithValue("@Detergent", parsedJson.Detergent);
                                        command.Parameters.AddWithValue("@DetergentDrops", parsedJson.DetergentDrops);
                                        command.Parameters.AddWithValue("@RinseAid", parsedJson.RinseAid);
                                        command.Parameters.AddWithValue("@RinseAidDrops", parsedJson.RinseAidDrops);
                                        command.Parameters.AddWithValue("@Sanitizer", parsedJson.Sanitizer);
                                        command.Parameters.AddWithValue("@SanitizerDrops", parsedJson.SanitizerDrops);
                                        command.Parameters.AddWithValue("@WaterHardness", parsedJson.WaterHardness);
                                        command.Parameters.AddWithValue("@Notes", parsedJson.Notes);
                                        command.Parameters.AddWithValue("@AuditPdf", parsedJson.AuditPdf);
                                        command.Parameters.AddWithValue("@AuditTech", parsedJson.AuditTech);
                                        command.Parameters.AddWithValue("@AuditContact", parsedJson.AuditContact);
                                        command.Parameters.AddWithValue("@TechSignature", parsedJson.TechSignature);
                                        command.Parameters.AddWithValue("@CustomerSignature", parsedJson.CustomerSignature);
                                        command.Parameters.AddWithValue("@AuditCreation", parsedJson.AuditCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "LaundryMachineList":
                            var jsonParsedLList = jsonCollection.ToObject<LaundryMachineList[]>();
                            
                            foreach (var parsedJson in jsonParsedLList)
                            {
                                DataRow[] nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND MachineSerial = '{parsedJson.MachineSerial}' AND MachineCreation = '{parsedJson.MachineCreation}' ");
                                if (nothingFound.Length == 0)
                                {
                                    sb.Append($"INSERT INTO {name} ({commaDelimited}) VALUES ({sqlParamaters})");
                                    using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@MachineSerial", parsedJson.MachineSerial);
                                        command.Parameters.AddWithValue("@MachineNo", parsedJson.MachineNo);
                                        command.Parameters.AddWithValue("@MachineMake", parsedJson.MachineMake);
                                        command.Parameters.AddWithValue("@MachineModel", parsedJson.MachineModel);
                                        command.Parameters.AddWithValue("@MachineCapacity", parsedJson.MachineCapacity);
                                        command.Parameters.AddWithValue("@DispenserMake", parsedJson.DispenserMake);
                                        command.Parameters.AddWithValue("@DispenserModel", parsedJson.DispenserModel);
                                        command.Parameters.AddWithValue("@DispenserSerial", parsedJson.DispenserSerial);
                                        command.Parameters.AddWithValue("@NoPump", parsedJson.NoPump);
                                        command.Parameters.AddWithValue("@Level", parsedJson.Level);
                                        command.Parameters.AddWithValue("@Valves", parsedJson.Valves);
                                        command.Parameters.AddWithValue("@Agitation", parsedJson.Agitation);
                                        command.Parameters.AddWithValue("@Timer", parsedJson.Timer);
                                        command.Parameters.AddWithValue("@Ph", parsedJson.Ph);
                                        command.Parameters.AddWithValue("@Bleach", parsedJson.Bleach);
                                        command.Parameters.AddWithValue("@Iron", parsedJson.Iron);
                                        command.Parameters.AddWithValue("@Chlorine", parsedJson.Chlorine);
                                        command.Parameters.AddWithValue("@WateHardness", parsedJson.WateHardness);
                                        command.Parameters.AddWithValue("@Temp", parsedJson.Temp);
                                        if (parsedJson.MachineExpirations == null)
                                            command.Parameters.AddWithValue("@MachineExpirations", DBNull.Value);
                                        else
                                            command.Parameters.AddWithValue("@MachineExpirations", parsedJson.MachineExpirations);
                                        command.Parameters.AddWithValue("@MachineCreation", parsedJson.MachineCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                    sb.Clear();
                                }
                                else
                                {
                                    sqlParamaters = $"UPDATE [dbo].[{name}] SET [MachineExpiration] = @MachineExpiration WHERE CustNo = '{parsedJson.CustNo}' AND MachineCreation = '{parsedJson.MachineCreation}' ";
                                    using (SqlCommand command = new SqlCommand(sqlParamaters,connection))
                                    {
                                        if (parsedJson.MachineExpirations == null)
                                            command.Parameters.AddWithValue("@MachineExpirations", DBNull.Value);
                                        else
                                            command.Parameters.AddWithValue("@MachineExpirations", parsedJson.MachineExpirations);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "WarewashMachineList":
                            var jsonParsedWList = jsonCollection.ToObject<WarewashMachineList[]>();
                            
                            foreach (var parsedJson in jsonParsedWList)
                            {
                                DataRow[] nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND MachineSerial = '{parsedJson.MachineSerial}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@MachineSerial", parsedJson.MachineSerial);
                                        command.Parameters.AddWithValue("@MachineNo", parsedJson.MachineNo);
                                        command.Parameters.AddWithValue("@MachineMake", parsedJson.MachineMake);
                                        command.Parameters.AddWithValue("@MachineModel", parsedJson.MachineModel);
                                        command.Parameters.AddWithValue("@location", parsedJson.location);
                                        command.Parameters.AddWithValue("@DispenserMake", parsedJson.DispenserMake);
                                        command.Parameters.AddWithValue("@DispenserModel", parsedJson.DispenserModel);
                                        command.Parameters.AddWithValue("@DispenserSerial", parsedJson.DispenserSerial);
                                        command.Parameters.AddWithValue("@Voltage", parsedJson.Voltage);
                                        command.Parameters.AddWithValue("@Scraping", parsedJson.Scraping);
                                        command.Parameters.AddWithValue("@Racking", parsedJson.Racking);
                                        command.Parameters.AddWithValue("@FlatwareSoak", parsedJson.FlatwareSoak);
                                        command.Parameters.AddWithValue("@MachineWater", parsedJson.MachineWater);
                                        command.Parameters.AddWithValue("@PrewashTank", parsedJson.PrewashTank);
                                        command.Parameters.AddWithValue("@WashTank", parsedJson.WashTank);
                                        command.Parameters.AddWithValue("@RinseTank", parsedJson.RinseTank);
                                        command.Parameters.AddWithValue("@TempType", parsedJson.TempType);
                                        command.Parameters.AddWithValue("@Temp", parsedJson.Temp);
                                        if (parsedJson.MachineExpiration == null)
                                            command.Parameters.AddWithValue("@MachineExpiration", DBNull.Value);
                                        else
                                            command.Parameters.AddWithValue("@MachineExpiration", parsedJson.MachineExpiration);
                                        command.Parameters.AddWithValue("@MachineCreation", parsedJson.MachineCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    responseMsg = ex.Message;
                }
            }
            return responseMsg;
        }

    }
}
