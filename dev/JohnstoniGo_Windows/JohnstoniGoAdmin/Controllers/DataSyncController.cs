﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using JohnstoniGoAdmin.JohnstoniGoDSTableAdapters;

namespace JohnstoniGoAdmin.Controllers
{
    public class DataSyncController : ApiController
    {

        //Sync the data fri om lev5 to pegasus
        //this will check and see if customer# exist and if theere 
        [HttpGet]
        public string SyncLev5Customers()
        {
            string syncStatus = "";

            var ds = new JohnstoniGoDS();

            ds.EnforceConstraints = false;

            var V_CUSTOMERSTableAdapter = new V_CUSTOMERSTableAdapter();
            var CustomerTableAdapter = new CustomerTableAdapter();
            var ShipTableAdapter = new ShippingAddressTableAdapter();
            var ShipToTa = new c_shiptoTableAdapter();
            var SalesmnTa = new c_slmn_mstTableAdapter();
            var TechTA = new c_slmn_mstTableAdapter();

            V_CUSTOMERSTableAdapter.Fill(ds.V_CUSTOMERS);
            
            foreach (JohnstoniGoDS.V_CUSTOMERSRow row in ds.V_CUSTOMERS)
            {
                CustomerTableAdapter.FillByCustomerNo(ds.Customer, row.CUSTNO);
                ShipToTa.FillBy(ds._c_shipto, row.CUSTNO);
                SalesmnTa.Fill(ds._c_slmn_mst, row.SALESMN1);
                TechTA.Fill(ds._c_slmn_mst, row.SALESMN3);
                
                var c_ShiptoRow = (JohnstoniGoDS._c_shiptoRow)ds._c_shipto.Rows[0];
                var salesmnRow = (JohnstoniGoDS._c_slmn_mstRow)ds._c_slmn_mst.Rows[0];
                var techRow = (JohnstoniGoDS._c_slmn_mstRow)ds._c_slmn_mst.Rows[0];

                if (ds.Customer.Rows.Count == 0)
                {
                    CustomerTableAdapter.InsertQuery(
                        row.CUSTNO,
                        row.NAME,
                        row.DEACONTACT,
                        row.PHONE,
                        row.EMAILADDRESS,
                        string.Empty,
                        string.Empty,
                        salesmnRow.name,
                        string.Empty,
                        techRow.name,
                        string.Empty,
                        row.CSRREP,
                        string.Empty
                        );
                }
                else
                {

                }
            }

            

            return syncStatus;
        }

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}