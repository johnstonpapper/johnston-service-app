﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JohnstoniGoAdmin.Controllers;

namespace JohnstoniGoAdmin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            DataSyncController syncController = new DataSyncController();
            syncController.SyncLev5Customers();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}