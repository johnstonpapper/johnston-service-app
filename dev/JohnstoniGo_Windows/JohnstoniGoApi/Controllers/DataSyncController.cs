﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JohnstoniGoApi.JohnstoniGoDSTableAdapters;

namespace JohnstoniGoApi.Controllers
{
    public class DataSyncController : ApiController
    {
        [HttpGet]
        public string syncLev5Customer()
        {
            string syncStatus = "";

            var ds = new JohnstoniGoDS();
            ds.EnforceConstraints = false;
            try
            {
                //variables for each table adapter
                var remoteCustTableAdapter = new c_mstTableAdapter();
            var remoteShipTableAdapter = new c_shiptoTableAdapter();
            var salesmnTableAdapter = new c_slmn_mstTableAdapter();
            var techTableAdapter = new c_slmn_mstTableAdapter();

            var localCustTableAdapter = new CustomerTableAdapter();
            var localShipTableAdapter = new ShippingAddressTableAdapter();

            remoteCustTableAdapter.Fill(ds._c_mst, true);
            
                foreach (JohnstoniGoDS._c_mstRow row in ds._c_mst)
                {
                    localCustTableAdapter.FillByCustNo(ds.Customer, row._cust_);
                    remoteShipTableAdapter.FillByCustNo(ds._c_shipto, row._cust_);
                    salesmnTableAdapter.Fill(ds._c_slmn_mst, row.salesmn1);
                    techTableAdapter.Fill(ds._c_slmn_mst, row.salesmn3);

                    var custShipRow = (JohnstoniGoDS._c_shiptoRow)ds._c_shipto.Rows[0];
                    var salesmanRow = (JohnstoniGoDS._c_slmn_mstRow)ds._c_slmn_mst.Rows[0];
                    var techmanRow = (JohnstoniGoDS._c_slmn_mstRow)ds._c_slmn_mst.Rows[0];
                    
                    while (ds.Customer.Rows.Count != 0)
                    {

                    }
                    if (ds.Customer.Rows.Count != 0)
                    {
                        localCustTableAdapter.InsertQuery(
                            row._cust_,
                            row.name,
                            row.buyer,
                            row.phone,
                            row._email_address,
                            string.Empty,
                            string.Empty,
                            salesmanRow.name,
                            string.Empty,
                            techmanRow.name,
                            string.Empty,
                            row._csr_rep,
                            string.Empty
                            );
                    }
                    else
                    {
                        
                    }
                }
            } catch (Exception ex)
            {

                 syncStatus = ex.Message;
            }
           


            return syncStatus;
        }

        /*
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        */

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}