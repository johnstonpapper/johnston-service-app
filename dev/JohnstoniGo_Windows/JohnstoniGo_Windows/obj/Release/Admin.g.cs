﻿#pragma checksum "..\..\Admin.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "E9ECA04E11AA5C9C4E54284D8779D061513C113914E8979A1AA0255ECFC76650"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using JohnstoniGo_Windows;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace JohnstoniGo_Windows {
    
    
    /// <summary>
    /// Admin
    /// </summary>
    public partial class Admin : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox serviceTechCombo;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid CompletedAudits;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button alphaNomega;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox sqlBox;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button executeSql;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AdminSearchBox;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel AuditSearchResults;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\Admin.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid AdminResultGrid;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/JohnstoniGo_Windows;component/admin.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Admin.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.serviceTechCombo = ((System.Windows.Controls.ComboBox)(target));
            
            #line 13 "..\..\Admin.xaml"
            this.serviceTechCombo.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ServiceTechCombo_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.CompletedAudits = ((System.Windows.Controls.DataGrid)(target));
            
            #line 15 "..\..\Admin.xaml"
            this.CompletedAudits.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.CompletedAudits_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 3:
            this.alphaNomega = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\Admin.xaml"
            this.alphaNomega.Click += new System.Windows.RoutedEventHandler(this.AlphaNomega_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.sqlBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.executeSql = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\Admin.xaml"
            this.executeSql.Click += new System.Windows.RoutedEventHandler(this.ExecuteSql_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.AdminSearchBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 38 "..\..\Admin.xaml"
            this.AdminSearchBox.KeyDown += new System.Windows.Input.KeyEventHandler(this.AdminSearchBox_KeyDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.AuditSearchResults = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 8:
            this.AdminResultGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 44 "..\..\Admin.xaml"
            this.AdminResultGrid.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.AdminResultGrid_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

