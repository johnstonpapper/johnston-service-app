﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Core;
using ToastNotifications.Lifetime;
using ToastNotifications.Lifetime.Clear;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace JohnstoniGo_Windows.Helpers
{
    public class ToastNote : INotifyPropertyChanged
    {
        private readonly Notifier notifier;

        public ToastNote()
        {
            notifier = new Notifier(cfg =>
              {
                  cfg.PositionProvider = new WindowPositionProvider(
                      parentWindow: Application.Current.MainWindow,
                      corner: Corner.BottomRight,
                      offsetX: 25,
                      offsetY: 100);

                  cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                      notificationLifetime: TimeSpan.FromSeconds(6),
                      maximumNotificationCount: MaximumNotificationCount.FromCount(6));

                  cfg.Dispatcher = Application.Current.Dispatcher;

                  cfg.DisplayOptions.TopMost = false;
                  cfg.DisplayOptions.Width = 250;
              });

            notifier.ClearMessages(new ClearAll());
        }

        public void OnUnLoaded()
        {
            notifier.Dispose();
        }

        public void ShowInformation(string message)
        {
            notifier.ShowInformation(message);
        }

        public void ShowInformation(string message, MessageOptions opts)
        {
            notifier.ShowInformation(message, opts);
        }

        public void ShowSuccess(string message)
        {
            notifier.ShowSuccess(message);
        }

        public void ShowSuccess(string message, MessageOptions opts)
        {
            notifier.ShowSuccess(message, opts);
        }

        public void ShowWarning(string message)
        {
            notifier.ShowWarning(message);
        }

        public void ShowWarning(string message, MessageOptions opts)
        {
            notifier.ShowWarning(message, opts);
        }

        public void ShowError(string message)
        {
            notifier.ShowError(message);
        }

        public void ShowError(string message, MessageOptions opts)
        {
            notifier.ShowError(message, opts);
        }

        internal void ClearMessage(string msg)
        {
            notifier.ClearMessages(new ClearByMessage(msg));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            var handler = PropertyChanged;
            if(handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ClearAll()
        {
            notifier.ClearMessages(new ClearAll());
        }

    }
}
