﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using PdfSharp.Drawing.Layout;
using System.Net;
using System.Drawing;
using System.Text.RegularExpressions;
using log4net;

namespace JohnstoniGo_Windows.Helpers
{
    class pdfGenerator
    {
        private const string Letters = "abcdefghijklmnopqrstuvwxyz";
        private readonly char[] AlphaNumeric = (Letters + Letters.ToUpper() + "1234567890").ToCharArray();

        //string pdfDataString = ConfigurationManager.ConnectionStrings["johnstonLocal"].ConnectionString; //testing remotely
        //string pdfDataString = ConfigurationManager.ConnectionStrings["johnstonRemote"].ConnectionString;
        //string pdfDataString = ConfigurationManager.ConnectionStrings["johnstonAmazon"].ConnectionString;
        string pdfDataString = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;
        string pdfStatus = "";

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        int yPoint = 0;
        XFont font = new XFont("Arial", 9, XFontStyle.Regular);
        XPen pen = new XPen(XColors.RoyalBlue, 1.75);


        private string pdfNameGenerator(string customer, string v)
        {
            //DateTime current
            var auditName = $"{DateTime.Now.Date.ToString("d")}-{customer}-{v}.pdf";
            Regex reg = new Regex("[\\/:*?<>|]");
            auditName = reg.Replace(auditName, " ");
            /*
            StringBuilder pdfFileName = new StringBuilder();
            Random r = new Random();
            for (int i = 0; i < 45; i++)
            {
                pdfFileName.Append(AlphaNumeric[r.Next(AlphaNumeric.Length)]);
            }
            */
            return auditName;
        }

        public void AuditHeaderFooter(PdfPage pg, XGraphics graphics)
        {

            //create font
            XFont font = new XFont("Arial", 8, XFontStyle.Regular);
            WebClient client = new WebClient();
            byte[] jpImg = client.DownloadData("https://s3.us-east-2.amazonaws.com/media.johnston.biz/JohnstonService/johnston/JPHeader.png");
            MemoryStream ms = new MemoryStream(jpImg);
            XImage logoHeader = XImage.FromStream(ms);

            graphics.DrawImage(logoHeader, 30, 25, 152, 43);
            
            //drain Cust
            graphics.DrawString("For further assistance, please call: 800.800.7123 ext. 168", font, XBrushes.Black, new XRect(30, 750, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
        }

        public string generateDrainPdf (string customer, string workOrder)
        {
            var directoryPath = @"C:\temp\PDF\";
            var imgDirectoryPath = @"C:\temp\signatures\";
            Directory.CreateDirectory(directoryPath);

            var auditData = $"SELECT DrainAudit.CustNo, Customers.AccountName, Customers.Contact, Customers.Phone, Customers.InsideSales, Customers.SalesRep, Customers.ServiceTech, Customers.CustomerServiceRep, Customers.Nickname, CustomerAddresses.Address1, CustomerAddresses.Address2, CustomerAddresses.City, CustomerAddresses.State, CustomerAddresses.Zip, DrainAudit.WorkOrder, DrainAudit.Visit, DrainAudit.Pump,  DrainAudit.TubeChangeLast, DrainAudit.BatteryChangeLast, DrainAudit.Pail, DrainAudit.Odors, DrainAudit.Blockage, DrainAudit.Floor, DrainAudit.Sink, DrainAudit.Notes, DrainAudit.AuditTech, DrainAudit.TechSignature, DrainAudit.AuditContact, DrainAudit.CustSignature, DrainAudit.AuditCreation FROM Customers INNER JOIN DrainAudit ON Customers.CustNo = DrainAudit.CustNo INNER JOIN CustomerAddresses ON Customers.CustNo = CustomerAddresses.CustNo WHERE(DrainAudit.CustNo = '{customer}' AND DrainAudit.WorkOrder = '{workOrder}') Order by [AuditCreation] Desc";

            PdfDocument auditDoc = new PdfDocument();
            //save the doc
            var filename = string.Empty;

            using (var sqlConnection = new SqlConnection(pdfDataString))
            {
                try
                {
                    sqlConnection.Open();

                    auditDoc.Info.Title = "Drain Audit Report";

                    //create an empty page
                    PdfPage pg = auditDoc.AddPage();

                    //get an xgraphic obj for drawing
                    XGraphics graphics = XGraphics.FromPdfPage(pg, XGraphicsPdfPageOptions.Append);

                    //create font

                    XFont font = new XFont("Arial", 8, XFontStyle.Regular);

                    AuditHeaderFooter(pg, graphics);
                    
                    yPoint = yPoint + 100;

                    using (SqlCommand sqlCmd = new SqlCommand(auditData, sqlConnection))
                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();

                            filename = pdfNameGenerator(dr["AccountName"].ToString(), "Drain");
                            graphics.TranslateTransform(15, 20);

                            XPen pen = new XPen(XColors.RoyalBlue, Math.PI);

                            graphics.DrawRoundedRectangle(pen, 30, 95, 515, 85, 10, 15);

                            //<!-- Customer Details -->
                            //left
                            graphics.DrawString($"Account Number: {dr["CustNo"]}", font, XBrushes.Black, new XRect(40, 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString($"Account Name: {dr["AccountName"]}" , font, XBrushes.Black, new XRect(40, 115, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString($"Contact: {dr["AuditContact"]}", font, XBrushes.Black, new XRect(40, 130, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString($"Phone: {dr["Phone"]}", font, XBrushes.Black, new XRect(40, 145, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            
                            //graphics.DrawString("Email: " + dr.GetString(4), font, XBrushes.Black, new XRect(40, 130, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            graphics.DrawString($"WO#: {dr["WorkOrder"]}", font, XBrushes.Black, new XRect(350, 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString($"Tech: {dr["AuditTech"]}", font, XBrushes.Black, new XRect(350, 115, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString($"Sales Rep: {dr["SalesRep"]}" + dr.GetString(5), font, XBrushes.Black, new XRect(350, 130, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //<!-- Audit Details -->
                            //-->left Align
                            //drain visit
                            graphics.DrawString($"Reason of Visitation: {dr["Visit"]}", font, XBrushes.Black, new XRect(30, 80, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain pump
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(20, yPoint + 95, 150, 20));
                            graphics.DrawString("Inspected Auto Drain Pump(s)", font, XBrushes.Black, new XRect(30, yPoint + 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(DrainBooleanToText((bool) dr["Pump"]), font, XBrushes.Black, new XRect(30, yPoint + 125, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain battery
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(20, yPoint + 170, 150, 20));
                            graphics.DrawString("Batteries Last Changed", font, XBrushes.Black, new XRect(30, yPoint + 175, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(dr.GetDateTime(18).Date.ToShortDateString(), font, XBrushes.Black, new XRect(30, yPoint + 200, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain odors
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(20, yPoint + 245, 150, 20));
                            graphics.DrawString("Grease Trap Odors", font, XBrushes.Black, new XRect(30, yPoint + 250, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(DrainBooleanToText((bool)dr["Odors"]), font, XBrushes.Black, new XRect(30, yPoint + 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain floor
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(20, yPoint + 320, 150, 20));
                            graphics.DrawString("Floor Drain(s) Treated", font, XBrushes.Black, new XRect(30, yPoint + 325, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(DrainBooleanToText((bool)dr["Floor"]), font, XBrushes.Black, new XRect(30, yPoint + 350, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain tech
                            XImage tImage = SignatureAudit(dr, 26);
                            //XImage tImage = XImage.FromFile(imgDirectoryPath + dr.GetString(26));
                            graphics.DrawImage(tImage, 30, yPoint + 520, 152, 43);
                            graphics.DrawString(dr.GetString(25), font, XBrushes.Black, new XRect(30, yPoint + 575, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //-->right align
                            //top right 
                            graphics.DrawString(dr["AuditCreation"].ToString(), font, XBrushes.Black, new XRect(425, 80, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain tube
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(365, yPoint + 95, 150, 20));
                            graphics.DrawString("Tube(s) Last Changed", font, XBrushes.Black, new XRect(375, yPoint + 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(dr.GetDateTime(17).Date.ToShortDateString(), font, XBrushes.Black, new XRect(375, yPoint + 125, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain pail
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(365, yPoint + 170, 150, 20));
                            graphics.DrawString("Replace Pail(s)", font, XBrushes.Black, new XRect(375, yPoint + 175, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(DrainBooleanToText((bool)dr["Pail"]), font, XBrushes.Black, new XRect(375, yPoint + 200, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain blockage
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(365, yPoint + 245, 150, 20));
                            graphics.DrawString("Grease Trap Blockage", font, XBrushes.Black, new XRect(375, yPoint + 250, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(DrainBooleanToText((bool)dr["Blockage"]), font, XBrushes.Black, new XRect(375, yPoint + 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain sink
                            graphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(365, yPoint + 320, 150, 20));
                            graphics.DrawString("Sink Drain(s) Treated", font, XBrushes.Black, new XRect(375, yPoint + 325, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            graphics.DrawString(DrainBooleanToText((bool)dr["Sink"]), font, XBrushes.Black, new XRect(375, yPoint + 350, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            //drain Cust
                            XImage cImage = SignatureAudit(dr,28);
                            //XImage cImage = XImage.FromFile(imgDirectoryPath + dr.GetString(28));
                            graphics.DrawImage(cImage, 375, yPoint + 520, 152, 43);
                            graphics.DrawString(dr.GetString(27), font, XBrushes.Black, new XRect(375, yPoint + 575, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                            
                            //---> this 
                            PdfPage notesPage = auditDoc.AddPage();
                            //get an xgraphic obj for drawing
                            XGraphics noteGraphics = XGraphics.FromPdfPage(notesPage);
                            XTextFormatter textFormatter = new XTextFormatter(noteGraphics);
                            AuditHeaderFooter(notesPage, noteGraphics);

                            //drain notes
                            noteGraphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(20, 100, 150, 20));
                            noteGraphics.DrawString("Notes/Comments", font, XBrushes.Black, new XRect(30, 105, notesPage.Width.Point, notesPage.Height.Point), XStringFormats.TopLeft);
                            textFormatter.DrawString((string) dr["Notes"], font, XBrushes.Black, new XRect(30, 130, 550, notesPage.Height.Point), XStringFormats.TopLeft);
                        }
                    }

                    saveAuditPdf(filename, customer, "DrainAudit", workOrder);

                    auditDoc.Save(directoryPath + filename);

                     Process.Start(directoryPath + filename);

                    yPoint = 0;

                }
                catch (Exception ex)
                {
                    log.Error("Drain audit creation failed: " + ex.Message);
                }
            }
            log.Info($"Drain audit for : {customer} generated successfully name : {filename}");
            return filename;
        }

        public string generateLaundryPdf(string customer, string workOrder)
        {
            var directoryPath = @"C:\temp\PDF\";
            var imgDirectoryPath = @"C:\temp\signatures\";
            
            Directory.CreateDirectory(directoryPath);

            var laundryAuditReport = $"SELECT LaundryAudit.CustNo, Customers.AccountName, Customers.Contact, Customers.Phone, Customers.InsideSales, Customers.SalesRep, Customers.ServiceTech, Customers.CustomerServiceRep, Customers.Nickname, CustomerAddresses.Address1, CustomerAddresses.Address2, CustomerAddresses.City, CustomerAddresses.State, CustomerAddresses.Zip, LaundryMachineList.MachineNo, LaundryMachineList.MachineMake, LaundryMachineList.MachineModel, LaundryMachineList.MachineSerial, LaundryMachineList.MachineCapacity, LaundryMachineList.DispenserMake, LaundryMachineList.DispenserModel, LaundryMachineList.DispenserSerial, LaundryMachineList.NoPump, LaundryMachineList.[Level], LaundryMachineList.Temp, LaundryMachineList.Valves, LaundryMachineList.Agitation, LaundryMachineList.Timer, LaundryMachineList.Ph, LaundryMachineList.Bleach, LaundryMachineList.Iron, LaundryMachineList.Chlorine, LaundryMachineList.WateHardness, LaundryAudit.WorkOrder, LaundryAudit.Visit, LaundryAudit.FabricAppearance, LaundryAudit.FabricFinal, LaundryAudit.FabricOdor, LaundryAudit.FabricFeel, LaundryAudit.FabricStain, LaundryAudit.FabricColor, LaundryAudit.ProcedureCollection, LaundryAudit.ProcedureSorting, LaundryAudit.ProcedurePre, LaundryAudit.ProcedureLoading, LaundryAudit.ProcedureReclaim, LaundryAudit.ProcedureReject, LaundryAudit.Notes, LaundryAudit.AuditTech, LaundryAudit.TechSignature, LaundryAudit.AuditContact, LaundryAudit.CustSignature, LaundryAudit.AuditCreation FROM Customers INNER JOIN LaundryAudit ON Customers.CustNo = LaundryAudit.CustNo INNER JOIN LaundryMachineList ON Customers.CustNo = LaundryMachineList.CustNo INNER JOIN CustomerAddresses ON Customers.CustNo = CustomerAddresses.CustNo WHERE (LaundryAudit.CustNo = '{customer}' AND LaundryAudit.WorkOrder = '{workOrder}') Order by [AuditCreation] Desc";

            //save the doc

            var filename = string.Empty;
            PdfDocument auditDoc = new PdfDocument();

            using (SqlConnection conn = new SqlConnection(pdfDataString))
            {
                try
                {
                    conn.Open();
                    auditDoc.Info.Title = "Laundry Audit Report";

                    using (SqlCommand sqlCmd = new SqlCommand(laundryAuditReport, conn))
                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            filename = pdfNameGenerator(dr["AccountName"].ToString(), "Laundry");
                            //create an empty page
                            PdfPage pg = auditDoc.AddPage();

                            //get an xgraphic obj for drawing
                            XGraphics graphics = XGraphics.FromPdfPage(pg);

                            AuditHeaderFooter(pg, graphics);
                            
                            yPoint = yPoint + 100;
                            if (dr.HasRows)
                            {
                                graphics.TranslateTransform(15, 20);

                                //<!-- Customer Details -->
                                //left
                                graphics.DrawString($"Account Number: {dr["CustNo"]}", font, XBrushes.Black, new XRect(40, 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Account Name: {dr["AccountName"]}", font, XBrushes.Black, new XRect(40, 115, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Contact: {dr["AuditContact"]}", font, XBrushes.Black, new XRect(40, 130, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Phone: {dr["Phone"]}", font, XBrushes.Black, new XRect(40, 145, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"WO#: {dr["WorkOrder"]}", font, XBrushes.Black, new XRect(350, 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Tech: {dr["AuditTech"]}", font, XBrushes.Black, new XRect(350, 115, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Sales Rep: {dr["SalesRep"]}", font, XBrushes.Black, new XRect(350, 130, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                
                                //Laundry visit
                                graphics.DrawString($"Reason of Visitation: {dr["Visit"]}", font, XBrushes.Black, new XRect(30, 80, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //<!-- Audit Details -->
                                //-->left Align
                                graphics.DrawString($"Machine No. {dr["MachineNo"]}", font, XBrushes.Black, new XRect(40, 195, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //grid design
                                laundryTable(pg, graphics, dr["AuditCreation"].ToString());
                                
                                //left to right
                                graphics.DrawString($"Machine Make: {dr["MachineMake"]} ", font, XBrushes.Black, new XRect(40, 215, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Machine Model: {dr["MachineModel"]}", font, XBrushes.Black, new XRect(40, 245, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Serial # {dr["MachineSerial"]}", font, XBrushes.Black, new XRect(40, 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Capacity {dr["MachineCapacity"]}", font, XBrushes.Black, new XRect(40, 305, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Dispenser Make: {dr["DispenserMake"]}", font, XBrushes.Black, new XRect(40, 335, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Dispenser Model: {dr["DispenserModel"]}", font, XBrushes.Black, new XRect(40, 365, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Dispenser Serial #: {dr["DispenserSerial"]}", font, XBrushes.Black, new XRect(40, 395, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //left to right
                                graphics.DrawString($"Water Level: {dr["Level"]}", font, XBrushes.Black, new XRect(220, 215, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Temperature: {dr["Temp"]} °F", font, XBrushes.Black, new XRect(220, 245, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Drain Valves: {dr["Valves"]}", font, XBrushes.Black, new XRect(220, 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Agitation: {dr["Agitation"]}", font, XBrushes.Black, new XRect(220, 305, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Timer: {dr["Timer"]}", font, XBrushes.Black, new XRect(220, 335, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Wash pH/Alkalinity: {dr["Ph"]}", font, XBrushes.Black, new XRect(220, 365, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //left to right
                                graphics.DrawString($"Bleach pH: {dr["Bleach"]}", font, XBrushes.Black, new XRect(385, 215, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Iron Level: {dr["Iron"]}", font, XBrushes.Black, new XRect(385, 245, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Chlorine Level: {dr["Chlorine"]}", font, XBrushes.Black, new XRect(385, 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Water Hardness: {dr["WaterHardness"]}" + dr.GetInt32(32), font, XBrushes.Black, new XRect(385, 305, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"# Pump: {dr["NoPump"]}", font, XBrushes.Black, new XRect(385, 335, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Appearance: {dr["FabricAppearance"]}", font, XBrushes.Black, new XRect(40, 460, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Final pH: {dr["FabricFinal"]}", font, XBrushes.Black, new XRect(40, 485, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Odor: {dr["FabricOdor"]}", font, XBrushes.Black, new XRect(40, 505, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Feel: {dr["FabricFeel"]}", font, XBrushes.Black, new XRect(150, 460, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Stain Removal: {dr["FabricStain"]}", font, XBrushes.Black, new XRect(150, 485, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Color: {dr["FabricColor"]}", font, XBrushes.Black, new XRect(150, 505, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Collection: {ChangeBoolToText((bool) dr["ProcedureCollection"])}", font, XBrushes.Black, new XRect(310, 460, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Sorting: {ChangeBoolToText((bool)dr["ProcedureSorting"])}", font, XBrushes.Black, new XRect(310, 485, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Pretreatment: {ChangeBoolToText((bool)dr["ProcedurePre"])}", font, XBrushes.Black, new XRect(310, 505, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString($"Loading: {ChangeBoolToText((bool) dr["ProcedureLoading"])}", font, XBrushes.Black, new XRect(435, 460, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Reclaim: {ChangeBoolToText((bool)dr["ProcedureReclaim"])}", font, XBrushes.Black, new XRect(435, 485, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString($"Reject: {ChangeBoolToText((bool)dr["ProcedureReject"])}", font, XBrushes.Black, new XRect(435, 505, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //drain tech
                                XImage tImage = SignatureAudit(dr, 49);
                                //XImage tImage = XImage.FromFile(imgDirectoryPath + dr.GetString(49));
                                graphics.DrawImage(tImage, 30, 675, 152, 43);
                                graphics.DrawString(dr.GetString(48), font, XBrushes.Black, new XRect(30, 715, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //drain Cust
                                XImage cImage = SignatureAudit(dr, 51);
                                //XImage cImage = XImage.FromFile(imgDirectoryPath + dr.GetString(51));
                                graphics.DrawImage(cImage, 375, 675, 152, 43);
                                graphics.DrawString(dr.GetString(50), font, XBrushes.Black, new XRect(375, 715, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            }

                            //---> this 
                            PdfPage notesPage = auditDoc.AddPage();
                            //get an xgraphic obj for drawing
                            XGraphics noteGraphics = XGraphics.FromPdfPage(notesPage);
                            XTextFormatter textFormatter = new XTextFormatter(noteGraphics);
                            AuditHeaderFooter(notesPage, noteGraphics);

                            //Notes 
                            noteGraphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(20, 100, 150, 20));
                            noteGraphics.DrawString("Notes/Comments", font, XBrushes.Black, new XRect(30, 105, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                            textFormatter.DrawString((string) dr["Notes"], font, XBrushes.Black, new XRect(30, 130, 515, pg.Height.Point), XStringFormats.TopLeft);

                        }
                        
                    }

                    saveAuditPdf(filename, customer, "LaundryAudit", workOrder);
                    
                    auditDoc.Save(directoryPath + filename);

                    Process.Start(directoryPath + filename);

                    yPoint = 0;

                } catch (Exception ex)
                {
                    log.Error("laundry audit creation failed: " + ex.Message);
                }
            }
            log.Info($"Laundry audit for : {customer} generated successfully name : {filename}");
            return filename;
        }

        public string generateWarewashPdf(string customer, string workorder)
        {
            var directoryPath = @"C:\temp\PDF\";
            var imgDirectoryPath = @"C:\temp\signatures\";

            Directory.CreateDirectory(directoryPath);
            
            var warewashAuditReport = $"SELECT WarewashAudit.CustNo, Customers.AccountName, Customers.Contact, Customers.Phone, Customers.Email, Customers.InsideSales, Customers.SalesRep, Customers.ServiceTech, Customers.CustomerServiceRep, WarewashMachineList.MachineNo, WarewashMachineList.MachineMake, WarewashMachineList.MachineModel, WarewashMachineList.MachineSerial, WarewashMachineList.location, WarewashMachineList.DispenserMake, WarewashMachineList.DispenserModel, WarewashMachineList.DispenserSerial, WarewashMachineList.Voltage, WarewashMachineList.Scraping, WarewashMachineList.Racking, WarewashMachineList.FlatwareSoak, WarewashMachineList.MachineWater, WarewashMachineList.PrewashTank, WarewashMachineList.WashTank, WarewashMachineList.RinseTank, WarewashMachineList.TempType, WarewashMachineList.Temp, WarewashAudit.WorkOrder, WarewashAudit.Visit, WarewashAudit.WashArms, WarewashAudit.RinseJets, WarewashAudit.RinseValves, WarewashAudit.OverFlow, WarewashAudit.ByPass, WarewashAudit.FinalRinsePsi, WarewashAudit.PumpMotor, WarewashAudit.FillValve, WarewashAudit.Drains, WarewashAudit.TempGauges, WarewashAudit.Curtains, WarewashAudit.Racks, WarewashAudit.Doors, WarewashAudit.Detergent, WarewashAudit.DetergentDrops, WarewashAudit.RinseAid, WarewashAudit.RinseAidDrops, WarewashAudit.Sanitizer, WarewashAudit.SanitizerDrops, WarewashAudit.WaterHardness, WarewashAudit.Notes, WarewashAudit.AuditContact, WarewashAudit.CustomerSignature, WarewashAudit.AuditTech, WarewashAudit.TechSignature, CustomerAddresses.Address1, CustomerAddresses.Address2, CustomerAddresses.City, CustomerAddresses.State, CustomerAddresses.Zip, WarewashAudit.AuditCreation FROM Customers INNER JOIN CustomerAddresses ON Customers.CustNo = CustomerAddresses.CustNo INNER JOIN WarewashAudit ON Customers.CustNo = WarewashAudit.CustNo INNER JOIN WarewashMachineList ON Customers.CustNo = WarewashMachineList.CustNo WHERE (WarewashAudit.CustNo = '{customer}' AND WarewashAudit.WorkOrder = '{workorder}') Order by [AuditCreation] Desc";

            //save the doc
            var filename = string.Empty;

            PdfDocument auditDoc = new PdfDocument();

            using (SqlConnection conn = new SqlConnection(pdfDataString))
            {
                try
                {
                    conn.Open();
                    auditDoc.Info.Title = "Warewash Audit Report";

                    using (SqlCommand cmd = new SqlCommand(warewashAuditReport, conn))
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                filename = pdfNameGenerator(dr["AccountName"].ToString(), "Warewash");
                                //create an empty page
                                PdfPage pg = auditDoc.AddPage();

                                //get an xgraphic obj for drawing
                                XGraphics graphics = XGraphics.FromPdfPage(pg);

                                yPoint = yPoint + 100;

                                AuditHeaderFooter(pg, graphics);

                                graphics.TranslateTransform(15, 20);

                                //<!-- Customer Details -->
                                //left
                                graphics.DrawString("Account Number: " + dr.GetString(0), font, XBrushes.Black, new XRect(40, 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Account Name: " + dr.GetString(1), font, XBrushes.Black, new XRect(40, 115, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Contact: " + dr.GetString(50), font, XBrushes.Black, new XRect(40, 130, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Phone: " + dr.GetString(3), font, XBrushes.Black, new XRect(40, 145, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                graphics.DrawString("WO#: " + dr.GetString(27), font, XBrushes.Black, new XRect(350, 100, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Tech: " + dr.GetString(52), font, XBrushes.Black, new XRect(350, 115, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Sales Rep: " + dr.GetString(6), font, XBrushes.Black, new XRect(350, 130, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //Laundry visit
                                graphics.DrawString("Reason of Visitation: " + dr.GetString(28), font, XBrushes.Black, new XRect(30, 80, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //<!-- Audit Details -->
                                //-->left Align
                                graphics.DrawString("Machine No. " + dr.GetInt32(9), font, XBrushes.Black, new XRect(40, 195, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //align right
                                graphics.DrawString("Location: " + dr.GetString(13), font, XBrushes.Black, new XRect(350, 195, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //grid design
                                WarewashTable(pg, graphics, dr["AuditCreation"].ToString());

                                //left to right
                                graphics.DrawString("Machine Make: " + dr.GetString(10), font, XBrushes.Black, new XRect(40, 215, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Machine Model: " + dr.GetString(11), font, XBrushes.Black, new XRect(40, 245, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Serial #: " + dr.GetString(12), font, XBrushes.Black, new XRect(40, 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Dispenser Make: " + dr.GetString(13), font, XBrushes.Black, new XRect(40, 305, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Dispenser Model: " + dr.GetString(14), font, XBrushes.Black, new XRect(40, 335, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Dispenser Serial #: " + dr.GetString(15), font, XBrushes.Black, new XRect(40, 365, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //left to right
                                graphics.DrawString("Voltage: " + dr.GetInt32(17), font, XBrushes.Black, new XRect(220, 215, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Scraping: " + ChangeBoolToText(dr.GetBoolean(18)), font, XBrushes.Black, new XRect(220, 245, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Racking: " + ChangeBoolToText(dr.GetBoolean(19)), font, XBrushes.Black, new XRect(220, 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Flatware Soak: " + ChangeBoolToText(dr.GetBoolean(20)), font, XBrushes.Black, new XRect(220, 305, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Machine Water: " + ChangeBoolToText(dr.GetBoolean(21)), font, XBrushes.Black, new XRect(220, 335, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //left to right
                                graphics.DrawString("PreWash Tank: " + dr.GetInt32(22) + "°F", font, XBrushes.Black, new XRect(385, 215, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Wash Tank: " + dr.GetInt32(23) + "°F", font, XBrushes.Black, new XRect(385, 245, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Rinse Tank: " + dr.GetInt32(24) + "°F", font, XBrushes.Black, new XRect(385, 275, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Temperature: " + dr.GetInt32(26) + "°F", font, XBrushes.Black, new XRect(385, 305, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                if (dr.GetBoolean(25) == false)
                                {
                                    graphics.DrawString("Machine Type: High ", font, XBrushes.Black, new XRect(385, 335, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                }
                                else
                                {
                                    graphics.DrawString("Machine  Type: Low ", font, XBrushes.Black, new XRect(385, 335, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                }

                                //Comments 
                                graphics.DrawString("Wash Arms: " + dr.GetString(29), font, XBrushes.Black, new XRect(40, 415, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Rinse Jets: " + dr.GetString(30), font, XBrushes.Black, new XRect(40, 415 + 15, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Rinse Valve: " + dr.GetString(31), font, XBrushes.Black, new XRect(40, 415 + 30, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Overflow: " + dr.GetString(32), font, XBrushes.Black, new XRect(40, 415 + 45, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("By-Pass: " + dr.GetString(33), font, XBrushes.Black, new XRect(40, 415 + 60, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Final Rinse psi: " + dr.GetInt32(34), font, XBrushes.Black, new XRect(40, 415 + 75, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //side
                                graphics.DrawString("Pump/Motor: " + dr.GetString(35), font, XBrushes.Black, new XRect(140, 415, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Fill Valve: " + dr.GetString(36), font, XBrushes.Black, new XRect(140, 415 + 15, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Drains: " + dr.GetString(37), font, XBrushes.Black, new XRect(140, 415 + 30, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Temp. Gauges: " + dr.GetString(38), font, XBrushes.Black, new XRect(140, 415 + 45, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Curtains: " + dr.GetString(39), font, XBrushes.Black, new XRect(140, 415 + 60, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Doors: " + dr.GetString(41), font, XBrushes.Black, new XRect(140, 415 + 75, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //Chemical Concentration
                                graphics.DrawString("Detergent: " + dr.GetString(42), font, XBrushes.Black, new XRect(325, 415, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Rinse Aid: " + dr.GetString(44), font, XBrushes.Black, new XRect(325, 415 + 15, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Sanitizer: " + dr.GetString(46), font, XBrushes.Black, new XRect(325, 415 + 30, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString("Water Hardness (gpg): " + dr.GetInt32(48), font, XBrushes.Black, new XRect(325, 415 + 45, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //Chemical Concentration con
                                graphics.DrawString(dr.GetInt32(43) + " ppm", font, XBrushes.Black, new XRect(455, 415, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString(dr.GetInt32(45) + " mils", font, XBrushes.Black, new XRect(455, 415 + 15, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
                                graphics.DrawString(dr.GetInt32(47) + " ppm", font, XBrushes.Black, new XRect(455, 415 + 30, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //Tech signatures
                                XImage tImage = SignatureAudit(dr, 53);
                                //XImage tImage = XImage.FromFile(imgDirectoryPath + dr.GetString(54));
                                graphics.DrawImage(tImage, 30, 675, 152, 43);
                                graphics.DrawString(dr.GetString(52), font, XBrushes.Black, new XRect(30, 715, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

                                //Customer Signature
                                XImage cImage = SignatureAudit(dr, 51);
                                //XImage cImage = XImage.FromFile(imgDirectoryPath + dr.GetString(52));
                                graphics.DrawImage(cImage, 375, 675, 152, 43);
                                graphics.DrawString(dr.GetString(50), font, XBrushes.Black, new XRect(375, 715, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);


                                //---> this 
                                PdfPage notesPage = auditDoc.AddPage();
                                //get an xgraphic obj for drawing
                                XGraphics noteGraphics = XGraphics.FromPdfPage(notesPage);
                                XTextFormatter textFormatter = new XTextFormatter(noteGraphics);
                                AuditHeaderFooter(notesPage, noteGraphics);

                                //Notes 
                                noteGraphics.DrawRectangle(new XSolidBrush(XColor.FromCmyk(0.77, 0.26, 0, 0)), new XRect(20, 100, 150, 20));
                                noteGraphics.DrawString("Notes/Comments", font, XBrushes.Black, new XRect(30, 105, notesPage.Width.Point, notesPage.Height.Point), XStringFormats.TopLeft);
                                textFormatter.DrawString(dr.GetString(49), font, XBrushes.Black, new XRect(30, 130, 515, notesPage.Height.Point), XStringFormats.TopLeft);
                            }
                        }

                        saveAuditPdf(filename, customer, "WarewashAudit", workorder);
                        auditDoc.Save(directoryPath + filename);

                        Process.Start(directoryPath + filename);

                        yPoint = 0;

                    }
                    log.Info($"Warewash audit for : {customer} generated successfully name : {filename}");
                } catch (Exception ex)
                {
                    log.Error("Warewash audit creation failed: " + ex.Message);
                    filename = ex.Message;
                }
            }
            log.Error($"This the second error : {filename}");
            return filename;
        }

        private static XImage SignatureAudit(SqlDataReader dr, int dbPosition)
        {
            var imgDirectoryPath = $@"C:\temp\signatures\{dr.GetString(dbPosition)}";
            XImage xImage;
            if (File.Exists(imgDirectoryPath))
            {
                xImage = XImage.FromFile(imgDirectoryPath);
            } else
            {
                WebClient webClient = new WebClient();
                byte[] imgByte = webClient.DownloadData($"https://s3.us-east-2.amazonaws.com/media.johnston.biz/JohnstonService/AuditSignatures/{dr.GetString(dbPosition)}");
                MemoryStream ms = new MemoryStream(imgByte);
                xImage = XImage.FromStream(ms);
            }
            
            return xImage;
        }

        void laundryTable(PdfPage pg, XGraphics graphics, string auditDate)
        {
            //Header display box
            graphics.DrawRoundedRectangle(pen, 30, 95, 515, 85, 10, 15);
            graphics.DrawString(auditDate, font, XBrushes.Black, new XRect(450, 85, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

            //Machine table display
            graphics.DrawRoundedRectangle(pen, 30, 205, 515, 210, 10, 15);

            //Fabric Results Titles
            graphics.DrawString("Fabric Results", font, XBrushes.Black, new XRect(40, 445, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
            //procedure
            graphics.DrawString("Procedures", font, XBrushes.Black, new XRect(310, 445, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
            
            //Machine Audit Details
            graphics.DrawRoundedRectangle(pen, 30, 455, 515, 65, 10, 15);

            //Machine lines
            graphics.DrawLine(pen, 30, 235, 545, 235);
            graphics.DrawLine(pen, 30, 265, 545, 265);
            graphics.DrawLine(pen, 30, 295, 545, 295);
            graphics.DrawLine(pen, 30, 325, 545, 325);
            graphics.DrawLine(pen, 30, 355, 545, 355);
            graphics.DrawLine(pen, 30, 385, 545, 385);

            //machine details lines
            graphics.DrawLine(pen, 30, 475, 545, 475);
            graphics.DrawLine(pen, 30, 500, 545, 500);
        }

        void WarewashTable(PdfPage pg, XGraphics graphics, string auditDate)
        {
            int tableX = 30, tableY = 0;

            //Header display box
            graphics.DrawRoundedRectangle(pen, 30, 95, 515, 85, 10, 15);
            graphics.DrawString(auditDate, font, XBrushes.Black, new XRect(tableX + 420, tableY + 85, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);

            //table display
            graphics.DrawRoundedRectangle(pen, tableX, 205, 515, 180, 10, 15);

            graphics.DrawString("Comments", font, XBrushes.Black, new XRect(tableX + 10, 405, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
            graphics.DrawString("Machine Chemical Concentration", font, XBrushes.Black, new XRect(tableX + 370, 405, pg.Width.Point, pg.Height.Point), XStringFormats.TopLeft);
            graphics.DrawRoundedRectangle(pen, tableX, 415, 515, 85, 10, 15);

        }

        private string ChangeBoolToText(Boolean dbValue)
        {
            var AuditResponseText = "";
            if(dbValue)
            {
                AuditResponseText = "Needs Review*";
            } else
            {
                AuditResponseText = "Good Practices";
            }
            return AuditResponseText;
        }

        private string DrainBooleanToText(Boolean dbValue)
        {
            var AuditResponseText = string.Empty;
            if (!dbValue)
            {
                AuditResponseText = "No";
            }
            else
            {
                AuditResponseText = "Yes";
            }
            return AuditResponseText;
        }

        bool saveAuditPdf(string auditName, string customerNo, string tableName, string jpWorkOrder)
        {
            var pdfSaveStatus = false;
            using (SqlConnection connection = new SqlConnection(pdfDataString))
            {
                connection.Open();
                try
                {
                    var updateAudit = $"UPDATE {tableName} SET AuditPdf = @AuditPdf  WHERE CustNo = '{customerNo}' AND WorkOrder = '{jpWorkOrder}'";
                    using (SqlCommand cmd = new SqlCommand(updateAudit, connection))
                    {
                        cmd.Parameters.AddWithValue("@AuditPDF", auditName);

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                    log.Info($"Audit updated : {customerNo} work order : {jpWorkOrder}");
                    pdfSaveStatus = true;
                }
                catch (Exception ex)
                {
                    log.Error($"Inserting audit name error : {customerNo} on work order : {jpWorkOrder} / error : {ex.Message}");
                    pdfSaveStatus = false;
                }
            }
            return pdfSaveStatus;
        }
    }
}
