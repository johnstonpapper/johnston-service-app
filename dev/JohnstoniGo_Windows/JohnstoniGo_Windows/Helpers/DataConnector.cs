﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using JohnstoniGo_Windows.Helpers;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using System.Net.Http.Headers;

namespace JohnstoniGo_Windows.Helpers
{
    class DataConnector
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string dataString = ConfigurationManager.ConnectionStrings["johnstonAmazon"].ConnectionString;
        string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;

        /// <summary>
        /// Check if a database exist in the first place
        /// </summary>
        public void dbExist()
        {
            var dbCheck = "SELECT * FROM master.dbo.sysdatabases WHERE name = 'JPService'";
            using (SqlConnection connection = new SqlConnection(localCS))
            {
                connection.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(dbCheck, connection))
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (!dr.HasRows)
                        {
                            CreateDB();
                            CreateServiceTables();
                            log.Info($"Database and Tables were completed");
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"Error during database check : {ex.Message}");
                }

            }
        }

        /// <summary>
        /// create the tables within the sql express
        /// using a list to create layout create and alter queries 
        /// </summary>
        public void CreateServiceTables()
        {
            List<string> sqlCreateTable = new List<string>()
            {
                "DROP TABLE if EXISTS [dbo].[DrainAudit]","DROP TABLE if EXISTS [dbo].[LaundryAudit]","DROP TABLE if EXISTS [dbo].[WarewashAudit]","DROP TABLE if EXISTS [dbo].[MediaCollection]","DROP TABLE if EXISTS [dbo].[ServiceTech]",
                "CREATE TABLE [dbo].[Contacts]([objId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[Contact] [nvarchar](50) NOT NULL,[Email] [nvarchar](max) NOT NULL,[Department] [nvarchar](50) NULL,[Title] [nvarchar](50) NULL,[Phone] [nvarchar](15) NULL,[ChemicalType] [nvarchar](15) NOT NULL,PRIMARY KEY CLUSTERED ([objId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[Customers]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NULL,[AccountName] [nvarchar](max) NULL,[Contact] [nvarchar](max) NULL,[Phone] [nvarchar](20) NULL,[Email] [nvarchar](max) NULL,[InsideSales] [nvarchar](max) NULL,[InsideSaleEmail] [nvarchar](max) NULL,[SalesRep] [nvarchar](max) NULL,[SaleRepEmail] [nvarchar](max) NULL,[ServiceTech] [nvarchar](max) NULL,[ServiceTechEmail] [nvarchar](max) NULL,[CustomerServiceRep] [nvarchar](max) NULL,[CustomerServiceRepEmail] [nvarchar](max) NULL,[Nickname] [nvarchar](100) NULL,CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[DrainAudit]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[WorkOrder] [nvarchar](50) NOT NULL,[Visit] [nvarchar](75) NOT NULL,[Pump] [bit] NOT NULL,[TubeChangeLast] [date] NOT NULL,[BatteryChangeLast] [date] NOT NULL,[Pail] [bit] NOT NULL,[Odors] [bit] NOT NULL,[Blockage] [bit] NOT NULL,[Floor] [bit] NOT NULL,[Sink] [bit] NOT NULL,[Notes] [nvarchar](max) NULL,[AuditPdf] [nvarchar](150) NULL,[AuditTech] [nvarchar](100) NOT NULL,[TechSignature] [nvarchar](max) NOT NULL,[AuditContact] [nvarchar](100) NOT NULL,[CustSignature] [nvarchar](max) NOT NULL,[AuditCreation] [datetime] NOT NULL,CONSTRAINT [PK_DrainAudit] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "ALTER TABLE [dbo].[DrainAudit] ADD  CONSTRAINT [DF_DrainAudit_AuditCreation]  DEFAULT (getdate()) FOR [AuditCreation]",
                "CREATE TABLE [dbo].[LaundryAudit]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[WorkOrder] [nvarchar](50) NOT NULL,[Visit] [nvarchar](50) NOT NULL,[FabricAppearance] [nvarchar](50) NULL,[FabricFinal] [decimal](18, 2) NULL,[FabricOdor] [nvarchar](50) NULL,[FabricFeel] [nvarchar](50) NULL,[FabricStain] [nvarchar](50) NULL,[FabricColor] [nvarchar](50) NULL,[ProcedureCollection] [bit] NULL,[ProcedureSorting] [bit] NULL,[ProcedurePre] [bit] NULL,[ProcedureLoading] [bit] NULL,[ProcedureReclaim] [bit] NULL,[ProcedureReject] [bit] NULL,[Notes] [nvarchar](max) NULL,[AuditPdf] [nvarchar](150) NULL,[AuditTech] [nvarchar](100) NOT NULL,[TechSignature] [nvarchar](max) NOT NULL,[AuditContact] [nvarchar](100) NOT NULL,[CustSignature] [nvarchar](max) NOT NULL,[AuditCreation] [datetime] NOT NULL,CONSTRAINT [PK_LaundryAudit] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON[PRIMARY] TEXTIMAGE_ON[PRIMARY]",
                "ALTER TABLE [dbo].[LaundryAudit] ADD  CONSTRAINT [DF_LaundryAudit_AuditCreation]  DEFAULT (getdate()) FOR [AuditCreation]",
                "CREATE TABLE [dbo].[LaundryMachineList]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[MachineNo] [int] NOT NULL,[MachineMake] [nvarchar](50) NOT NULL,[MachineModel] [nvarchar](50) NOT NULL,[MachineSerial] [nvarchar](50) NOT NULL,[MachineCapacity] [nvarchar](50) NOT NULL,[DispenserMake] [nvarchar](50) NULL,[DispenserModel] [nvarchar](50) NULL,[DispenserSerial] [nvarchar](50) NULL,[NoPump] [int] NOT NULL,[Level] [nvarchar](50) NOT NULL,[Temp] [int] NOT NULL,[Valves] [nvarchar](50) NOT NULL,[Agitation] [nvarchar](50) NOT NULL,[Timer] [nvarchar](50) NOT NULL,[Ph] [decimal](18, 2) NOT NULL,[Bleach] [decimal](18, 2) NOT NULL,[Iron] [nchar](4) NOT NULL,[Chlorine] [int] NOT NULL,[WateHardness] [int] NOT NULL,[MachineExpirations] [datetime] NULL,[MachineCreation] [datetime] NOT NULL,CONSTRAINT [PK_LaundryMachineList] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[LaundryMachineList] ADD  CONSTRAINT [DF_LaundryMachineList_MachineCreation]  DEFAULT (getdate()) FOR [MachineCreation]",
                "CREATE TABLE [dbo].[MachineMedia]([ObjId] [int] IDENTITY(1,1) NOT NULL,[MachineNo] [int] NULL,[SerialNo] [nvarchar](150) NULL,[CustNo] [nvarchar](50) NOT NULL,[Name] [nvarchar](125) NOT NULL,[MediaType] [nvarchar](100) NOT NULL,[Creation] [datetime] NOT NULL,PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[MachineMedia] ADD  DEFAULT (getdate()) FOR [Creation]",
                "CREATE TABLE [dbo].[ServiceTechs]([ObjId] [int] IDENTITY(1,1) NOT NULL,[TechId] [nvarchar](50) NOT NULL,[Name] [nvarchar](100) NOT NULL,[Email] [nvarchar](125) NULL,[Password] [nvarchar](50) NULL,[Phone] [nvarchar](20) NULL,[Retired] [bit] NOT NULL,CONSTRAINT [PK_ServiceTech] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[ServiceTechs] ADD  CONSTRAINT [DF_ServiceTech_Retired]  DEFAULT ((0)) FOR [Retired]",
                "CREATE TABLE [dbo].[CustomerAddresses]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](max) NULL,[ShipNo] [nvarchar](10) NULL,[Name] [nvarchar](max) NULL,[Atn] [nvarchar](75) NULL,[Address1] [nvarchar](max) NULL,[Address2] [nvarchar](max) NULL,[City] [nvarchar](max) NULL,[State] [nvarchar](2) NULL,[Zip] [nvarchar](12) NULL,[Phone] [nvarchar](20) NULL,CONSTRAINT [PK_Shipping_Address] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[WarewashAudit]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[WorkOrder] [nvarchar](50) NOT NULL,[SerialNo] [nvarchar](50) NOT NULL,[Visit] [nvarchar](50) NOT NULL,[WashArms] [nvarchar](50) NULL,[RinseJets] [nvarchar](50) NULL,[RinseValves] [nvarchar](50) NULL,[OverFlow] [nvarchar](50) NULL,[ByPass] [nvarchar](50) NULL,[FinalRinsePsi] [int] NULL,[PumpMotor] [nvarchar](50) NULL,[FillValve] [nvarchar](50) NULL,[Drains] [nvarchar](50) NULL,[TempGauges] [nvarchar](50) NULL,[Curtains] [nvarchar](50) NULL,[Racks] [nvarchar](50) NULL,[Doors] [nvarchar](50) NULL,[Detergent] [nvarchar](50) NULL,[DetergentDrops] [int] NULL,[RinseAid] [nvarchar](50) NULL,[RinseAidDrops] [int] NULL,[Sanitizer] [nvarchar](50) NULL,[SanitizerDrops] [int] NULL,[WaterHardness] [int] NULL,[WaterHardnessDrops] [int] NULL,[Notes] [nvarchar](max) NULL,[AuditPdf] [nvarchar](150) NULL,[AuditContact] [nvarchar](100) NOT NULL,[CustomerSignature] [nvarchar](max) NOT NULL,[AuditTech] [nvarchar](100) NOT NULL,[TechSignature] [nvarchar](max) NOT NULL,[AuditCreation] [datetime] NOT NULL,CONSTRAINT [PK_WarewashAudit] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "ALTER TABLE [dbo].[WarewashAudit] ADD  CONSTRAINT [DF_WarewashAudit_AuditCreation]  DEFAULT (getdate()) FOR [AuditCreation]",
                "CREATE TABLE [dbo].[WarewashMachineList]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[MachineNo] [int] NOT NULL,[MachineMake] [nvarchar](50) NOT NULL,[MachineModel] [nvarchar](50) NOT NULL,[MachineSerial] [nvarchar](50) NOT NULL,[location] [nvarchar](150) NULL,[DispenserMake] [nvarchar](50) NULL,[DispenserModel] [nvarchar](50) NULL,[DispenserSerial] [nvarchar](50) NULL,[Voltage] [int] NULL,[Scraping] [bit] NOT NULL,[Racking] [bit] NOT NULL,[FlatwareSoak] [bit] NOT NULL,[MachineWater] [bit] NOT NULL,[PrewashTank] [int] NOT NULL,[WashTank] [int] NOT NULL,[RinseTank] [int] NOT NULL,[TempType] [bit] NOT NULL,[Temp] [int] NOT NULL,[MachineExpiration] [datetime] NULL,[MachineCreation] [datetime] NOT NULL,CONSTRAINT [PK_WarewashMachineList] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[WarewashMachineList] ADD  CONSTRAINT [DF_WarewashMachineList_MachineCreation]  DEFAULT (getdate()) FOR [MachineCreation]"
            };
            using (SqlConnection connection = new SqlConnection(localCS))
            {
                connection.Open();

                foreach (var sqlTable in sqlCreateTable)
                {
                    string dbUse = "USE [JPService] " + sqlTable;
                    try
                    {
                        using (SqlCommand command = new SqlCommand(dbUse, connection))
                        {
                            command.ExecuteNonQuery();
                            log.Info($"Table creation/alter : {sqlTable} was successful");
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Table creation error : {sqlTable} / error : {ex.Message}");
                    }
                }
            }
        }
        /// <summary>
        /// Create database 
        /// </summary>
        public void CreateDB()
        {
            var createDB = "CREATE DATABASE [JPService]";
            using (SqlConnection connection = new SqlConnection(localCS))
            {
                connection.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(createDB, connection))
                    {
                        command.ExecuteNonQuery();
                        log.Info($"Database has been created successfully");
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"Error during creating db : {ex.Message}");
                }
            }
        }

        public void UpdatePriorityDBAsync()
        {
            List<string> tblNames = new List<string>()
            {
                "Contacts","Customers","ServiceTechs","MachineMedia", "CustomerAddresses",
            };
            List<string> tblCreates = new List<string>()
            {
                "ALTER TABLE [LaundryMachineList] ALTER COLUMN Iron nchar(5) NOT NULL", "ALTER TABLE WarewashAudit DROP COLUMN IF EXISTS WaterHardnessDrops",
                "Drop Table If Exists [dbo].[Contacts] CREATE TABLE [dbo].[Contacts]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[Contact] [nvarchar](50) NOT NULL,[Email] [nvarchar](max) NOT NULL,[Department] [nvarchar](50) NULL,[Title] [nvarchar](50) NULL,[Phone] [nvarchar](15) NULL,[ChemicalType] [nvarchar](15) NOT NULL,PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[Customers]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NULL,[AccountName] [nvarchar](max) NULL,[Contact] [nvarchar](max) NULL,[Phone] [nvarchar](20) NULL,[Email] [nvarchar](max) NULL,[InsideSales] [nvarchar](max) NULL,[InsideSaleEmail] [nvarchar](max) NULL,[SalesRep] [nvarchar](max) NULL,[SaleRepEmail] [nvarchar](max) NULL,[ServiceTech] [nvarchar](max) NULL,[ServiceTechEmail] [nvarchar](max) NULL,[CustomerServiceRep] [nvarchar](max) NULL,[CustomerServiceRepEmail] [nvarchar](max) NULL,[Nickname] [nvarchar](100) NULL,CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[MachineMedia]([ObjId] [int] IDENTITY(1,1) NOT NULL,[MachineNo] [int] NULL,[SerialNo] [nvarchar](150) NULL,[CustNo] [nvarchar](50) NOT NULL,[Name] [nvarchar](125) NOT NULL,[MediaType] [nvarchar](100) NOT NULL,[Creation] [datetime] NOT NULL,PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[MachineMedia] ADD  DEFAULT (getdate()) FOR [Creation]",
                "CREATE TABLE [dbo].[ServiceTechs]([ObjId] [int] IDENTITY(1,1) NOT NULL,[TechId] [nvarchar](50) NOT NULL,[Name] [nvarchar](100) NOT NULL,[Email] [nvarchar](125) NULL,[Password] [nvarchar](50) NULL,[Phone] [nvarchar](20) NULL,[Retired] [bit] NOT NULL,CONSTRAINT [PK_ServiceTech] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[ServiceTechs] ADD  CONSTRAINT [DF_ServiceTech_Retired]  DEFAULT ((0)) FOR [Retired]",
                "CREATE TABLE [dbo].[CustomerAddresses]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](max) NULL,[ShipNo] [nvarchar](10) NULL,[Name] [nvarchar](max) NULL,[Atn] [nvarchar](75) NULL,[Address1] [nvarchar](max) NULL,[Address2] [nvarchar](max) NULL,[City] [nvarchar](max) NULL,[State] [nvarchar](2) NULL,[Zip] [nvarchar](12) NULL,[Phone] [nvarchar](20) NULL,CONSTRAINT [PK_Shipping_Address] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='EmergencyAudit' ) Begin CREATE TABLE  [dbo].[EmergencyAudit]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[WorkOrder] [nvarchar](50) NOT NULL,[Notes] [nvarchar](max) NOT NULL,[AuditPdf] [nvarchar](150) NULL,[AuditTech] [nvarchar](100) NOT NULL,[AuditTechSignature] [nvarchar](max) NOT NULL,[AuditContact] [nvarchar](100) NOT NULL,[AuditContactSignature] [nvarchar](max) NOT NULL,[AuditCreation] [nvarchar](max) NOT NULL,CONSTRAINT [PK_EmergencyAudit] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] ALTER TABLE [dbo].[EmergencyAudit] ADD CONSTRAINT [DF_EmergencyAudit_AuditCreation]  DEFAULT (getdate()) FOR [AuditCreation] End"
            };

            using (SqlConnection connection = new SqlConnection(localCS))
            {
                try
                {
                    connection.Open();
                    foreach (var tblName in tblNames)
                    {
                        var dropTable = $"DROP TABLE IF EXISTS dbo.{tblName}";
                        using (SqlCommand command = new SqlCommand(dropTable, connection))
                        {
                            command.ExecuteNonQuery();
                        }
                        log.Info($"Table Dropped : {tblName}");
                    }

                    foreach (var tblCreate in tblCreates)
                    {
                        using (SqlCommand command = new SqlCommand(tblCreate, connection))
                        {
                            command.ExecuteNonQuery();
                        }
                        log.Info($"Table Recreated : {tblCreate}");
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"Error Updating table Content : {ex.Message}");
                }
            }
        }

        public async Task ServiceConsumeAsync(string tblInsert)
        { 
            var webUrlApi = $"https://johnstonapi20190719083104.azurewebsites.net/api/service/{tblInsert}";
            //var webUrlApi = $"https://localhost:44366/api/service/{tblInsert}";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage httpResponse = client.GetAsync(webUrlApi).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    using (HttpContent content = httpResponse.Content)
                    {
                        var tableResponse = await httpResponse.Content.ReadAsStringAsync();
                        tableResponse = Regex.Unescape(tableResponse.Trim('"'));
                        JsonSyncParsed(tblInsert, tableResponse);
                    }
                }
                log.Info($"Successful table updateed : {tblInsert}");
            }
            catch (Exception ex)
            {
                log.Error($"Error Deserializing: {ex.Message}");
            }
        }

        private void JsonSyncParsed(string tblInsert, string tableResponse)
        {
            JArray jsonArray = JArray.Parse(tableResponse);
            StringBuilder sb = new StringBuilder();
            List<string> sqlSyntax = new List<string>();
            List<string> sqlParams = new List<string>();
            var commaDelimited = string.Empty;
            var commaDelimitedParams = string.Empty;

            try
            {
                switch (tblInsert)
                {
                    case "Customers":
                        var custTableParse = jsonArray.ToObject<List<Customers>>();
                        Customers custs = new Customers();
                        foreach (var cust in custs.GetType().GetProperties())
                        {
                            sqlSyntax.Add(cust.Name);
                            sqlParams.Add($"@{cust.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var custParsed in custTableParse)
                        {
                            using (SqlConnection connection = new SqlConnection(localCS))
                            {
                                connection.Open();
                                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", custParsed.CustNo);
                                    command.Parameters.AddWithValue("@AccountName", custParsed.AccountName);
                                    command.Parameters.AddWithValue("@Contact", custParsed.Contact);
                                    command.Parameters.AddWithValue("@Phone", custParsed.Phone);
                                    if (custParsed.Email == null)
                                    {
                                        command.Parameters.AddWithValue("@Email", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@Email", custParsed.Email);
                                    }
                                    if (custParsed.InsideSales == null)
                                    {
                                        command.Parameters.AddWithValue("@InsideSales", DBNull.Value);
                                        command.Parameters.AddWithValue("@InsideSaleEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@InsideSales", custParsed.InsideSales);
                                        command.Parameters.AddWithValue("@InsideSaleEmail", custParsed.InsideSaleEmail);
                                    }


                                    command.Parameters.AddWithValue("@SalesRep", custParsed.SalesRep);
                                    command.Parameters.AddWithValue("@SaleRepEmail", custParsed.SaleRepEmail);
                                    command.Parameters.AddWithValue("@ServiceTech", custParsed.ServiceTech);
                                    if (custParsed.ServiceTechEmail == null)
                                    {
                                        command.Parameters.AddWithValue("@ServiceTechEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@ServiceTechEmail", custParsed.ServiceTechEmail);
                                    }
                                    
                                    command.Parameters.AddWithValue("@CustomerServiceRep", custParsed.CustomerServiceRep);
                                    if (custParsed.CustomerServiceRepEmail == null)
                                    {
                                        command.Parameters.AddWithValue("@CustomerServiceRepEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@CustomerServiceRepEmail", custParsed.CustomerServiceRepEmail);
                                    }
                                    
                                    if (custParsed.Nickname == null)
                                    {
                                        command.Parameters.AddWithValue("@Nickname", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@Nickname", custParsed.Nickname);
                                    }

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "Contacts":
                        var contactTableParse = jsonArray.ToObject<List<Contacts>>();
                        Contacts contacts = new Contacts();
                        foreach (var contact in contacts.GetType().GetProperties())
                        {
                            sqlSyntax.Add(contact.Name);
                            sqlParams.Add($"@{contact.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var contactParsed in contactTableParse)
                        {
                            using (SqlConnection connection = new SqlConnection(localCS))
                            {
                                connection.Open();
                                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", contactParsed.CustNo);
                                    command.Parameters.AddWithValue("@Contact", contactParsed.Contact);
                                    command.Parameters.AddWithValue("@Email", contactParsed.Email);
                                    command.Parameters.AddWithValue("@Department", contactParsed.Department);
                                    command.Parameters.AddWithValue("@Title", contactParsed.Title);
                                    command.Parameters.AddWithValue("@Phone", contactParsed.Phone);
                                    command.Parameters.AddWithValue("@ChemicalType", contactParsed.ChemicalType);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "MachineMedia":
                        var mediaTableParse = jsonArray.ToObject<List<MachineMedia>>();
                        MachineMedia machineMedia = new MachineMedia();
                        foreach (var mMedia in machineMedia.GetType().GetProperties())
                        {
                            sqlSyntax.Add(mMedia.Name);
                            sqlParams.Add($"@{mMedia.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var mediaParsed in mediaTableParse)
                        {
                            using (SqlConnection connection = new SqlConnection(localCS))
                            {
                                connection.Open();
                                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", mediaParsed.CustNo);
                                    command.Parameters.AddWithValue("@MachineNo", mediaParsed.MachineNo);
                                    command.Parameters.AddWithValue("@SerialNo", mediaParsed.SerialNo);
                                    command.Parameters.AddWithValue("@Name", mediaParsed.Name);
                                    command.Parameters.AddWithValue("@MediaType", mediaParsed.MediaType);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "CustomerAddresses":
                        var addressTableParse = jsonArray.ToObject<List<CustomerAddresses>>();
                        CustomerAddresses addresses = new CustomerAddresses();
                        foreach (var cAddress in addresses.GetType().GetProperties())
                        {
                            sqlSyntax.Add(cAddress.Name);
                            sqlParams.Add($"@{cAddress.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var addressParsed in addressTableParse)
                        {
                            using (SqlConnection connection = new SqlConnection(localCS))
                            {
                                connection.Open();

                                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", addressParsed.CustNo);
                                    command.Parameters.AddWithValue("@ShipNo", addressParsed.ShipNo);
                                    command.Parameters.AddWithValue("@Name", addressParsed.Name);
                                    command.Parameters.AddWithValue("@Atn", addressParsed.Atn);
                                    command.Parameters.AddWithValue("@Address1", addressParsed.Address1);
                                    command.Parameters.AddWithValue("@Address2", addressParsed.Address2);
                                    command.Parameters.AddWithValue("@City", addressParsed.City);
                                    command.Parameters.AddWithValue("@State", addressParsed.State);
                                    command.Parameters.AddWithValue("@Zip", addressParsed.Zip);
                                    command.Parameters.AddWithValue("@Phone", addressParsed.Phone);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "ServiceTechs":
                        var techTableParse = jsonArray.ToObject<List<ServiceTechs>>();
                        ServiceTechs techs = new ServiceTechs();
                        foreach (var tech in techs.GetType().GetProperties())
                        {
                            sqlSyntax.Add(tech.Name);
                            sqlParams.Add($"@{tech.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = String.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");

                        foreach (var techParsed in techTableParse)
                        {
                            using (SqlConnection connection = new SqlConnection(localCS))
                            {
                                connection.Open();

                                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@TechId", techParsed.TechId);
                                    command.Parameters.AddWithValue("@Name", techParsed.Name);
                                    command.Parameters.AddWithValue("@Email", techParsed.Email);
                                    if (techParsed.Phone == null)
                                    {
                                        command.Parameters.AddWithValue("@Phone", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@Phone", techParsed.Phone);
                                    }

                                    command.Parameters.AddWithValue("@Retired", techParsed.Retired);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                }

                log.Info($"Successful update : {tblInsert}");
            }
            catch (Exception ex)
            {
                log.Error($"Updating DB failed : {tblInsert} / {ex.Message}");
            }
        }

        public async Task ServicePostAsync(string localTable)
        {
            var getLocalData = string.Empty;
            if (localTable == "Customers")
                getLocalData = $"SELECT [CustNo], [Nickname] FROM {localTable} WHERE [Nickname] IS NOT NULL";
            else
                getLocalData = $"SELECT * FROM {localTable}";

            var sqlJsonResponse = string.Empty;
            var webUrlApiP = $"https://johnstonapi20190719083104.azurewebsites.net/api/service/{localTable}";
            //var webUrlApiP = $"https://localhost:44366/api/service/{localTable}";
            DataTable dt = new DataTable();

            using (SqlConnection connection = new SqlConnection(localCS))
            {
                connection.Open();
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(getLocalData, connection))
                    {
                        da.Fill(dt);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        var generateResponse = JsonConvert.SerializeObject(dt);

                        using (var client = new HttpClient())
                        using (var request = new HttpRequestMessage(HttpMethod.Post, webUrlApiP))
                        {
                            using (var stringContent = new StringContent(generateResponse, Encoding.UTF8, "application/json"))
                            {
                                request.Content = stringContent;

                                using (var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false))
                                {
                                    response.EnsureSuccessStatusCode();
                                    log.Info($"Table collect complete and sent : {localTable}");
                                }
                            }
                        }
                    } 
                }
                catch (HttpRequestException ex)
                {
                    log.Error($"Error compiling post request : {ex.InnerException.Message}");
                }
            }
        }

    }

}
