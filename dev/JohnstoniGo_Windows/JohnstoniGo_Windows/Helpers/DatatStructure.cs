﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JohnstoniGo_Windows.Helpers
{

    
    public class ServiceTechs
    {
        public int ObjId { get; set; }
        public string TechId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public object Phone { get; set; }
        public bool Retired { get; set; }
    }

    public class Customers
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string AccountName { get; set; }
        public string Contact { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string InsideSales { get; set; }
        public string InsideSaleEmail { get; set; }
        public string SalesRep { get; set; }
        public string SaleRepEmail { get; set; }
        public string ServiceTech { get; set; }
        public string ServiceTechEmail { get; set; }
        public string CustomerServiceRep { get; set; }
        public string CustomerServiceRepEmail { get; set; }
        public string Nickname { get; set; }
    }

    public class CustomerAddresses
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string ShipNo { get; set; }
        public string Name { get; set; }
        public string Atn { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
    }

    public class Contacts
    {
        public int objId { get; set; }
        public string CustNo { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string ChemicalType { get; set; }
    }

    public class MachineMedia
    {
        public int ObjId { get; set; }
        public int MachineNo { get; set; }
        public string SerialNo { get; set; }
        public string CustNo { get; set; }
        public string Name { get; set; }
        public string MediaType { get; set; }
    }

}
