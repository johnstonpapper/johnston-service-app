﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using log4net;

using OutlookApp = Microsoft.Office.Interop.Outlook;

namespace JohnstoniGo_Windows.Helpers
{
    class EmailData
    {

        string emailString = ConfigurationManager.ConnectionStrings["johnstonAmazon"].ConnectionString;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a">Outlook attachment for audit</param>
        /// <param name="b">Outlook body message</param>
        /// <param name="c">Outlook send to contact</param>
        /// <param name="s">Outlook subject</param>
        public void isOutlook(string a, string b, string c, string s)
        {
            var attachAudit = $@"C:\temp\PDF\{a}";
            try
            {
                var ol = new OutlookApp.Application();
                OutlookApp.MailItem mail = ol.CreateItem(OutlookApp.OlItemType.olMailItem) as OutlookApp.MailItem;

                mail.To = c;
                mail.Subject = $"Johnston {s} - {DateTime.Now.Date.ToShortDateString()}";
                mail.Body = $"I was in today to service your equipment and wanted to make certain you had a copy of my report. Please feel free to contact me if I can answer questions or be of any further assistance. " +
                    $"\r\n Thank you for the opportunity to be of service and your support!";
                mail.Attachments.Add(attachAudit);

                mail.Display(true);
            }
            catch (Exception ex)
            {
                log.Error($"Error accessing outlook : {ex.Message}");   
            }
        }

        public string sendCompletedAudit(string auditAttachment)
        {
            string mailingStatus = "";

            try
            {
                SmtpClient client = new SmtpClient("");

                client.Port = 587;
                client.Credentials = new NetworkCredential("", "");
                client.EnableSsl = true;

                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("");
                mail.To.Add("");
                mail.Subject = "";
                mail.Body = "";

                Attachment attach = new Attachment(auditAttachment);

                client.Send(mail);

                mailingStatus = "Mail Sent";

            } catch (Exception ex)
            {
                mailingStatus = ex.Message;
            }

            return mailingStatus;

        }

        public string ThereAnError(string errorMsg, string errorWhere)
        {
            string msgSendStatus;
            try
            {
                SmtpClient client = new SmtpClient("http://apollo.johnstonpaper.com/");
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress("DoNotReply@johnston.biz", "Johnston IT");
                mail.To.Add("dauris.little@johnston.biz");
                mail.CC.Add("dave.colbert@johnston.biz, David.May@johnston.biz");

                mail.Subject = "There is a n error " + errorWhere;
                mail.Body = errorMsg;

                client.Port = 25;
                client.UseDefaultCredentials = true;
                //client.Credentials = new NetworkCredential("","");
                client.EnableSsl = true;

                client.Send(mail);
                msgSendStatus = "Success";
            } catch (Exception ex)
            {
                msgSendStatus = ex.Message;
            }

            return msgSendStatus;
        }

        void EmailError(string errorMsg, string errorPosition)
        {
            try
            {
                MailMessage errorMail = new MailMessage();
                SmtpClient errorClient = new SmtpClient();
                errorClient.Host = "smtp.office365.com";

                errorMail.From = new MailAddress("dauris.little@johnston.biz");
                errorMail.To.Add("dauris.little@johnston.biz");
                errorMail.Subject = "Service App Error";
                errorMail.Body = "There is an error within the service application list as: " + errorMsg;

                errorClient.Port = 587;
                errorClient.UseDefaultCredentials = false;
                errorClient.Credentials = new NetworkCredential("dlittle@johnstonpaper.com", "daurislittle5504");
                errorClient.EnableSsl = true;

                errorClient.Send(errorMail);
            } catch(Exception ex)
            {
                log.Error("there is an error with the error email piece: " + ex.Message);
            }
        }

        public void EmailAudit(string customer, string pdfName,string tEmail, string auditSubject)
        {
            var getServiceEmail = $"SELECT Email FROM ServiceTech WHERE TechId = '{tEmail}'";
            try
            {
                using (SqlConnection connection = new SqlConnection(emailString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(getServiceEmail,connection))
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            var techMail = dr.GetString(0);
                            MailMessage mm = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            Attachment attachPdf = new Attachment("C://temp/PDF/" + pdfName);
                            client.Host = "smtp.office365.com";

                            mm.From = new MailAddress(techMail);
                            mm.To.Add(customer);
                            mm.Subject = auditSubject;
                            mm.Body = "Please see attached document to review your completed Audit.";
                            mm.IsBodyHtml = true;
                            mm.Attachments.Add(attachPdf);

                            client.Port = 587;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new NetworkCredential("dlittle@johnstonpaper.com", "daurislittle5504");
                            client.EnableSsl = true;

                            client.Send(mm);
                        }
                    }
                }
                
            } catch(Exception ex)
            {
                EmailError(ex.Message, "Within the email area");
                log.Error("there is an error with emailing audit:" + ex.Message);   
            }
        }
    }
}
