﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JohnstoniGo_Windows.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.1.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=PEGASUS\\JOHSQL;Initial Catalog=JP_Service;Persist Security Info=True;" +
            "User ID=developer;Password=*J0hnst0nD3vGr0up*")]
        public string johnstonRemote {
            get {
                return ((string)(this["johnstonRemote"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=johnstonpaper.database.windows.net;Initial Catalog=JPService;Persist " +
            "Security Info=True;User ID=jpAdmin;Password=9@9^*dIiK6NP4PIMo907!A&dLTu08!z")]
        public string johnstonAzure {
            get {
                return ((string)(this["johnstonAzure"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=alphanomega.cdruqzrthcv2.us-east-2.rds.amazonaws.com;Initial Catalog=" +
            "johnstonService;Persist Security Info=True;User ID=johnstonService;Password=Serv" +
            "iceJohnston")]
        public string johnstonAmazon {
            get {
                return ((string)(this["johnstonAmazon"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=localhost\\VSYP1IB087ELQLX;Initial Catalog=JPService;Integrated Securi" +
            "ty=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;Applicatio" +
            "nIntent=ReadWrite;MultiSubnetFailover=False")]
        public string johnstonExpress {
            get {
                return ((string)(this["johnstonExpress"]));
            }
        }
    }
}
