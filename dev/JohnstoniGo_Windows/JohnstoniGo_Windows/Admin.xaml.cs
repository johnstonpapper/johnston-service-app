﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using JohnstoniGo_Windows.Helpers;
using log4net;

using OutlookApp = Microsoft.Office.Interop.Outlook;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using Exception = System.Exception;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace JohnstoniGo_Windows
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        AmazonUploader s3Finder = new AmazonUploader();
        pdfGenerator audit = new pdfGenerator();
        //string adminCS = ConfigurationManager.ConnectionStrings["johnstonAzure"].ConnectionString;
        string adminCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        DateTime todayDate = DateTime.Now;

        public Admin()
        {
            try
            {
                InitializeComponent();
                log4net.Config.XmlConfigurator.Configure();
                BindServiceTech(serviceTechCombo);
                BindServiceAudits("all");
            } catch (Exception ex)
            {
                
            }
        }

        private async void BindServiceAudits(string tech)
        {
            var webUrlApi = $"https://johnstonapi20190719083104.azurewebsites.net/api/adminservice/{tech}";
            //var webUrlApi = $"https://localhost:44366//api/AdminService/{tech}";
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage httpResponse = client.GetAsync(webUrlApi).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    using (HttpContent content = httpResponse.Content)
                    {
                        var tableResponse = await httpResponse.Content.ReadAsStringAsync();
                        tableResponse = Regex.Unescape(tableResponse.Trim('"'));

                        JArray arrayJ = JArray.Parse(tableResponse);

                        CompletedAudits.ItemsSource = arrayJ;
                        
                    }
                }
                log.Info($"Successful table updateed : {tech}");
            }
            catch (Exception ex)
            {
                log.Error($"Error Deserializing: {ex.Message}");
            }
        }

        private void BindServiceTech(ComboBox serviceTechCombo)
        {
            var serviceTechLoad = "SELECT TechId, Name FROM ServiceTechs WHERE TechId <> '135' AND TechId <> '161' AND TechId <> '500' ";
            DataTable dt = new DataTable();
            using (SqlConnection connection = new SqlConnection(adminCS))
            {
                connection.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(serviceTechLoad, connection))
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dt);
                        DataRow newDr = dt.NewRow();
                        newDr["TechId"] = 0;
                        newDr["Name"] = "All";

                        dt.Rows.InsertAt(newDr, 0);
                        serviceTechCombo.ItemsSource = dt.DefaultView;
                        serviceTechCombo.DisplayMemberPath = "Name";
                        serviceTechCombo.SelectedValuePath = "TechId";

                        //serviceTechCombo.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {

                }

            }
        }

        private void ServiceTechCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tech = serviceTechCombo.SelectedValue.ToString();
            BindServiceAudits(tech);
        }

        private void AlphaNomega_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("You are now entering God Mode");
            God godMode = new God();
            godMode.ShowDialog();
        }

        private async void CompletedAudits_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var auditDirectory = @"c:\temp\pdf\";
            var row = JObject.Parse(CompletedAudits.SelectedItem.ToString());
            var customerAudit = row.GetValue("CustNo").ToString();
            var workOrderAudit = row.GetValue("WorkOrder").ToString();
            var auditType = row.GetValue("AuditType").ToString();

            MessageBoxResult result = MessageBox.Show("Would you like to review the pdf file?", "Review Completed Audit", MessageBoxButton.YesNoCancel);
            Directory.CreateDirectory(auditDirectory);

            switch (result)
            {
                case MessageBoxResult.Yes:
                    var selectedResult = row.GetValue("AuditPdf").ToString();
                    if (selectedResult != "")
                    {
                        var adminTask = await Task.Run(() =>
                        {
                            s3Finder.DownloadAudit(selectedResult, auditDirectory);
                            return "Audit Request Completed";
                        });

                        log.Info(adminTask);
                    }
                    else
                    {
                        if (auditType == "Drain Audit")
                        {
                            audit.generateDrainPdf(customerAudit, workOrderAudit);
                        }
                        else if (auditType == "Laundry Audit")
                        {
                            audit.generateLaundryPdf(customerAudit, workOrderAudit);
                        }
                        else
                        {
                            audit.generateWarewashPdf(customerAudit, workOrderAudit);
                        }
                    }

                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private async void AdminResultGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var auditDirectory = @"c:\temp\pdf\";
            DataRowView row = AdminResultGrid.SelectedItem as DataRowView;
            var selectRow = row.Row.ItemArray[6].ToString();

            var adminEmailAudit = await Task.Run(() =>
            {
                return s3Finder.EamilDownloadedAudit(selectRow, auditDirectory);
            });

            isOutlook(adminEmailAudit);

        }

        private void isOutlook(string v)
        {
            try
            {
                var ol = new OutlookApp.Application();
                OutlookApp.MailItem mail = ol.CreateItem(OutlookApp.OlItemType.olMailItem) as OutlookApp.MailItem;

                mail.Subject = "test subject";
                mail.Body = "Test body message";
                mail.ReadReceiptRequested = true;
                mail.Attachments.Add(v);

                mail.Display(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AdminSearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var findAndEmail = $"SELECT [DrainAudit].[CustNo], [Customers].[AccountName], [DrainAudit].[WorkOrder], [DrainAudit].[AuditTech], 'Drain Audit' as 'AuditType' ,[DrainAudit].[AuditCreation], [DrainAudit].[AuditPdf] FROM Customers INNER JOIN DrainAudit ON [Customers].CustNo = [DrainAudit].[CustNo] INNER JOIN [ServiceTechs] ON [DrainAudit].[AuditTech] = [ServiceTechs].[Name] WHERE [DrainAudit].[CustNo] = '{AdminSearchBox.Text.Trim()}' OR [Customers].[AccountName] = '{AdminSearchBox.Text.Trim()}' OR [DrainAudit].[WorkOrder] = '{AdminSearchBox.Text.Trim()}' OR [DrainAudit].[AuditTech] = '{AdminSearchBox.Text.Trim()}' " +
                $"UNION ALL SELECT [LaundryAudit].[CustNo], [Customers].[AccountName], [LaundryAudit].[WorkOrder], [LaundryAudit].[AuditTech], 'Laundry Audit' as 'AuditType', [LaundryAudit].[AuditCreation], [LaundryAudit].[AuditPdf] FROM [Customers] INNER JOIN [LaundryAudit] ON [Customers].[CustNo] = [LaundryAudit].[CustNo] INNER JOIN [ServiceTechs] ON [LaundryAudit].[AuditTech] = [ServiceTechs].[Name] WHERE [LaundryAudit].[CustNo] = '{AdminSearchBox.Text.Trim()}' OR [Customers].[AccountName] = '{AdminSearchBox.Text.Trim()}' OR [LaundryAudit].[WorkOrder] = '{AdminSearchBox.Text.Trim()}' OR [LaundryAudit].[AuditTech] = '{AdminSearchBox.Text.Trim()}' " +
                $"UNION ALL SELECT [WarewashAudit].[CustNo], [Customers].[AccountName], [WarewashAudit].[WorkOrder], [WarewashAudit].[AuditTech], 'Warewash Audit' as 'AuditType', [WarewashAudit].[AuditCreation], [WarewashAudit].[AuditPdf] FROM [Customers] INNER JOIN [WarewashAudit] ON [Customers].[CustNo] = [WarewashAudit].[CustNo] INNER JOIN [ServiceTechs] ON [WarewashAudit].[AuditTech] = [ServiceTechs].[Name] WHERE [WarewashAudit].[CustNo] = '{AdminSearchBox.Text.Trim()}' OR [Customers].[AccountName] = '{AdminSearchBox.Text.Trim()}' OR [WarewashAudit].[WorkOrder] = '{AdminSearchBox.Text.Trim()}' OR [WarewashAudit].[AuditTech] = '{AdminSearchBox.Text.Trim()}' ";

                using (SqlConnection connect = new SqlConnection(adminCS))
                {
                    connect.Open();
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand(findAndEmail, connect))
                        {
                            using (SqlDataAdapter searchAdapt = new SqlDataAdapter(cmd))
                            {
                                DataTable adminSearchResults = new DataTable();
                                searchAdapt.Fill(adminSearchResults);
                                AdminResultGrid.ItemsSource = adminSearchResults.DefaultView;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex.Message);
                    }
                }
            }
        }

        string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;
        private void ExecuteSql_Click(object sender, RoutedEventArgs e)
        {
            

            using (SqlConnection connection = new SqlConnection(localCS))
            {
                var executeCmd = string.Empty;
                try
                {
                    executeCmd = sqlBox.Text;
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(executeCmd, connection))
                    {

                        command.ExecuteNonQuery();
                        
                    }
                    MessageBox.Show("Success");

                } catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
        }
    }

    public class AuditsCompleted
    {
        public string CustNo { get; set; }
        public string AccountName { get; set; }
        public string WorkOrder { get; set; }
        public string Visit { get; set; }
        public string AuditTech { get; set; }
        public string AuditType { get; set; }
        public DateTime AuditCreation { get; set; }
        public string AuditPdf { get; set; }
    }

   
}
