﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JohnstoniGo_Windows
{
    /// <summary>
    /// Interaction logic for God.xaml
    /// </summary>
    public partial class God : Window
    {
        string remoteCS = ConfigurationManager.ConnectionStrings["johnstonAzure"].ConnectionString;
        string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;
        public God()
        {
            InitializeComponent();
        }

        private void SqlExecute_Click(object sender, RoutedEventArgs e)
        {
            
            var connectSql = string.Empty;
            var sqlRequest = commandSql.Text.Trim();
            DataTable dt = new DataTable();

            if (localDB.IsChecked == true)
                connectSql = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;
            else
            {
                RemoteAccessToCloud(sqlRequest);
                return;
            }
                

            using (SqlConnection connection = new SqlConnection(connectSql))
            {
                connection.Open();
                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(sqlRequest, connection))
                    {
                        da.Fill(dt);
                    }
                    sqlResults.ItemsSource = dt.DefaultView;
                } catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private async void RemoteAccessToCloud(string sqlRequest)
        {
            var webUrlApi = $"https://johnstonapi20190719083104.azurewebsites.net/api/God/{sqlRequest}";
            //var webUrlApi = $"https://localhost:44366/api/God/{sqlRequest}";

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage httpResponse = client.GetAsync(webUrlApi).Result;
                httpResponse.EnsureSuccessStatusCode();
                using (HttpContent content = httpResponse.Content)
                {
                    var tableResponse = await httpResponse.Content.ReadAsStringAsync();
                    tableResponse = Regex.Unescape(tableResponse.Trim('"'));

                    JArray arrayJ = JArray.Parse(tableResponse);

                    sqlResults.ItemsSource = arrayJ;
                }
            }
            //log.Info($"Successful table updated : {tech}");
        }
    }
}
