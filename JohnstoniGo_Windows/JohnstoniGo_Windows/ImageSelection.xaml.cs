﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;


//image capture
using AForge.Video;
using AForge.Video.DirectShow;
using System.Windows.Media.Imaging;
using JohnstoniGo_Windows.Helpers;
using System.Collections.ObjectModel;
using System.Drawing;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using Image = System.Windows.Controls.Image;
using System.Data.SqlClient;
using System.Configuration;
using log4net;

namespace JohnstoniGo_Windows
{
    /// <summary>
    /// Interaction logic for ImageSelection.xaml
    /// </summary>
    public partial class ImageSelection : Window, INotifyPropertyChanged
    {
        #region public props

        public ObservableCollection<FilterInfo> VideoDevices { get; set; }

        public FilterInfo CurrentDevice
        {
            get { return _currentDevice; }
            set { _currentDevice = value; this.OnPropertyChanged("CurrentDevice"); }
        }

        private FilterInfo _currentDevice;
        private BitmapImage _image;

        #endregion

        #region windows eyes only
        private IVideoSource _videoSource;
        #endregion

        private const string Letters = "abcdefghijklmnopqrstuvwxyz";
        private readonly char[] AlphaNumeric = (Letters + Letters.ToUpper() + "1234567890").ToCharArray();
        private static readonly Regex _regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
        private int machineNumber;
        private string machineSerial;
        private string machineCustNo;

        //string mediaConnection = ConfigurationManager.ConnectionStrings["johnstonLocal"].ConnectionString;
        //string mediaConnection = ConfigurationManager.ConnectionStrings["johnstonRemote"].ConnectionString;
        //string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;
        string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;


        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ImageSelection(int v, string s, string cust)
        {
            InitializeComponent();
            getMediaDevices();
            startCamera();

            machineNumber = v;
            machineSerial = s;
            machineCustNo = cust;
            findMachineImages();
        }

        private void findMachineImages()
        {
            var machineSearch = $"SELECT Name FROM MachineMedia WHERE CustNo = '{machineCustNo}' AND SerialNo = '{machineSerial}' ORDER BY MachineNo ASC";
            List<string> machineResults = new List<string>();
            using (SqlConnection connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(machineSearch, connection))
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            machineResults.Add(dr.GetString(0));
                        }
                        try
                        {
                            for (int i = 0; i < machineResults.Capacity; i++)
                            {
                                var builderI = i + 1;
                                Image machineImageHolder = FindName("machine" + builderI.ToString()) as Image;
                                Bitmap file = new Bitmap(@"C:\temp\machines\images\" + machineResults[i]);
                                machineImageHolder.Source = file.toBitmapImg();
                                machineImageHolder.Tag = machineResults[i].ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("there is an error with finding previous images: " + ex.Message);
                        }
                    }
                }

            }

        }

        private void startCamera()
        {
            try
            {
                if (CurrentDevice != null)
                {
                    _videoSource = new VideoCaptureDevice(CurrentDevice.MonikerString);
                    _videoSource.NewFrame += videoFrame;
                    _videoSource.Start();
                }
            }
            catch (Exception ex)
            {
                log.Error("starting camera has failed " + ex.Message);
            }

        }

        private void StopCamera()
        {
            try
            {
                if (_videoSource != null && _videoSource.IsRunning)
                {
                    _videoSource.SignalToStop();
                    _videoSource.NewFrame -= new NewFrameEventHandler(videoFrame);
                }
            }
            catch (Exception ex)
            {
                log.Error("stopping camera failed with: " + ex.Message);
            }

        }

        private void videoFrame(object sender, NewFrameEventArgs eventArgs)
        {
            try
            {
                BitmapImage bi;
                using (var bmap = (Bitmap)eventArgs.Frame.Clone())
                {
                    bi = bmap.toBitmapImg();
                }
                bi.Freeze();
                Dispatcher.BeginInvoke(new ThreadStart(delegate { imgCapturer.Source = bi; }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                log.Error("video frame capture failed with: " + ex.Message);
            }
        }

        private void getMediaDevices()
        {
            try
            {
                VideoDevices = new ObservableCollection<FilterInfo>();
                foreach (FilterInfo info in new FilterInfoCollection(FilterCategory.VideoInputDevice))
                {
                    VideoDevices.Add(info);
                }
                if (VideoDevices.Any())
                {
                    CurrentDevice = VideoDevices[0];
                }
                else
                {
                    MessageBox.Show("no access for a video source");
                    log.Info("there is no video source usable");
                }
            }
            catch (Exception ex)
            {
                log.Error("collecting media devices error: " + ex.Message);
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public string snapShot(Image machineImg)
        {
            var s3Key = imgGenerate();
            var directoryPath = @"C:\temp\machines\images\";

            var encodeThis = new PngBitmapEncoder();

            encodeThis.Frames.Add(BitmapFrame.Create((BitmapSource)machineImg.Source));

            //save the ink to a memory stream
            BmpBitmapEncoder jpgEncode = new BmpBitmapEncoder();

            Directory.CreateDirectory(directoryPath);

            var s3Create = directoryPath + s3Key;
            FileStream file = File.Open(s3Create, FileMode.Create);

            encodeThis.Save(file);
            file.Close();

            //this is to save the image to s3 environment
            //confirmS3Upload = s3Access.saveSignaturesS3(s3Create, s3Key);

            return s3Key;
        }

        void mediaSave(string machineNames) // update image if they exist
        {
            var mediaCollection = "INSERT INTO [dbo].[MachineMedia]([CustNo],[Name],[MediaType],[MachineNo],[SerialNo]) VALUES (@CustNo,@Name,@MediaType,@MachineNo,@SerialNo)";

            /*
            if (!checkImageExist(machineNames))
            {

            }
            */
            using (SqlConnection connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(mediaCollection, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@CustNo", Port.customerNo);
                        command.Parameters.AddWithValue("@Name", machineNames);
                        command.Parameters.AddWithValue("@MediaType", "image/jpeg");
                        command.Parameters.AddWithValue("@MachineNo", machineNumber);
                        command.Parameters.AddWithValue("@SerialNo", machineSerial);

                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        log.Error("saving media content failed : " + ex.Message);
                    }
                }
            }
        }

        bool checkImageExist(string name)
        {
            bool imgResults = false;
            var mediaExist = $"SELECT ObjId FROM MachineMedia WHERE Name = '{machine1.Tag}'";

            using (SqlConnection connection = new SqlConnection(localCS))
            {
                SqlCommand command = new SqlCommand(mediaExist, connection);
                SqlDataReader dr = command.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    //var updateAudit = $"UPDATE {tableName} SET AuditPdf = @AuditPdf  WHERE CustNo = '{customerNo}' AND WO# = '{jpWorkOrder}'";
                    var updateMediaExist = $"UPDATE MachineMedia SET Name = @Name WHERE = '{dr.GetInt32(0)}' ";

                    using (SqlCommand cmd = new SqlCommand(updateMediaExist, connection))
                    {
                        cmd.Parameters.AddWithValue("@Name", name);

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                    imgResults = true;
                }
            }

            return imgResults;
        }
        void clearGroup(DependencyObject dependency)
        {
            Image img = dependency as Image;

            if (img.Source != null)
            {

            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(dependency as DependencyObject); i++)
            {
                clearGroup(VisualTreeHelper.GetChild(dependency, i));
            }
        }

        private string imgGenerate()
        {
            StringBuilder imgName = new StringBuilder();
            Random r = new Random();
            for (int i = 0; i < 75; i++)
            {
                imgName.Append(AlphaNumeric[r.Next(AlphaNumeric.Length)]);
            }
            return imgName.ToString() + ".jpg";
        }

        #region image capture and saves
        private void Machine1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StopCamera();
            machine1.Source = imgCapturer.Source;
            var mName = snapShot(machine1);
            mediaSave(mName);
            machine1.IsEnabled = false;
            startCamera();
        }

        private void Machine2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StopCamera();
            machine2.Source = imgCapturer.Source;
            var mName = snapShot(machine2);
            mediaSave(mName);
            machine2.IsEnabled = false;
            startCamera();
        }

        private void Machine3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StopCamera();
            machine3.Source = imgCapturer.Source;
            var mName = snapShot(machine3);
            mediaSave(mName);
            machine3.IsEnabled = false;
            startCamera();
        }

        private void Machine4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StopCamera();
            machine4.Source = imgCapturer.Source;
            var mName = snapShot(machine4);
            mediaSave(mName);
            machine4.IsEnabled = false;
            startCamera();
        }

        private void Machine5_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StopCamera();
            machine5.Source = imgCapturer.Source;
            var mName = snapShot(machine5);
            mediaSave(mName);
            machine5.IsEnabled = false;
            startCamera();
        }
        #endregion
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StopCamera();
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            StopCamera();
        }
    }
}
