﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace JohnstoniGo_Windows.Helpers
{
    static class BitmapAssist
    {
        public static BitmapImage toBitmapImg(this Bitmap bitmap)
        {
            BitmapImage bi = new BitmapImage();
            MemoryStream ms = new MemoryStream();

            bi.BeginInit();
            bitmap.Save(ms, ImageFormat.Bmp);
            ms.Seek(0, SeekOrigin.Begin);
            bi.StreamSource = ms;
            
            bi.EndInit();

            return bi;
        }
    }
}
