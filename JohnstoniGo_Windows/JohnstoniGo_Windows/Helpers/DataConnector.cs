﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using JohnstoniGo_Windows.Helpers;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using System.Net.Http.Headers;

namespace JohnstoniGo_Windows.Helpers
{
    class DataConnector
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string dataString = ConfigurationManager.ConnectionStrings["johnstonAmazon"].ConnectionString;
        string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;


        public void UpdatePriorityDBAsync()
        {
            List<string> tblNames = new List<string>()
            {
                "Contacts","Customers","ServiceTechs","MachineMedia", "CustomerAddresses",
            };
            List<string> tblCreates = new List<string>()
            {
                "ALTER TABLE [LaundryMachineList] ALTER COLUMN Iron nchar(5) NOT NULL", "ALTER TABLE WarewashAudit DROP COLUMN IF EXISTS WaterHardnessDrops",
                "Drop Table If Exists [dbo].[Contacts] CREATE TABLE [dbo].[Contacts]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[Contact] [nvarchar](50) NOT NULL,[Email] [nvarchar](max) NOT NULL,[Department] [nvarchar](50) NULL,[Title] [nvarchar](50) NULL,[Phone] [nvarchar](15) NULL,[ChemicalType] [nvarchar](15) NOT NULL,PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[Customers]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NULL,[AccountName] [nvarchar](max) NULL,[Contact] [nvarchar](max) NULL,[Phone] [nvarchar](20) NULL,[Email] [nvarchar](max) NULL,[InsideSales] [nvarchar](max) NULL,[InsideSaleEmail] [nvarchar](max) NULL,[SalesRep] [nvarchar](max) NULL,[SaleRepEmail] [nvarchar](max) NULL,[ServiceTech] [nvarchar](max) NULL,[ServiceTechEmail] [nvarchar](max) NULL,[CustomerServiceRep] [nvarchar](max) NULL,[CustomerServiceRepEmail] [nvarchar](max) NULL,[Nickname] [nvarchar](100) NULL,CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]",
                "CREATE TABLE [dbo].[MachineMedia]([ObjId] [int] IDENTITY(1,1) NOT NULL,[MachineNo] [int] NULL,[SerialNo] [nvarchar](150) NULL,[CustNo] [nvarchar](50) NOT NULL,[Name] [nvarchar](125) NOT NULL,[MediaType] [nvarchar](100) NOT NULL,[Creation] [datetime] NOT NULL,PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[MachineMedia] ADD  DEFAULT (getdate()) FOR [Creation]",
                "CREATE TABLE [dbo].[ServiceTechs]([ObjId] [int] IDENTITY(1,1) NOT NULL,[TechId] [nvarchar](50) NOT NULL,[Name] [nvarchar](100) NOT NULL,[Email] [nvarchar](125) NULL,[Password] [nvarchar](50) NULL,[Phone] [nvarchar](20) NULL,[Retired] [bit] NOT NULL,CONSTRAINT [PK_ServiceTech] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY]",
                "ALTER TABLE [dbo].[ServiceTechs] ADD  CONSTRAINT [DF_ServiceTech_Retired]  DEFAULT ((0)) FOR [Retired]",
                "CREATE TABLE [dbo].[CustomerAddresses]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](max) NULL,[ShipNo] [nvarchar](10) NULL,[Name] [nvarchar](max) NULL,[Atn] [nvarchar](75) NULL,[Address1] [nvarchar](max) NULL,[Address2] [nvarchar](max) NULL,[City] [nvarchar](max) NULL,[State] [nvarchar](2) NULL,[Zip] [nvarchar](12) NULL,[Phone] [nvarchar](20) NULL,CONSTRAINT [PK_Shipping_Address] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
            };

            using (SqlConnection connection = new SqlConnection(localCS))
            {
                try
                {
                    connection.Open();
                    foreach (var tblName in tblNames)
                    {
                        var dropTable = $"DROP TABLE IF EXISTS dbo.{tblName}";
                        using (SqlCommand command = new SqlCommand(dropTable, connection))
                        {
                            command.ExecuteNonQuery();
                        }
                        log.Info($"Table Dropped : {tblName}");
                    }

                    foreach (var tblCreate in tblCreates)
                    {
                        using (SqlCommand command = new SqlCommand(tblCreate, connection))
                        {
                            command.ExecuteNonQuery();
                        }
                        log.Info($"Table Recreated : {tblCreate}");
                    }

                }
                catch (Exception ex)
                {
                    log.Error($"Error Updating table Content : {ex.Message}");
                }
            }
        }

        public async Task ServiceConsumeAsync(string tblInsert)
        { 
            
            var webUrlApi = $"https://vmart.johnstonpaper.com/servicedept/api/service/{tblInsert}";
            //var webUrlApi = $"https://johnstonapi20190719083104.azurewebsites.net/api/service/{tblInsert}";
            //var webUrlApi = $"https://localhost:44366/api/service/{tblInsert}";
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var httpResponse = client.GetAsync(webUrlApi).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    using (var content = httpResponse.Content)
                    {
                        var tableResponse = await httpResponse.Content.ReadAsStringAsync();
                        tableResponse = Regex.Unescape(tableResponse.Trim('"'));
                        JsonSyncParsed(tblInsert, tableResponse);
                    }
                }
                log.Info($"Successful table updateed : {tblInsert}");
            }
            catch (Exception ex)
            {
                log.Error($"Error Deserializing: {ex.Message}");
            }
        }

        private void JsonSyncParsed(string tblInsert, string tableResponse)
        {
            var jsonArray = JArray.Parse(tableResponse);
            StringBuilder sb = new StringBuilder();
            List<string> sqlSyntax = new List<string>();
            List<string> sqlParams = new List<string>();
            var commaDelimited = string.Empty;
            var commaDelimitedParams = string.Empty;

            try
            {
                switch (tblInsert)
                {
                    case "Customers":
                        var customers = jsonArray.ToObject<List<Customers>>();
                        Customers custs = new Customers();
                        foreach (var cust in custs.GetType().GetProperties())
                        {
                            sqlSyntax.Add(cust.Name);
                            sqlParams.Add($"@{cust.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var customer in customers)
                        {
                            using (var connection = new SqlConnection(localCS))
                            {
                                connection.Open();
                                using (SqlCommand command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", customer.CustNo);
                                    command.Parameters.AddWithValue("@AccountName", customer.AccountName);
                                    command.Parameters.AddWithValue("@Contact", customer.Contact);
                                    command.Parameters.AddWithValue("@Phone", customer.Phone);
                                    if (customer.Email == null)
                                        command.Parameters.AddWithValue("@Email", DBNull.Value);
                                    else
                                        command.Parameters.AddWithValue("@Email", customer.Email);

                                    if (customer.InsideSales == null)
                                    {
                                        command.Parameters.AddWithValue("@InsideSales", DBNull.Value);
                                        command.Parameters.AddWithValue("@InsideSaleEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@InsideSales", customer.InsideSales);
                                        command.Parameters.AddWithValue("@InsideSaleEmail", customer.InsideSaleEmail);
                                    }


                                    command.Parameters.AddWithValue("@SalesRep", customer.SalesRep);
                                    command.Parameters.AddWithValue("@SaleRepEmail", customer.SaleRepEmail);
                                    command.Parameters.AddWithValue("@ServiceTech", customer.ServiceTech);
                                    if (customer.ServiceTechEmail == null)
                                        command.Parameters.AddWithValue("@ServiceTechEmail", DBNull.Value);
                                    else
                                        command.Parameters.AddWithValue("@ServiceTechEmail", customer.ServiceTechEmail);

                                    
                                    command.Parameters.AddWithValue("@CustomerServiceRep", customer.CustomerServiceRep);
                                    if (customer.CustomerServiceRepEmail == null)
                                        command.Parameters.AddWithValue("@CustomerServiceRepEmail", DBNull.Value);
                                    else
                                        command.Parameters.AddWithValue("@CustomerServiceRepEmail", customer.CustomerServiceRepEmail);
                                    
                                    
                                    if (customer.Nickname == null)
                                        command.Parameters.AddWithValue("@Nickname", DBNull.Value);
                                    else
                                        command.Parameters.AddWithValue("@Nickname", customer.Nickname);
                                    
                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "Contacts":
                        var contactTableParse = jsonArray.ToObject<List<Contacts>>();
                        Contacts contacts = new Contacts();
                        foreach (var contact in contacts.GetType().GetProperties())
                        {
                            sqlSyntax.Add(contact.Name);
                            sqlParams.Add($"@{contact.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var contactParsed in contactTableParse)
                        {
                            using (var connection = new SqlConnection(localCS))
                            {
                                connection.Open();
                                using (var command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", contactParsed.CustNo);
                                    command.Parameters.AddWithValue("@Contact", contactParsed.Contact);
                                    command.Parameters.AddWithValue("@Email", contactParsed.Email);
                                    command.Parameters.AddWithValue("@Department", contactParsed.Department);
                                    command.Parameters.AddWithValue("@Title", contactParsed.Title);
                                    command.Parameters.AddWithValue("@Phone", contactParsed.Phone);
                                    command.Parameters.AddWithValue("@ChemicalType", contactParsed.ChemicalType);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "MachineMedia":
                        var images = jsonArray.ToObject<List<MachineMedia>>();
                        MachineMedia machineMedia = new MachineMedia();
                        foreach (var mMedia in machineMedia.GetType().GetProperties())
                        {
                            sqlSyntax.Add(mMedia.Name);
                            sqlParams.Add($"@{mMedia.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var media in images)
                        {
                            using (var connection = new SqlConnection(localCS))
                            {
                                connection.Open();
                                using (var command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", media.CustNo);
                                    command.Parameters.AddWithValue("@MachineNo", media.MachineNo);
                                    command.Parameters.AddWithValue("@SerialNo", media.SerialNo);
                                    command.Parameters.AddWithValue("@Name", media.Name);
                                    command.Parameters.AddWithValue("@MediaType", media.MediaType);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "CustomerAddresses":
                        var addressTableParse = jsonArray.ToObject<List<CustomerAddresses>>();
                        CustomerAddresses addresses = new CustomerAddresses();
                        foreach (var cAddress in addresses.GetType().GetProperties())
                        {
                            sqlSyntax.Add(cAddress.Name);
                            sqlParams.Add($"@{cAddress.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = string.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");
                        foreach (var addressParsed in addressTableParse)
                        {
                            using (var connection = new SqlConnection(localCS))
                            {
                                connection.Open();

                                using (var command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@CustNo", addressParsed.CustNo);
                                    command.Parameters.AddWithValue("@ShipNo", addressParsed.ShipNo);
                                    command.Parameters.AddWithValue("@Name", addressParsed.Name);
                                    command.Parameters.AddWithValue("@Atn", addressParsed.Atn);
                                    command.Parameters.AddWithValue("@Address1", addressParsed.Address1);
                                    command.Parameters.AddWithValue("@Address2", addressParsed.Address2);
                                    command.Parameters.AddWithValue("@City", addressParsed.City);
                                    command.Parameters.AddWithValue("@State", addressParsed.State);
                                    command.Parameters.AddWithValue("@Zip", addressParsed.Zip);
                                    command.Parameters.AddWithValue("@Phone", addressParsed.Phone);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                    case "ServiceTechs":
                        var techTableParse = jsonArray.ToObject<List<ServiceTechs>>();
                        ServiceTechs techs = new ServiceTechs();
                        foreach (var tech in techs.GetType().GetProperties())
                        {
                            sqlSyntax.Add(tech.Name);
                            sqlParams.Add($"@{tech.Name}");
                        }
                        sqlSyntax.RemoveAt(0);
                        sqlParams.RemoveAt(0);
                        commaDelimited = String.Join(", ", sqlSyntax);
                        commaDelimitedParams = string.Join(", ", sqlParams);
                        sb.Append($"INSERT INTO {tblInsert} ({commaDelimited}) VALUES({commaDelimitedParams})");

                        foreach (var techParsed in techTableParse)
                        {
                            using (var connection = new SqlConnection(localCS))
                            {
                                connection.Open();

                                using (var command = new SqlCommand(sb.ToString(), connection))
                                {
                                    command.Parameters.AddWithValue("@TechId", techParsed.TechId);
                                    command.Parameters.AddWithValue("@Name", techParsed.Name);
                                    command.Parameters.AddWithValue("@Email", techParsed.Email);
                                    if (techParsed.Phone == null)
                                    {
                                        command.Parameters.AddWithValue("@Phone", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@Phone", techParsed.Phone);
                                    }

                                    command.Parameters.AddWithValue("@Retired", techParsed.Retired);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                        }
                        break;
                }

                log.Info($"Successful update : {tblInsert}");
            }
            catch (Exception ex)
            {
                log.Error($"Updating DB failed : {tblInsert} / {ex.Message}");
            }
        }

        public async Task ServicePostAsync(string localTable)
        {
            var getLocalData = string.Empty;
            if (localTable == "Customers")
                getLocalData = $"SELECT [CustNo], [Nickname] FROM {localTable} WHERE [Nickname] IS NOT NULL";
            else
                getLocalData = $"SELECT * FROM {localTable}";

            var sqlJsonResponse = string.Empty;
            
            var webUrlApi = $"https://vmart.johnstonpaper.com/servicedept/api/service/{localTable}";
            //var webUrlApi = $"https://johnstonapi20190719083104.azurewebsites.net/api/service/{localTable}";
            //var webUrlApi = $"https://localhost:44366/api/service/{localTable}";
            var dt = new DataTable();

            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                try
                {
                    using (var da = new SqlDataAdapter(getLocalData, connection))
                    {
                        da.Fill(dt);
                    }

                    if (dt.Rows.Count > 0)
                    {
                        var generateResponse = JsonConvert.SerializeObject(dt);

                        using (var client = new HttpClient())
                        using (var request = new HttpRequestMessage(HttpMethod.Post, webUrlApi))
                        {
                            using (var stringContent = new StringContent(generateResponse, Encoding.UTF8, "application/json"))
                            {
                                request.Content = stringContent;

                                using (var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false))
                                {
                                    response.EnsureSuccessStatusCode();
                                    log.Info($"Table collect complete and sent : {localTable}");
                                }
                            }
                        }
                    } 
                }
                catch (HttpRequestException ex)
                {
                    log.Error($"Error compiling post request : {ex.InnerException.Message}");
                }
            }
        }

    }

}
