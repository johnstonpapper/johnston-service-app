﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using System.IO;
using System;
using System.Windows;
using System.Threading.Tasks;
using log4net;
using Amazon.S3.Model;
using System.Diagnostics;

namespace JohnstoniGo_Windows.Helpers
{
    public class AmazonUploader
    {
        //private const string bucketName = "media.johnston.biz";
        private const string bucketName = "media.johnston.biz";
        private const string subBucketName = "/Signatures";
        private static readonly RegionEndpoint bucketEndpoint = RegionEndpoint.USEast2;



        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string existingBucketName = "media.johnston.biz/JohnstonService";
        private const string directoryPath = @"c:\temp\";

        private static IAmazonS3 s3Client = new AmazonS3Client(bucketEndpoint);

        public async Task UploadDirAsync()
        {
            try
            {
                
                var directoryTransfer = new TransferUtility(s3Client);

                await directoryTransfer.UploadDirectoryAsync(directoryPath + "/PDF", existingBucketName + "/AuditPdf");

                await directoryTransfer.UploadDirectoryAsync(directoryPath + "/machines/images", existingBucketName + "/AuditMachinesImages");

                await directoryTransfer.UploadDirectoryAsync(directoryPath + "/signatures", existingBucketName + "/AuditSignatures");

            } catch(Exception ex)
            {
                log.Error($"There is an error with uploading directory : {ex.Message}");
            }
        }

        private const string auditBucket = "media.johnston.biz/JohnstonService/AuditPdf";
        
        internal void DownloadAudit(string v, string dp)
        {

            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = auditBucket,
                    Key = v
                };

                using (GetObjectResponse response = s3Client.GetObject(request))
                {
                    response.WriteResponseStreamToFile(dp + v);
                }

                string path = dp + v;
                Process.Start(path);
            } catch (Exception ex)
            {
                MessageBox.Show($"Error retrieving audit form : {ex.Message}");
                log.Error($"Error downloading audit : {v} / error: {ex.Message}");
            }
        }

        internal string EamilDownloadedAudit(string v, string ad)
        {
            var auditFile = string.Empty;
            try
            {
                
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = auditBucket,
                    Key = v
                };

                using (GetObjectResponse response = s3Client.GetObject(request))
                {
                    response.WriteResponseStreamToFile(ad + v);
                }

                auditFile = ad + v;
            } catch (AmazonS3Exception s3Ex)
            {
                log.Error($"Amazon error : {s3Ex}");
                return s3Ex.Message;
            }
            catch (Exception Ex)
            {
                log.Error($"Error : {Ex.Message}");
                return Ex.Message;
            }

            return auditFile;
        }
    }
}
