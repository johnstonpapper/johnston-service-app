﻿using System;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
namespace Serialization
{
    /// <summary>
    /// Converts a DataTable to JSON. Note no support for deserialization
    /// </summary>
    public class DataTableConverter : JsonConverter
    {

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param><returns>
        ///   <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
        /// </returns>
        public override bool CanConvert(Type valueType)
        {
            return (valueType == typeof(DataTable));
        }

        /// <summary>
        /// Reads the json.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value.</param>
        /// <param name="serializer">The serializer.</param><returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            DataTable dt;

            if (reader.TokenType == JsonToken.PropertyName)
            {
                dt = new DataTable((string)reader.Value);
                reader.Read();
            }
            else
            {
                dt = new DataTable();
            }

            reader.Read();

            while (reader.TokenType == JsonToken.StartObject)
            {
                DataRow dr = dt.NewRow();
                reader.Read();

                while (reader.TokenType == JsonToken.PropertyName)
                {
                    string columnName = (string)reader.Value;

                    reader.Read();

                    if (!dt.Columns.Contains(columnName))
                    {
                        Type columnType = GetColumnDataType(reader.TokenType);
                        dt.Columns.Add(new DataColumn(columnName, columnType));
                    }

                    dr[columnName] = reader.Value ?? DBNull.Value;
                    reader.Read();
                }

                dr.EndEdit();
                dt.Rows.Add(dr);

                reader.Read();
            }

            return dt;
        }

        private static Type GetColumnDataType(JsonToken tokenType)
        {
            switch (tokenType)
            {
                case JsonToken.Integer:
                    return typeof(long);
                case JsonToken.Float:
                    return typeof(double);
                case JsonToken.String:
                case JsonToken.Null:
                case JsonToken.Undefined:
                    return typeof(string);
                case JsonToken.Boolean:
                    return typeof(bool);
                case JsonToken.Date:
                    return typeof(DateTime);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Writes the json.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            DataTable table = (DataTable)value;
            DefaultContractResolver resolver = serializer.ContractResolver as DefaultContractResolver;

            writer.WriteStartArray();

            foreach (DataRow row in table.Rows)
            {
                writer.WriteStartObject();
                foreach (DataColumn column in row.Table.Columns)
                {
                    if (serializer.NullValueHandling == NullValueHandling.Ignore && (row[column] == null || row[column] == DBNull.Value))
                        continue;

                    writer.WritePropertyName((resolver != null) ? resolver.GetResolvedPropertyName(column.ColumnName) : column.ColumnName);
                    serializer.Serialize(writer, row[column]);
                }
                writer.WriteEndObject();
            }

            writer.WriteEndArray();
        }

    }

}
