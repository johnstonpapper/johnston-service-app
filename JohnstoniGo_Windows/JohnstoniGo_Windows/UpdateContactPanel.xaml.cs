﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using JohnstoniGo_Windows.Helpers;
using System.Text.RegularExpressions;
using PdfSharp.Drawing;

using JohnstoniGo_Windows.Helpers;
using JohnstoniGo_Windows;

namespace JohnstoniGo_Windows
{
    /// <summary>
    /// Interaction logic for UpdateContactPanel.xaml
    /// </summary>
    /// 

    public partial class UpdateContactPanel : Window
    {
        string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;

        string assignNickname = "";
        string custCity, custState, custZip, custAd1, custAd2, portCustomerNo;


        public UpdateContactPanel()
        {
            InitializeComponent();
            portCustomerNo = Port.customerNo;
            this.Title = portCustomerNo;
            loadContacts();
            LoadCustomerContact();
        }

        private void LoadCustomerContact()
        {
            try
            {
                var populateContact =
                    $"SELECT Customers.Contact, Customers.Phone, Customers.Email, Customers.InsideSales, Customers.ServiceTech, Customers.CustomerServiceRep, Customers.SalesRep, Customers.Nickname, [CustomerAddresses].Atn, [CustomerAddresses].[Address1], CustomerAddresses.Address2, CustomerAddresses.City, CustomerAddresses.State, CustomerAddresses.Zip, CustomerAddresses.Phone FROM Customers INNER JOIN CustomerAddresses ON Customers.CustNo = CustomerAddresses.CustNo WHERE (Customers.CustNo = '{portCustomerNo}')";

                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    using (var command = new SqlCommand(populateContact, connection))
                    using (var dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                poi.Text = (string) dr["Contact"];
                                contactPhone.Text = (string) dr["Phone"];
                                contactAddress1.Text = (string) dr["Address1"];
                                contactCity.Text = (string) dr["City"];
                                contactState.Text = (string)dr["State"];
                                contactZip.Text = (string)dr["Zip"];

                                if (!dr.IsDBNull(2))
                                {
                                    contactEmail.Text = (string) dr["Email"];
                                }

                                if (!dr.IsDBNull(7))
                                {
                                    contactNickname.Text = (string)dr["Nickname"];
                                }
                                else
                                {
                                    assignNickname = string.Empty;
                                }

                                if (!dr.IsDBNull(10))
                                {
                                    contactAddress2.Text = (string)dr["Address2"];
                                    custAd2 = (string)dr["Address2"];
                                }

                                custAd1 = (string)dr["Address1"];

                                custCity = (string)dr["City"];
                                custState = (string)dr["State"];
                                custZip = (string)dr["Zip"];
                            }
                        }
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectQuery = $"SELECT count(*) FROM [dbo].[Contacts] WHERE [Contact] = '{poi.Text.Trim()}' AND [CustNo] = '{portCustomerNo}' ";
            SubmitContact(selectQuery);
            loadContacts();
        }

        private void DoneBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectQuery = $"SELECT count(*) FROM [dbo].[Contacts] WHERE [Contact] = '{poi.Text.Trim()}' AND [CustNo] = '{portCustomerNo}' ";
            SubmitContact(selectQuery);
            this.Close();
        }

        private void SubmitContact(string selectQuery)
        {
            var executeCommand = string.Empty;
            if (pgChemical.IsChecked == true || spartanChemical.IsChecked == true || bothChemical.IsChecked == true || contactEmail.Text == "")
            {
                if (custAd1 != contactAddress1.Text)
                    updateAddress();

                if (assignNickname != contactNickname.Text)
                    updateNickname();

                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    try
                    {
                        SqlCommand cmd = new SqlCommand(selectQuery, connection);
                        int counter = (int)cmd.ExecuteScalar();

                        if (counter > 0)
                            executeCommand = $"UPDATE [dbo].[Contacts] SET [Contact] = @Contact, [Email] = @Email, [Department] = @Department, [Title] =  @Title, [Phone] = @Phone, [ChemicalType] = @ChemicalType WHERE [ObjId] = '{contactsCombo.SelectedValue}' ";
                        else
                            executeCommand = "INSERT INTO [dbo].[Contacts]([CustNo],[Contact],[Email],[Department],[Title],[Phone],[ChemicalType]) VALUES(@CustNo,@Contact,@Email,@Department,@Title,@Phone,@ChemicalType)";

                        using (SqlCommand command = new SqlCommand(executeCommand, connection))
                        {
                            command.Parameters.AddWithValue("@CustNo", portCustomerNo);
                            command.Parameters.AddWithValue("@Contact", poi.Text);
                            command.Parameters.AddWithValue("@Email", contactEmail.Text);
                            command.Parameters.AddWithValue("@Department", contactDept.Text);
                            command.Parameters.AddWithValue("@Title", contactTitle.Text);
                            command.Parameters.AddWithValue("@Phone", contactPhone.Text);

                            if (pgChemical.IsChecked == true)
                                command.Parameters.AddWithValue("@ChemicalType", pgChemical.Content);
                            else if (spartanChemical.IsChecked == true)
                                command.Parameters.AddWithValue("@ChemicalType", spartanChemical.Content);
                            else
                                command.Parameters.AddWithValue("@ChemicalType", bothChemical.Content);

                            command.ExecuteNonQuery();
                            command.Parameters.Clear();

                            MessageBox.Show("Contact has been updated");
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        //createEmail.ThereAnError(ex.Message, "");
                    }
                }
            }
            else
            {
                MessageBox.Show("Chemical Type and Email Required when adding contact");
                pgChemical.Foreground = Brushes.Crimson;
                spartanChemical.Foreground = Brushes.Crimson;
                bothChemical.Foreground = Brushes.Crimson;
                contactEmail.Foreground = Brushes.Crimson;
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void NewBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateBtn.IsEnabled = false;
            clearGroup();
        }

        private void ContactEdit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string cmdContact = $"SELECT Contacts.Contact, Contacts.Email, Contacts.Department, Contacts.Title, Contacts.Phone, CustomerAddresses.Address1, CustomerAddresses.Address2, CustomerAddresses.City, CustomerAddresses.State, CustomerAddresses.Zip, CustomerAddresses.Phone AS PhoneShip, Contacts.ChemicalType FROM Contacts INNER JOIN CustomerAddresses ON Contacts.CustNo = CustomerAddresses.CustNo WHERE (Contacts.CustNo = '{portCustomerNo}' And Contacts.ObjId = '{contactsCombo.SelectedValue}')";

            try
            {
                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(cmdContact, connection))
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                poi.Text = (string)dr["Contact"];
                                contactEmail.Text = (string)dr["Email"];
                                contactDept.Text = (string)dr["Department"];
                                contactTitle.Text = (string)dr["Title"];
                                contactPhone.Text = (string)dr["Phone"];
                                contactAddress1.Text = (string)dr["Address1"];
                                contactAddress2.Text = (string)dr["Address2"];
                                contactCity.Text = (string)dr["City"];
                                contactState.Text = (string)dr["State"];
                                contactZip.Text = (string)dr["Zip"];

                                if ((string) dr["ChemicalType"] == "Both")
                                    bothChemical.IsChecked = true;
                                else if ((string)dr["ChemicalType"] == "Spartan")
                                    spartanChemical.IsChecked = true;
                                else
                                    pgChemical.IsChecked = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void loadContacts()
        {
            DataTable dt = new DataTable();
            var loadingContactList = $"SELECT [ObjId], [Contact] FROM Contacts WHERE CustNo = '{portCustomerNo}' ";
            try
            {
                using (var connection = new SqlConnection(localCS))
                {
                    try
                    {
                        connection.Open();
                        using (SqlCommand cmd = new SqlCommand(loadingContactList, connection))
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(dt);
                            contactsCombo.ItemsSource = dt.DefaultView;
                            contactsCombo.DisplayMemberPath = "Contact";
                            contactsCombo.SelectedValuePath = "ObjId";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("contact table error: " + ex.Message);
                        //log.Error("error with loading the service table: " + ex.Message);
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        void clearGroup()
        {
            pgChemical.Foreground = Brushes.Black;
            spartanChemical.Foreground = Brushes.Black;
            bothChemical.Foreground = Brushes.Black;
            contactEmail.Foreground = Brushes.Black;
            poi.Text = "";
            contactTitle.Text = "";
            contactDept.Text = "";
            contactEmail.Text = "";
            contactPhone.Text = "";
        }

        void updateNickname()
        {
            var updateNickname = $"UPDATE [dbo].[Customers] SET [Nickname] = @Nickname WHERE CustNo = '{portCustomerNo}' ";

            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                try
                {
                    using (SqlCommand cmd = new SqlCommand(updateNickname, connection))
                    {
                        cmd.Parameters.AddWithValue("@Nickname", contactNickname.Text);

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    //createEmail.ThereAnError(ex.Message, "");
                }

            }
        }

        void updateAddress()
        {
            var updateAddress =
                $"UPDATE [dbo].[CustomerAddresses] SET [Address1] = @Add1, [Address2] = @Add2, [City] = @City, [State] = @State, [Zip] = @Zip WHERE [CustNo] = '{portCustomerNo}' ";

            using (var connection = new SqlConnection(localCS))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(updateAddress, connection))
                    {
                        cmd.Parameters.AddWithValue("@Add1", contactAddress1.Text);
                        cmd.Parameters.AddWithValue("@Add2", contactAddress2.Text);
                        cmd.Parameters.AddWithValue("@City", contactCity.Text);
                        cmd.Parameters.AddWithValue("@State", contactState.Text);
                        cmd.Parameters.AddWithValue("@Zip", contactState.Text);

                        cmd.ExecuteNonQuery();
                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
