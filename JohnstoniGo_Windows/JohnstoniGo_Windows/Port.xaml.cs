﻿//this is to capture the images
using AForge.Video;
using AForge.Video.DirectShow;
using JohnstoniGo_Windows.Helpers;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ToastNotifications.Core;
using Image = System.Windows.Controls.Image;

namespace JohnstoniGo_Windows
{
    /// <summary>
    /// Interaction logic for Port.xaml
    /// </summary>
    public partial class Port : Window
    {

        private const string Letters = "abcdefghijklmnopqrstuvwxyz";
        private readonly char[] AlphaNumeric = (Letters + Letters.ToUpper() + "1234567890").ToCharArray();
        private static readonly Regex _regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text

        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string emailAuditBody = "";

        string remoteCS = ConfigurationManager.ConnectionStrings["johnstonAzure"].ConnectionString;
        string localCS = ConfigurationManager.ConnectionStrings["johnstonExpress"].ConnectionString;

        public static string customerNo;

        AmazonUploader s3Upload = new AmazonUploader();
        pdfGenerator pdfMaker = new pdfGenerator();
        EmailData auditEmail = new EmailData();
        DataConnector dbMaker = new DataConnector();


        #region public properties
        public ObservableCollection<FilterInfo> VideoDevices { get; set; }
        public FilterInfo CurrentDevice
        {
            get { return _currentDevice; }
            set { _currentDevice = value; this.OnPropertyChanged("CurrentDevice"); }
        }
        private FilterInfo _currentDevice;
        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        #endregion

        private readonly ToastNote vm;
        private IVideoSource _videoSource;

        public Port()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
            ServiceApplication.Title = $"{GetCurrentVersion()}";
            vm = new ToastNote();
            AuditTabs.Visibility = Visibility.Hidden;
            workOrderCust.Visibility = Visibility.Hidden;
            loadCustomerAccounts();
            getCameraDevices();
            
            
        }

        //consider moving somewhere else just clucter
        private string GetCurrentVersion()
        {
            var appVersion = string.Empty;
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                ApplicationDeployment applicationDeployment = ApplicationDeployment.CurrentDeployment;
                appVersion = $"JohnstonIGo - {applicationDeployment.CurrentVersion}";
            }
            return appVersion;

        }
        void getCameraDevices()
        {
            VideoDevices = new ObservableCollection<FilterInfo>();
            foreach (FilterInfo info in new FilterInfoCollection(FilterCategory.VideoInputDevice))
            {
                VideoDevices.Add(info);
            }
            if (VideoDevices.Any())
            {
                CurrentDevice = VideoDevices[0];
            }
            else
            {
                log.Error("Accessing the camera has failed");
            }
        }

        #region All loading features
        void loadCustomerAccounts()
        {
            var getCustomers = "SELECT [CustNo],[AccountName],[Nickname] FROM Customers ORDER BY [AccountName]";

            try
            {
                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(getCustomers, connection))
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            DataTable sourceDataTable = new DataTable();
                            sourceDataTable.Columns.Add("CustNo", typeof(string));
                            sourceDataTable.Columns.Add("Nickname", typeof(string));
                            sourceDataTable.Columns.Add("AccountName", typeof(string));
                            DataTable displayDataTable = new DataTable();
                            displayDataTable.Columns.Add("Display", typeof(string));
                            displayDataTable.Columns.Add("SelectedValue", typeof(string));

                            sourceDataTable.Load(dr);

                            foreach (DataRowView row in sourceDataTable.DefaultView)
                            {
                                var display = (string)row["AccountName"];
                                var accountNickname = "";
                                if (!row.Row.IsNull("NickName"))
                                {
                                    accountNickname = " - " + (string)row["Nickname"];
                                }

                                var selected = (string)row["CustNo"];
                                var newRow = displayDataTable.NewRow();
                                newRow["Display"] = display + " - " + selected + accountNickname;
                                newRow["SelectedValue"] = selected;
                                displayDataTable.Rows.Add(newRow);
                            }
                            var emptyRow = displayDataTable.NewRow();
                            displayDataTable.Rows.InsertAt(emptyRow, 0);
                            var rowDisplay = "Display";
                            custSelect.ItemsSource = displayDataTable.DefaultView;
                            custSelect.DisplayMemberPath = rowDisplay;
                            custSelect.SelectedValuePath = "SelectedValue";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("There's an error : " + ex.Message);
            }
        }

        void loadContacts(string customer)
        {
            var populateContacts = $"SELECT Contact, Email, Phone FROM Contacts WHERE (CustNo = '{customer}') UNION SELECT Contact, Email, Phone FROM Customers WHERE (CustNo = '{customer}')";
            DataTable dt = new DataTable();

            using (var connection = new SqlConnection(localCS))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand(populateContacts, connection))
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {

                        da.Fill(dt);

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("There is an error with customer contact that should be reported to IT: " + ex.Message);
                    log.Error("loading contact table error: " + ex.Message);
                }
            }

            custContactCombo.ItemsSource = dt.DefaultView;
            custContactCombo.DisplayMemberPath = "Contact";
            custContactCombo.SelectedValuePath = "Email";
            custContactCombo.SelectedIndex = 0;
        }

        void loadServiceTech()
        {
            var getTechs = "SELECT TechId, Email, Name FROM ServiceTechs ";

            using (var connection = new SqlConnection(localCS))
            {
                try
                {
                    connection.Open();
                    using (SqlCommand cmd = new SqlCommand(getTechs, connection))
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        techSelection.ItemsSource = dt.DefaultView;
                        techSelection.DisplayMemberPath = "Name";
                        techSelection.SelectedValuePath = "TechId";

                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("tech table error: " + ex.Message);
                    log.Error("error with loading the service table: " + ex.Message);
                }

            }
        }

        void loadLaundryMachines(string customer)
        {
            var machineCollection = $"SELECT LaundryMachineList.MachineNo, LaundryMachineList.MachineMake, LaundryMachineList.MachineModel, LaundryMachineList.MachineSerial, LaundryMachineList.MachineCapacity, LaundryMachineList.DispenserMake, LaundryMachineList.DispenserModel, LaundryMachineList.DispenserSerial, LaundryMachineList.NoPump, LaundryMachineList.[Level], LaundryMachineList.Temp, LaundryMachineList.Valves, LaundryMachineList.Agitation, LaundryMachineList.Timer, LaundryMachineList.Ph, LaundryMachineList.Bleach, LaundryMachineList.Iron, LaundryMachineList.Chlorine, LaundryMachineList.WateHardness FROM [LaundryMachineList] WHERE [CustNo] = '{customer}'";

            using (var connection = new SqlConnection(localCS))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand(machineCollection, connection))
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                TextBox make = this.FindName("machine" + dr.GetInt32(0) + "_make") as TextBox;
                                TextBox model = this.FindName("machine" + dr.GetInt32(0) + "_model") as TextBox;
                                TextBox capacity = this.FindName("machine" + dr.GetInt32(0) + "_capacity") as TextBox;
                                TextBox mSerial = this.FindName("machine" + dr.GetInt32(0) + "_serial") as TextBox;
                                TextBox dMake = this.FindName("machine" + dr.GetInt32(0) + "_dispenser") as TextBox;
                                TextBox dModel = this.FindName("machine" + dr.GetInt32(0) + "_dispenser_model") as TextBox;
                                TextBox dSerial = this.FindName("machine" + dr.GetInt32(0) + "_dispenser_serial") as TextBox;
                                TextBox pumps = this.FindName("machine" + dr.GetInt32(0) + "_pump") as TextBox;
                                TextBox level = this.FindName("machine" + dr.GetInt32(0) + "_water") as TextBox;
                                TextBox valves = this.FindName("machine" + dr.GetInt32(0) + "_drain") as TextBox;
                                TextBox agitation = this.FindName("machine" + dr.GetInt32(0) + "_agitation") as TextBox;
                                TextBox timer = this.FindName("machine" + dr.GetInt32(0) + "_timer") as TextBox;
                                TextBox wash = this.FindName("machine" + dr.GetInt32(0) + "_Ph") as TextBox;
                                TextBox bleach = this.FindName("machine" + dr.GetInt32(0) + "_BleachPh") as TextBox;
                                TextBox iron = this.FindName("machine" + dr.GetInt32(0) + "_Iron") as TextBox;
                                TextBox chlorine = this.FindName("machine" + dr.GetInt32(0) + "_chlorine") as TextBox;
                                TextBox hardness = this.FindName("machine" + dr.GetInt32(0) + "_WaterHardness") as TextBox;
                                TextBox temp = this.FindName("machine" + dr.GetInt32(0) + "_temp") as TextBox;
                                CheckBox obselete = this.FindName("machine" + dr.GetInt32(0) + "_obsolete") as CheckBox;
                                //Image laundryImg = this.FindName("laundryImg" + dr.GetInt32(0)) as Image;

                                make.Text = dr.GetString(1);
                                make.IsEnabled = false;
                                model.Text = dr.GetString(2);
                                model.IsEnabled = false;
                                mSerial.Text = dr.GetString(3);
                                mSerial.IsEnabled = false;
                                capacity.Text = dr.GetString(4);
                                capacity.IsEnabled = false;
                                dMake.Text = dr.GetString(5);
                                dMake.IsEnabled = false;
                                dModel.Text = dr.GetString(6);
                                dModel.IsEnabled = false;
                                dSerial.Text = dr.GetString(7);
                                dSerial.IsEnabled = false;
                                pumps.Text = dr.GetInt32(8).ToString();
                                pumps.IsEnabled = false;

                                obselete.IsEnabled = true;
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    log.Error("Loading laundry machine error: " + ex.Message);
                }
            }
        }

        //add image change like laundry load 
        void loadWarewashMachines(string customer)
        {
            string collectMachines = $"SELECT [MachineNo],[MachineMake],[MachineModel],[MachineSerial],[location],[DispenserMake],[DispenserModel],[DispenserSerial],[Voltage],[Scraping],[Racking],[FlatwareSoak],[MachineWater],[PrewashTank],[WashTank],[RinseTank],[TempType],[Temp] FROM [WarewashMachineList] WHERE [CustNo] = '{customer}'";

            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(collectMachines, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    try
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                TextBox make = this.FindName("machine_type" + dr.GetInt32(0) + "_mfg") as TextBox;
                                TextBox model2 = this.FindName("machine_type" + dr.GetInt32(0) + "_model") as TextBox;
                                TextBox location = this.FindName("machine_type" + dr.GetInt32(0) + "_location") as TextBox;
                                TextBox mSerial = this.FindName("machine_type" + dr.GetInt32(0) + "_serial") as TextBox;
                                TextBox dMake = this.FindName("machine_type" + dr.GetInt32(0) + "_dispenser") as TextBox;
                                TextBox dModel = this.FindName("machine_type" + dr.GetInt32(0) + "_dispenser_model") as TextBox;
                                TextBox dSerial = this.FindName("machine_type" + dr.GetInt32(0) + "_dispenser_serial") as TextBox;
                                TextBox voltage = this.FindName("machine_type" + dr.GetInt32(0) + "_voltage") as TextBox;

                                CheckBox hiTemp = this.FindName("machine_type" + dr.GetInt32(0) + "_Hi") as CheckBox;
                                CheckBox lowTemp = this.FindName("machine_type" + dr.GetInt32(0) + "_Lo") as CheckBox;
                                TextBox temp = this.FindName("machine_type" + dr.GetInt32(0) + "_TempRinse") as TextBox;
                                CheckBox obselete = this.FindName("machine_type" + dr.GetInt32(0) + "_obsolete") as CheckBox;

                                make.Text = dr.GetString(1);
                                make.IsEnabled = false;
                                model2.Text = dr.GetString(2);
                                model2.IsEnabled = false;
                                mSerial.Text = dr.GetString(3);
                                mSerial.IsEnabled = false;
                                location.Text = dr.GetString(4);
                                location.IsEnabled = false;
                                dMake.Text = dr.GetString(5);
                                dMake.IsEnabled = false;
                                dModel.Text = dr.GetString(6);
                                dModel.IsEnabled = false;
                                dSerial.Text = dr.GetString(7);
                                dSerial.IsEnabled = false;
                                voltage.Text = dr.GetInt32(8).ToString();
                                voltage.IsEnabled = false;

                                obselete.IsEnabled = true;

                                //logic belong
                                if (dr.GetBoolean(16) == true)
                                {
                                    hiTemp.IsChecked = true;
                                    lowTemp.IsChecked = false;
                                }
                                else
                                {
                                    hiTemp.IsChecked = false;
                                    lowTemp.IsChecked = true;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        log.Error("Loading warewash machine error: " + ex.Message);
                    }
                }
            }
        }

        private void loadAudits()
        {
            DataTable dt = new DataTable();
            var selectAllAudits =
                $"SELECT Customers.CustNo, Customers.AccountName, DrainAudit.WorkOrder, DrainAudit.Visit, DrainAudit.AuditCreation, Customers.Nickname, 'Drain Audit' as 'AuditType', [AuditPdf] FROM Customers INNER JOIN DrainAudit ON Customers.CustNo = DrainAudit.CustNo UNION " +
                "SELECT Customers.CustNo, Customers.AccountName, LaundryAudit.WorkOrder, LaundryAudit.Visit, LaundryAudit.AuditCreation, Customers.Nickname, 'Laundry Audit' as 'AuditType', [AuditPdf] FROM Customers INNER JOIN LaundryAudit ON Customers.CustNo = LaundryAudit.CustNo UNION " +
                "SELECT Customers.CustNo, Customers.AccountName, WarewashAudit.WorkOrder, WarewashAudit.Visit, WarewashAudit.AuditCreation, Customers.Nickname, 'Warewash Audit' as 'AuditType', [AuditPdf] FROM Customers INNER JOIN WarewashAudit ON Customers.CustNo = WarewashAudit.CustNo UNION " +
                "SELECT Customers.CustNo, Customers.AccountName, EmergencyAudit.WorkOrder, EmergencyAudit.Visit, EmergencyAudit.AuditCreation, Customers.Nickname, 'Emergency Audit' as 'AuditType', [AuditPdf] FROM Customers INNER JOIN EmergencyAudit ON Customers.CustNo = EmergencyAudit.CustNo Order by [AuditCreation] Desc";

            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (var dataAdapter = new SqlDataAdapter(selectAllAudits, connection))
                {
                    dataAdapter.Fill(dt);

                    completedAudits.ItemsSource = dt.DefaultView;
                }
            }

        }
        #endregion

        void checkLastChanged()
        {
            DateTime lastBatteryInstalled = DateTime.Parse(previousDateSource.Content.ToString());
            DateTime lastTubeInstalled = DateTime.Parse(previousDateTube.Content.ToString());

            DateTime changeBatteryNow = lastBatteryInstalled.AddDays(90).Date;
            DateTime changeTubeNow = lastTubeInstalled.AddDays(90).Date;

            if (DateTime.Now.Date >= changeBatteryNow.Date || DateTime.Now.Date >= changeTubeNow.Date)
            {
                //title : 90 Day Notification
                //message : Evaulate the scrub tube and power supply 
                string msgTitle = "90 Days Notification";
                string msgNotification = "Evaluate the 'power supply' and the 'squeegee tube'";

                MessageBox.Show(msgNotification, msgTitle, MessageBoxButton.OK, MessageBoxImage.Warning);
                log.Info("Tech informed that they should be reviewing and changing the battery and tubes today");
            }
        }

        void TermContact(SqlConnection connection)
        {
            var termContactInfo = $"SELECT Customers.CustNo, Customers.Contact, Customers.Phone, Customers.Email, CustomerAddresses.ShipNo, CustomerAddresses.Name, CustomerAddresses.Address1, CustomerAddresses.Address2, CustomerAddresses.City, CustomerAddresses.State, CustomerAddresses.Zip FROM Customers INNER JOIN CustomerAddresses ON Customers.CustNo = CustomerAddresses.CustNo WHERE (Customers.CustNo = '{customerNo}')";

            try
            {
                using (SqlCommand cmd = new SqlCommand(termContactInfo, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //customer data 
                            workOrderCust.Header = "Cust NO: " + dr.GetString(0); //customer #

                            custContact.Content = dr.GetString(1); //customer contact
                            custAddress1.Content = dr.GetString(6); //address 1
                            custAddress2.Content = dr.GetString(7); //address 2
                            custCity.Content = dr.GetString(8) + '/' + dr.GetString(9) + '/' + dr.GetString(10); // city, state, zip
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("there is an error to report to it: " + ex.Message);
                log.Error("Error with term orginal contact" + ex.Message);
            }
        }

        #region Generators 
        private string jpWO()
        {
            StringBuilder customWO = new StringBuilder();
            Random r = new Random();
            customWO.Append("JP-");
            for (int i = 0; i < 12; i++)
            {
                customWO.Append(AlphaNumeric[r.Next(AlphaNumeric.Length)]);
                if (i == 10 && techSelection.SelectedIndex != -1)
                    customWO.Append(techSelection.SelectedValue.ToString());
            }
            return customWO.ToString();
        }
        private string ImageNameGenerate()
        {
            var nameCreator = new StringBuilder();
            Random r = new Random();
            Regex reg = new Regex("[\\/:*?<>|]");
            nameCreator.Append($"img_{DateTime.Now:HH:mm:ss}_");

            for (int i = 0; i < 76; i++)
            {
                nameCreator.Append(AlphaNumeric[r.Next(AlphaNumeric.Length)]);
                if (i == 75)
                    nameCreator.Append(".png");
            }

            var imgName = nameCreator.ToString();
            imgName = reg.Replace(imgName, "-");
            var operatingDirectory = $@"c:\temp\signatures\{imgName}";
            if (File.Exists(operatingDirectory))
                ImageNameGenerate();

            return imgName;
        }

        private string SerialMaker()
        {
            StringBuilder serialNo = new StringBuilder();
            Random r = new Random();
            for (int i = 0; i < 11; i++)
            {
                serialNo.Append(AlphaNumeric[r.Next(AlphaNumeric.Length)]);
            }
            return $"jp-{serialNo}";
        }

        #endregion

        #region All the dropdowns
        private void CustSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearGroup(this);

            if (e.AddedItems.Count == 0) return;

            try
            {
                DataRowView row = (DataRowView)e.AddedItems[0];
                customerNo = row["SelectedValue"].ToString();
                string getPreviousAudit = $"SELECT TubeChangeLast, BatteryChangeLast FROM DrainAudit WHERE CustNo  = '{customerNo}' order by AuditCreation;";

                using (var sqlConnection = new SqlConnection(localCS))
                {
                    //open sqlconnection
                    sqlConnection.Open();

                    //sql command to process my commands
                    using (SqlCommand sqlCommand = new SqlCommand(getPreviousAudit, sqlConnection))
                    using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        if (sqlDataReader.HasRows)
                        {
                            sqlDataReader.Read();
                            previousDateTube.Content = sqlDataReader.GetDateTime(0).Date;
                            previousDateSource.Content = sqlDataReader.GetDateTime(1).Date;
                            checkLastChanged();
                        }
                        else
                        {
                            previousDateTube.Content = "No Previous Date";
                            previousDateSource.Content = "No Previous Date";
                            overrideBattery.IsEnabled = false;
                            tubeOverride.IsEnabled = false;
                        }
                    }
                }

                AuditTabs.Visibility = Visibility.Visible;
                workOrderCust.Visibility = Visibility.Visible;
                VisitingReason.Visibility = Visibility.Visible;
                AuditVisit.Visibility = Visibility.Visible;
                loadAudits();
                getCustomerData();
                loadLaundryMachines(customerNo);
                loadWarewashMachines(customerNo);
                loadContacts(customerNo);
                loadServiceTech();
            }
            catch (Exception ex)
            {
                log.Error($"There is an error with change {ex.Message}");
            }

        }

        private void CustContactCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (e.AddedItems.Count == 0) return;

            DataRowView row = (DataRowView)e.AddedItems[0];

            var serviceContact = row["Contact"].ToString();
            var chemicalFound = $"SELECT ChemicalType FROM Contacts WHERE Contact = '{serviceContact}' AND CustNo = '{customerNo}'";

            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(chemicalFound, connection))
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dr.Read();
                        chemicalStatus.Content = dr["ChemicalType"];
                    }
                    else
                    {
                        chemicalStatus.Content = "Chemical Type Missing";
                    }


                }
            }

            drainCustName.Content = serviceContact;
            laundryCustName.Content = serviceContact;
            warewashCustName.Content = serviceContact;
            emergencyCustName.Content = serviceContact;

        }

        private void TechSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;

            DataRowView row = (DataRowView)e.AddedItems[0];
            var serviceTech = row["Name"].ToString();

            drainTechName.Content = serviceTech;
            laundryTechName.Content = serviceTech;
            warewashTechName.Content = serviceTech;
            emergencyTechName.Content = serviceTech;
            if (!techSelection.SelectedValue.Equals(null))
                jpWorkOrderNo.Content = jpWO();
        }

        private void CustContactCombo_DropDownOpened(object sender, EventArgs e)
        {
            //this is for the customer refresh
            loadContacts(customerNo);
        }

        private void CustSelect_DropDownOpened(object sender, EventArgs e)
        {
            //this is for the nickname refresh
            loadCustomerAccounts();
        }

        #endregion

        private void getCustomerData()
        {
            var ContactExist = "SELECT Contacts.CustNo, Contacts.Contact, Contacts.Phone, Contacts.Email, CustomerAddresses.ShipNo, CustomerAddresses.Name, CustomerAddresses.Address1, CustomerAddresses.Address2, CustomerAddresses.City, CustomerAddresses.State, CustomerAddresses.Zip, Contacts.ChemicalType FROM Contacts INNER JOIN CustomerAddresses ON Contacts.CustNo = CustomerAddresses.CustNo WHERE (Contacts.CustNo = '" + customerNo + "')";

            string johnstonWorker = "SELECT Customers.InsideSales, Customers.ServiceTech, Customers.CustomerServiceRep, Customers.SalesRep FROM Customers WHERE (Customers.CustNo = '" + customerNo + "')";

            drainCurrentDate.Content = DateTime.Now.Date;
            using (var connection = new SqlConnection(localCS))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand(ContactExist, connection))
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //customer data 
                                workOrderCust.Header = "Cust NO: " + reader.GetString(0); //customer #

                                custContact.Content = reader.GetString(1); //customer contact
                                custAddress1.Content = reader.GetString(6); //address 1
                                custAddress2.Content = reader.GetString(7); //address 2
                                custCity.Content = reader.GetString(8) + '/' + reader.GetString(9) + '/' + reader.GetString(10); // city, state, zip
                                if (!reader.IsDBNull(11))
                                {
                                    chemicalStatus.Content = reader.GetString(11);
                                }
                                else
                                {
                                    chemicalStatus.Content = "Chemical Type is Missing";
                                }
                            }
                            reader.Close();
                        }
                        else
                        {
                            reader.Close();
                            TermContact(connection);
                        }
                    }

                    using (SqlCommand cmd = new SqlCommand(johnstonWorker, connection))
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            //jpTech.Content = dr.GetString(1); remove this 
                            jpCustomerService.Content = dr.GetString(2);
                            jpServiceRep.Content = dr.GetString(3);

                            if (!dr.IsDBNull(0))
                            {
                                jpInsideSale.Content = dr.GetString(0);
                            }
                            else
                            {
                                jpInsideSale.Content = "Missing";
                            }

                            drainTechName.Content = dr.GetString(1);
                            laundryTechName.Content = dr.GetString(1);
                            warewashTechName.Content = dr.GetString(1);
                            emergencyTechName.Content = dr.GetString(1);
                        }
                        dr.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("There's an error that should be communicated to IT: " + ex.Message);
                }
            }
        }

        private string saveSignature(InkCanvas inked)
        {
            var s3Key = ImageNameGenerate();
            var directoryPath = @"c:\temp\signatures\";

            inked.Margin = new Thickness(0, 0, 0, 0);
            var size = new Size(inked.ActualWidth, inked.ActualHeight);

            inked.Measure(size);
            inked.Arrange(new Rect(size));
            var encodeThis = new PngBitmapEncoder();
            var bitmapTarget = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96, 96, PixelFormats.Default);
            bitmapTarget.Render(inked);
            encodeThis.Frames.Add(BitmapFrame.Create(bitmapTarget));

            int margin = (int)inked.Margin.Left;
            int width = (int)inked.ActualWidth - margin;
            int height = (int)inked.ActualHeight - margin;

            inked.Background = System.Windows.Media.Brushes.White;

            //render ink to bitmap
            RenderTargetBitmap rtb = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Default);

            rtb.Render(inked);

            //save the ink to a memory stream
            BmpBitmapEncoder jpgEncode = new BmpBitmapEncoder();
            jpgEncode.Frames.Add(BitmapFrame.Create(rtb));
            Directory.CreateDirectory(directoryPath);

            var s3Create = directoryPath + s3Key;

            FileStream file = File.Open(s3Create, FileMode.Create);

            jpgEncode.Save(file);
            file.Close();

            return s3Key;
        }

        private void clearGroup(DependencyObject dependency)
        {
            TextBox textBox = dependency as TextBox;
            CheckBox checkBox = dependency as CheckBox;
            InkCanvas inkCanvas = dependency as InkCanvas;
            ComboBox comboBox = dependency as ComboBox;
            Label label = dependency as Label;

            if (textBox != null)
            {
                textBox.Text = "";
                textBox.IsEnabled = true;
            }

            if (checkBox != null)
            {
                checkBox.IsChecked = false;
                checkBox.IsEnabled = true;
            }

            if (inkCanvas != null)
            {
                inkCanvas.Strokes.Clear();
            }

            if (comboBox != null)
            {
                comboBox.SelectedIndex = 0;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(dependency as DependencyObject); i++)
            {
                clearGroup(VisualTreeHelper.GetChild(dependency, i));
            }
        }

        #region Saving procedures
        private void drainSave_Click(object sender, RoutedEventArgs e)
        {
            var drainInsert = "INSERT INTO [dbo].[DrainAudit]([CustNo],[WorkOrder],[Visit],[Pump],[TubeChangeLast],[BatteryChangeLast],[Pail],[Odors],[Blockage],[Floor],[Sink],[Notes],[AuditTech],[TechSignature],[AuditContact],[CustSignature]) VALUES(@CustNo,@WorkOrder,@Visit,@Pump,@TubeChangeLast,@BatteryChangeLast,@Pail,@Odors,@Blockage,@Floor,@Sink,@Notes,@AuditTech,@TechSignature,@AuditContact,@CustSignature)";

            if (AuditSubmissionCheck(drainCustSignature, drainTechSignature))
                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    using (SqlCommand sqlCmd = new SqlCommand(drainInsert, connection))
                    {
                        try
                        {
                            sqlCmd.Parameters.AddWithValue("@CustNo", customerNo); //customer #
                            sqlCmd.Parameters.AddWithValue("@Visit", AuditVisit.Text.ToString());
                            sqlCmd.Parameters.AddWithValue("@WorkOrder", jpWorkOrderNo.Content.ToString());
                            ;
                            sqlCmd.Parameters.AddWithValue("@Pump", drainPump.IsChecked);
                            sqlCmd.Parameters.AddWithValue("@Pail", drainPails.IsChecked);

                            if (previousDateTube.Content.ToString() != "No Previous Date")
                            {
                                if (tubeOverride.IsChecked == true)
                                    sqlCmd.Parameters.AddWithValue("@TubeChangeLast", DateTime.Now.Date); //batteries
                                else
                                    sqlCmd.Parameters.AddWithValue("@TubeChangeLast",
                                        DateTime.Parse(previousDateTube.Content.ToString()).Date);
                            }
                            else
                            {
                                sqlCmd.Parameters.AddWithValue("@TubeChangeLast", DateTime.Now.Date); //batteries
                            }

                            if (previousDateSource.Content.ToString() != "No Previous Date")
                            {
                                if (overrideBattery.IsChecked == true)
                                    sqlCmd.Parameters.AddWithValue("@BatteryChangeLast", DateTime.Now.Date); //batteries
                                else
                                    sqlCmd.Parameters.AddWithValue("@BatteryChangeLast",
                                        DateTime.Parse(previousDateSource.Content.ToString()).Date);
                            }
                            else
                            {
                                sqlCmd.Parameters.AddWithValue("@BatteryChangeLast", DateTime.Now.Date); //batteries
                            }

                            sqlCmd.Parameters.AddWithValue("@Odors", drainOdors.IsChecked);
                            sqlCmd.Parameters.AddWithValue("@Blockage", drainBlockage.IsChecked);
                            sqlCmd.Parameters.AddWithValue("@Floor", drainFloor.IsChecked);
                            sqlCmd.Parameters.AddWithValue("@Sink", drainSink.IsChecked);
                            sqlCmd.Parameters.AddWithValue("Notes", drainNotes.Text.ToString()); //notes
                            sqlCmd.Parameters.AddWithValue("@AuditContact", drainCustName.Content);
                            sqlCmd.Parameters.AddWithValue("@CustSignature", saveSignature(drainCustSignature));
                            sqlCmd.Parameters.AddWithValue("@AuditTech", drainTechName.Content);
                            sqlCmd.Parameters.AddWithValue("@TechSignature", saveSignature(drainTechSignature));

                            sqlCmd.ExecuteNonQuery();
                            sqlCmd.Parameters.Clear();
                            MessageBox.Show("The audit form has been completed and saved");
                            log.Info($"Drain Audit has been saved : {customerNo}");
                            if (AuditVisit.SelectedIndex != 2 || AuditVisit.SelectedIndex != 3)
                            {
                                var emailAuditReport =
                                    pdfMaker.generateDrainPdf(customerNo, jpWorkOrderNo.Content.ToString());
                                var emailSubject = $"Drain Audit - {jpWorkOrderNo.Content}";

                                //audit, body, contact email, subject
                                auditEmail.isOutlook(emailAuditReport, "", custContactCombo.SelectedValue.ToString(),
                                    emailSubject);
                            }

                            clearGroup(this); 
                            jpWorkOrderNo.Content = jpWO();

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                            log.Error($"Drain audit : {customerNo} / error : {ex.Message}");
                        }
                    }
                }
            else
                return;
        }

        private void EmergencySave_Click(object sender, RoutedEventArgs e)
        {
            var insertEmergencyAudit = "INSERT INTO [dbo].[EmergencyAudit]([CustNo],[WorkOrder],[Notes],[AuditTech],[AuditTechSignature],[AuditContact],[AuditContactSignature]) VALUES(@CustNo,@WorkOrder,@Notes,@AuditTech,@AuditTechSignature,@AuditContact,@AuditContactSignature)";

            if (AuditSubmissionCheck(emergencyCustSignature, emergencyTechSignature))
                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    try
                    {
                        SqlCommand commandCount = new SqlCommand("SELECT COUNT(*) FROM [dbo].[EmergencyAudit]", connection);
                        int counter = (int)commandCount.ExecuteScalar();
                        if (counter == 0)
                        {
                            var executeDropCreate = "DROP TABLE if EXISTS [dbo].[EmergencyAudit] CREATE TABLE [dbo].[EmergencyAudit]([ObjId] [int] IDENTITY(1,1) NOT NULL,[CustNo] [nvarchar](50) NOT NULL,[WorkOrder] [nvarchar](50) NOT NULL,[Notes] [nvarchar](max) NOT NULL,[AuditPdf] [nvarchar](150) NULL,[AuditTech] [nvarchar](100) NOT NULL,[AuditTechSignature] [nvarchar](max) NOT NULL,[AuditContact] [nvarchar](100) NOT NULL,[AuditContactSignature] [nvarchar](max) NOT NULL,[AuditCreation] [datetime] NULL,[Visit] [nvarchar](50) NULL,CONSTRAINT [PK_EmergencyAudit] PRIMARY KEY CLUSTERED ([ObjId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] ALTER TABLE [dbo].[EmergencyAudit] ADD  CONSTRAINT [DF_EmergencyAudit_AuditCreation]  DEFAULT (getdate()) FOR [AuditCreation] ALTER TABLE [dbo].[EmergencyAudit] ADD  CONSTRAINT [DF_EmergencyAudit_Visit]  DEFAULT (N'Emergency Audit') FOR [Visit]";
                            SqlCommand tempCommand = new SqlCommand(executeDropCreate, connection);
                            tempCommand.ExecuteNonQuery();
                        }

                        using (SqlCommand command = new SqlCommand(insertEmergencyAudit, connection))
                        {
                            command.Parameters.AddWithValue("@CustNo", customerNo);
                            command.Parameters.AddWithValue("@WorkOrder", jpWorkOrderNo.Content);
                            command.Parameters.AddWithValue("Notes", emergencyNotes.Text.Trim()); //notes
                            command.Parameters.AddWithValue("@AuditContact", emergencyCustName.Content);
                            command.Parameters.AddWithValue("@AuditContactSignature", saveSignature(emergencyCustSignature));
                            command.Parameters.AddWithValue("@AuditTech", emergencyTechName.Content);
                            command.Parameters.AddWithValue("@AuditTechSignature", saveSignature(emergencyTechSignature));

                            command.ExecuteNonQuery();
                            command.Parameters.Clear();
                            
                            log.Info($"Emergency Audit completed : {customerNo}");
                            MessageBox.Show("The audit form has been completed and saved");

                            var emailAuditReport = pdfMaker.generateDrainPdf(customerNo, jpWorkOrderNo.Content.ToString());
                            var emailSubject = $"Emergency Audit - {jpWorkOrderNo.Content.ToString()}";
                            //audit, body, contact email, subject
                            auditEmail.isOutlook(emailAuditReport, "", custContactCombo.SelectedValue.ToString(), emailSubject);

                            clearGroup(this);
                            jpWorkOrderNo.Content = jpWO();

                        }
                    } catch(Exception ex)
                    {

                    }
                    
                }
            else
                return;
        }

        private void SaveLaundryAudit_Click(object sender, RoutedEventArgs e)
        {
            var laundryMachineInsert = "INSERT INTO [dbo].[LaundryMachineList]([CustNo],[MachineNo],[MachineMake],[MachineModel],[MachineSerial],[MachineCapacity],[DispenserMake],[DispenserModel],[DispenserSerial],[NoPump],[Level],[Temp],[Valves],[Agitation],[Timer],[Ph],[Bleach],[Iron],[Chlorine],[WateHardness])VALUES (@CustNo,@MachineNo,@MachineMake,@MachineModel,@MachineSerial,@MachineCapacity,@DispenserMake,@DispenserModel,@DispenserSerial,@NoPump,@Level,@Temp,@Valves,@Agitation,@Timer,@Ph,@Bleach,@Iron,@Chlorine,@WateHardness)";

            var laundryInsert = "INSERT INTO [dbo].[LaundryAudit]([CustNo],[WorkOrder],[Visit],[FabricAppearance],[FabricFinal],[FabricOdor],[FabricFeel],[FabricStain],[FabricColor],[ProcedureCollection],[ProcedureSorting],[ProcedurePre],[ProcedureLoading],[ProcedureReclaim],[ProcedureReject],[Notes],[AuditTech],[TechSignature],[AuditContact],[CustSignature]) VALUES(@CustNo,@WorkOrder,@Visit,@FabricAppearance,@FabricFinal,@FabricOdor,@FabricFeel,@FabricStain,@FabricColor,@ProcedureCollection,@ProcedureSorting,@ProcedurePre,@ProcedureLoading,@ProcedureReclaim,@ProcedureReject,@Notes,@AuditTech,@TechSignature,@AuditContact,@CustSignature)";

            if (AuditSubmissionCheck(laundryCustSignature, laundryTechSignature))
                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();

                    try
                    {
                        if (machine1_make.Text != "" && !DoesCurrentMachineExist(1, machine1_serial.Text.Trim(), "LaundryMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM LaundryMachineList WHERE CustNo = '{customerNo}' AND MachineNo = '{1}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(laundryMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo.ToString());
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("1"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine1_make.Text);
                                    cmd.Parameters.AddWithValue("@MachineModel", machine1_model.Text);
                                    cmd.Parameters.AddWithValue("@MachineCapacity", machine1_capacity.Text);
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine1_serial.Text);
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine1_dispenser.Text);
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine1_dispenser_model.Text);
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine1_dispenser_model.Text);
                                    cmd.Parameters.AddWithValue("@NoPump", machine1_pump.Text);
                                    cmd.Parameters.AddWithValue("@Level", machine1_water.Text);
                                    cmd.Parameters.AddWithValue("@Temp", machine1_temp.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Valves", machine1_drain.Text);
                                    cmd.Parameters.AddWithValue("@Agitation", machine1_agitation.Text);
                                    cmd.Parameters.AddWithValue("@Timer", machine1_timer.Text);
                                    cmd.Parameters.AddWithValue("@Ph", Convert.ToDecimal(machine1_Ph.Text));
                                    cmd.Parameters.AddWithValue("@Bleach", Convert.ToDecimal(machine1_BleachPh.Text));
                                    cmd.Parameters.AddWithValue("@Iron", machine1_Iron.Text);
                                    cmd.Parameters.AddWithValue("@Chlorine", machine1_chlorine.Text);
                                    cmd.Parameters.AddWithValue("@WateHardness", machine1_WaterHardness.Text);

                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        if (machine2_make.Text != "" && !DoesCurrentMachineExist(2, machine2_serial.Text.Trim(), "LaundryMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM LaundryMachineList WHERE CustNo = '{customerNo}' AND MachineNo = '{2}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(laundryMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo.ToString());
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("2"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine2_make.Text);
                                    cmd.Parameters.AddWithValue("@MachineModel", machine2_model.Text);
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine2_serial.Text);
                                    cmd.Parameters.AddWithValue("@MachineCapacity", machine2_capacity.Text);
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine2_dispenser.Text);
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine2_dispenser_model.Text);
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine2_dispenser_serial.Text);
                                    cmd.Parameters.AddWithValue("@NoPump", machine2_pump.Text);
                                    cmd.Parameters.AddWithValue("@Level", machine2_water.Text);
                                    cmd.Parameters.AddWithValue("@Temp", machine2_temp.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Valves", machine2_drain.Text);
                                    cmd.Parameters.AddWithValue("@Agitation", machine2_agitation.Text);
                                    cmd.Parameters.AddWithValue("@Timer", machine2_timer.Text);
                                    cmd.Parameters.AddWithValue("@Ph", Convert.ToDecimal(machine2_Ph.Text));
                                    cmd.Parameters.AddWithValue("@Bleach", Convert.ToDecimal(machine2_BleachPh.Text));
                                    cmd.Parameters.AddWithValue("@Iron", machine2_Iron.Text);
                                    cmd.Parameters.AddWithValue("@Chlorine", machine2_chlorine.Text);
                                    cmd.Parameters.AddWithValue("@WateHardness", machine2_WaterHardness.Text);

                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        if (machine3_make.Text != "" && !DoesCurrentMachineExist(3, machine3_serial.Text.Trim(), "LaundryMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM LaundryMachineList WHERE CustNo = '{customerNo}' AND MachineNo = '{3}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(laundryMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo.ToString());
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("3"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine3_make.Text);
                                    cmd.Parameters.AddWithValue("@MachineModel", machine3_model.Text);
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine3_serial.Text);
                                    cmd.Parameters.AddWithValue("@MachineCapacity", machine3_capacity.Text);
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine3_dispenser.Text);
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine3_dispenser_model.Text);
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine3_dispenser_serial.Text);
                                    cmd.Parameters.AddWithValue("@NoPump", machine3_pump.Text);
                                    cmd.Parameters.AddWithValue("@Level", machine3_water.Text);
                                    cmd.Parameters.AddWithValue("@Temp", machine3_temp.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Valves", machine3_drain.Text);
                                    cmd.Parameters.AddWithValue("@Agitation", machine3_agitation.Text);
                                    cmd.Parameters.AddWithValue("@Timer", machine3_timer.Text);
                                    cmd.Parameters.AddWithValue("@Ph", Convert.ToDecimal(machine3_Ph.Text));
                                    cmd.Parameters.AddWithValue("@Bleach", Convert.ToDecimal(machine3_BleachPh.Text));
                                    cmd.Parameters.AddWithValue("@Iron", machine3_Iron.Text);
                                    cmd.Parameters.AddWithValue("@Chlorine", machine3_chlorine.Text);
                                    cmd.Parameters.AddWithValue("@WateHardness", machine3_WaterHardness.Text);

                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        if (machine4_make.Text != "" && !DoesCurrentMachineExist(4, machine4_serial.Text.Trim(), "LaundryMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM LaundryMachineList WHERE CustNo = '{customerNo}' AND MachineNo = '{4}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(laundryMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo.ToString());
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("4"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine4_make.Text);
                                    cmd.Parameters.AddWithValue("@MachineModel", machine4_model.Text);
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine4_serial.Text);
                                    cmd.Parameters.AddWithValue("@MachineCapacity", machine4_capacity.Text);
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine4_dispenser.Text);
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine4_dispenser_model.Text);
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine4_dispenser_serial.Text);
                                    cmd.Parameters.AddWithValue("@NoPump", machine4_pump.Text);
                                    cmd.Parameters.AddWithValue("@Level", machine4_water.Text);
                                    cmd.Parameters.AddWithValue("@Temp", machine4_temp.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Valves", machine4_drain.Text);
                                    cmd.Parameters.AddWithValue("@Agitation", machine4_agitation.Text);
                                    cmd.Parameters.AddWithValue("@Timer", machine4_timer.Text);
                                    cmd.Parameters.AddWithValue("@Ph", Convert.ToDecimal(machine4_Ph.Text));
                                    cmd.Parameters.AddWithValue("@Bleach", Convert.ToDecimal(machine4_BleachPh.Text));
                                    cmd.Parameters.AddWithValue("@Iron", machine4_Iron.Text);
                                    cmd.Parameters.AddWithValue("@Chlorine", machine4_chlorine.Text);
                                    cmd.Parameters.AddWithValue("@WateHardness", machine4_WaterHardness.Text);

                                    cmd.ExecuteNonQuery();
                                }
                            }

                        }

                        if (fabric_appearance.Text != "")
                        {
                            using (SqlCommand cmd = new SqlCommand(laundryInsert, connection))
                            {
                                cmd.Parameters.AddWithValue("@CustNo", customerNo);
                                cmd.Parameters.AddWithValue("@WorkOrder", jpWorkOrderNo.Content);
                                cmd.Parameters.AddWithValue("@Visit", AuditVisit.Text.ToString());
                                cmd.Parameters.AddWithValue("@FabricAppearance", fabric_appearance.Text.Trim());
                                cmd.Parameters.AddWithValue("@FabricFinal", Convert.ToDecimal(fabric_final.Text.Trim()));
                                cmd.Parameters.AddWithValue("@FabricOdor", fabric_odor.Text.Trim());
                                cmd.Parameters.AddWithValue("@FabricFeel", fabric_feel.Text.Trim());
                                cmd.Parameters.AddWithValue("@FabricStain", fabric_stain.Text.Trim());
                                cmd.Parameters.AddWithValue("@FabricColor", fabric_color.Text.Trim());
                                cmd.Parameters.AddWithValue("@ProcedureCollection", procedures_collection.IsChecked);
                                cmd.Parameters.AddWithValue("@ProcedureSorting", procedures_sort.IsChecked);
                                cmd.Parameters.AddWithValue("@ProcedurePre", procedures_pre.IsChecked);
                                cmd.Parameters.AddWithValue("@ProcedureLoading", procedures_loading.IsChecked);
                                cmd.Parameters.AddWithValue("@ProcedureReclaim", procedures_reclaim.IsChecked);
                                cmd.Parameters.AddWithValue("@ProcedureReject", procedures_reject.IsChecked);
                                cmd.Parameters.AddWithValue("@Notes", laundryNotes.Text.ToString());
                                cmd.Parameters.AddWithValue("@TechSignature", saveSignature(laundryTechSignature));
                                cmd.Parameters.AddWithValue("@CustSignature", saveSignature(laundryCustSignature));
                                cmd.Parameters.AddWithValue("@AuditContact", laundryCustName.Content);
                                cmd.Parameters.AddWithValue("@AuditTech", laundryTechName.Content);

                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();
                            }
                        }

                        log.Info($"Laundry audit completed : {AuditVisit.Text.ToString()} / {customerNo}");
                        MessageBox.Show("The audit form has been completed and saved");

                        if (AuditVisit.SelectedIndex != 2 || AuditVisit.SelectedIndex != 3)
                        {
                            var emailAuditReport = pdfMaker.generateLaundryPdf(customerNo, jpWorkOrderNo.Content.ToString());
                            var emailSubject = $"Laundry Audit - {jpWorkOrderNo.Content.ToString()}";
                            auditEmail.isOutlook(emailAuditReport, "", custContactCombo.SelectedValue.ToString(), emailSubject);
                        }
                        clearGroup(this);
                        jpWorkOrderNo.Content = jpWO();
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Laundry audit failed : {customerNo} / error : {ex.Message}");
                        MessageBox.Show(ex.Message);
                    }
                }
            else
                return;
        }

        private void SaveWarewashAudit_Click(object sender, RoutedEventArgs e)
        {
            var warewashMachineInsert = "INSERT INTO [dbo].[WarewashMachineList]([CustNo],[MachineNo],[MachineMake],[MachineModel],[MachineSerial],[location],[DispenserMake],[DispenserModel],[DispenserSerial],[Voltage],[Scraping],[Racking],[FlatwareSoak],[MachineWater],[PrewashTank],[WashTank],[RinseTank],[TempType],[Temp])VALUES(@CustNo,@MachineNo,@MachineMake,@MachineModel,@MachineSerial,@location,@DispenserMake,@DispenserModel,@DispenserSerial,@Voltage,@Scraping,@Racking,@FlatwareSoak,@MachineWater,@PrewashTank,@WashTank,@RinseTank,@TempType,@Temp)";

            var warewashInsert = "INSERT INTO [dbo].[WarewashAudit]([CustNo],[WorkOrder],[SerialNO],[Visit],[WashArms],[RinseJets],[RinseValves],[OverFlow],[ByPass],[FinalRinsePsi],[PumpMotor],[FillValve],[Drains],[TempGauges],[Curtains],[Racks],[Doors],[Detergent],[DetergentDrops],[RinseAid],[RinseAidDrops],[Sanitizer],[SanitizerDrops],[WaterHardness],[Notes],[AuditContact],[CustomerSignature],[AuditTech],[TechSignature])VALUES(@CustNo,@WorkOrder,@SerialNo,@Visit,@WashArms,@RinseJets,@RinseValves,@OverFlow,@ByPass,@FinalRinsePsi,@PumpMotor,@FillValve, @Drains,@TempGauges,@Curtains,@Racks,@Doors,@Detergent,@DetergentDrops,@RinseAid,@RinseAidDrops,@Sanitizer,@SanitizerDrops,@WaterHardness,@Notes,@AuditContact,@CustomerSignature,@AuditTech,@TechSignature)";

            if (AuditSubmissionCheck(warewashCustSignature, warewashTechSignature))
                using (var connection = new SqlConnection(localCS))
                {
                    try
                    {
                        connection.Open();

                        if (machine_type1_mfg.Text != "" && !DoesCurrentMachineExist(1, machine_type1_serial.Text.Trim(), "WarewashMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM [WarewashMachineList] WHERE [CustNo] = '{customerNo}' AND [MachineNo] = '{1}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(warewashMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo);
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("1"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine_type1_mfg.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineModel", machine_type1_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine_type1_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@location", machine_type1_location.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine_type1_dispenser.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine_type1_dispenser_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine_type1_dispenser_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Voltage", machine_type1_voltage.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Scraping", machine_type1_Scraping.IsChecked);
                                    cmd.Parameters.AddWithValue("@Racking", machine_type1_Racking.IsChecked);
                                    cmd.Parameters.AddWithValue("@FlatwareSoak", machine_type1_Flatware.IsChecked);
                                    cmd.Parameters.AddWithValue("@MachineWater", machine_type1_Water.IsChecked);
                                    cmd.Parameters.AddWithValue("@PrewashTank", machine_type1_Prewash.Text.Trim());
                                    cmd.Parameters.AddWithValue("@WashTank", machine_type1_WashTank.Text.Trim());
                                    cmd.Parameters.AddWithValue("@RinseTank", machine_type1_RinseTank.Text.Trim());
                                    if (machine_type1_Hi.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", false);
                                    }
                                    else if (machine_type1_Lo.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", true);
                                    }
                                    cmd.Parameters.AddWithValue("@Temp", machine_type1_TempRinse.Text.Trim());

                                    cmd.ExecuteNonQuery();
                                }
                            }

                        }
                        if (machine_type2_mfg.Text != "" && !DoesCurrentMachineExist(2, machine_type2_serial.Text.Trim(), "WarewashMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM [WarewashMachineList] WHERE [CustNo] = '{customerNo}' AND [MachineNo] = '{2}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(warewashMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo.ToString());
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("2"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine_type2_mfg.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineModel", machine_type2_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine_type2_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@location", machine_type2_location.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine_type2_dispenser.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine_type2_dispenser_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine_type2_dispenser_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Voltage", machine_type2_voltage.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Scraping", machine_type2_Scraping.IsChecked);
                                    cmd.Parameters.AddWithValue("@Racking", machine_type2_Racking.IsChecked);
                                    cmd.Parameters.AddWithValue("@FlatwareSoak", machine_type2_Flatware.IsChecked);
                                    cmd.Parameters.AddWithValue("@MachineWater", machine_type2_Water.IsChecked);
                                    cmd.Parameters.AddWithValue("@PrewashTank", machine_type2_Prewash.Text.Trim());
                                    cmd.Parameters.AddWithValue("@WashTank", machine_type2_WashTank.Text.Trim());
                                    cmd.Parameters.AddWithValue("@RinseTank", machine_type2_RinseTank.Text.Trim());
                                    if (machine_type2_Hi.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", false);
                                    }
                                    else if (machine_type2_Lo.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", true);
                                    }
                                    cmd.Parameters.AddWithValue("@Temp", machine_type2_TempRinse.Text.Trim());

                                    cmd.ExecuteNonQuery();
                                }
                            }

                        }
                        if (machine_type3_mfg.Text != "" && !DoesCurrentMachineExist(3, machine_type3_serial.Text.Trim(), "WarewashMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM [WarewashMachineList] WHERE [CustNo] = '{customerNo}' AND [MachineNo] = '{3}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(warewashMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo);
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("3"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine_type3_mfg.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineModel", machine_type3_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine_type3_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@location", machine_type3_location.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine_type3_dispenser.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine_type3_dispenser_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine_type3_dispenser_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Voltage", machine_type3_voltage.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Scraping", machine_type3_Scraping.IsChecked);
                                    cmd.Parameters.AddWithValue("@Racking", machine_type3_Racking.IsChecked);
                                    cmd.Parameters.AddWithValue("@FlatwareSoak", machine_type3_Flatware.IsChecked);
                                    cmd.Parameters.AddWithValue("@MachineWater", machine_type3_Water.IsChecked);
                                    cmd.Parameters.AddWithValue("@PrewashTank", machine_type3_Prewash.Text.Trim());
                                    cmd.Parameters.AddWithValue("@WashTank", machine_type3_WashTank.Text.Trim());
                                    cmd.Parameters.AddWithValue("@RinseTank", machine_type3_RinseTank.Text.Trim());
                                    if (machine_type3_Hi.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", false);
                                    }
                                    else if (machine_type3_Lo.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", true);
                                    }
                                    cmd.Parameters.AddWithValue("@Temp", machine_type3_TempRinse.Text.Trim());

                                    cmd.ExecuteNonQuery();
                                }
                            }

                        }
                        if (machine_type4_mfg.Text != "" && !DoesCurrentMachineExist(4, machine_type4_serial.Text.Trim(), "WarewashMachineList"))
                        {
                            SqlCommand reviewCmd = new SqlCommand($"SELECT * FROM [WarewashMachineList] WHERE [CustNo] = '{customerNo}' AND [MachineNo] = '{4}';", connection);
                            SqlDataReader dr = reviewCmd.ExecuteReader();
                            if (dr.HasRows)
                            {
                                dr.Close();
                            }
                            else
                            {
                                dr.Close();
                                using (SqlCommand cmd = new SqlCommand(warewashMachineInsert, connection))
                                {
                                    cmd.Parameters.AddWithValue("@CustNo", customerNo);
                                    cmd.Parameters.AddWithValue("@MachineNo", Convert.ToInt16("4"));
                                    cmd.Parameters.AddWithValue("@MachineMake", machine_type4_mfg.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineModel", machine_type4_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@MachineSerial", machine_type4_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@location", machine_type4_location.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserMake", machine_type4_dispenser.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserModel", machine_type4_dispenser_model.Text.Trim());
                                    cmd.Parameters.AddWithValue("@DispenserSerial", machine_type4_dispenser_serial.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Voltage", machine_type4_voltage.Text.Trim());
                                    cmd.Parameters.AddWithValue("@Scraping", machine_type4_Scraping.IsChecked);
                                    cmd.Parameters.AddWithValue("@Racking", machine_type4_Racking.IsChecked);
                                    cmd.Parameters.AddWithValue("@FlatwareSoak", machine_type4_Flatware.IsChecked);
                                    cmd.Parameters.AddWithValue("@MachineWater", machine_type4_Water.IsChecked);
                                    cmd.Parameters.AddWithValue("@PrewashTank", machine_type4_Prewash.Text.Trim());
                                    cmd.Parameters.AddWithValue("@WashTank", machine_type4_WashTank.Text.Trim());
                                    cmd.Parameters.AddWithValue("@RinseTank", machine_type4_RinseTank.Text.Trim());
                                    if (machine_type4_Hi.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", false);
                                    }
                                    else if (machine_type4_Lo.IsChecked == true)
                                    {
                                        cmd.Parameters.AddWithValue("@TempType", true);
                                    }
                                    cmd.Parameters.AddWithValue("@Temp", machine_type4_TempRinse.Text.Trim());

                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }

                        if (CommentsWashArms.Text != "")
                        {
                            using (SqlCommand cmd = new SqlCommand(warewashInsert, connection))
                            {
                                cmd.Parameters.AddWithValue("@CustNo", customerNo.ToString());
                                cmd.Parameters.AddWithValue("@WorkOrder", jpWorkOrderNo.Content);
                                cmd.Parameters.AddWithValue("@SerialNo", machine_type1_serial.Text.ToString());
                                cmd.Parameters.AddWithValue("@Visit", AuditVisit.Text.ToString());
                                cmd.Parameters.AddWithValue("@WashArms", CommentsWashArms.Text);
                                cmd.Parameters.AddWithValue("@RinseJets", CommentsRinseJets.Text);
                                cmd.Parameters.AddWithValue("@RinseValves", CommentsValve.Text.Trim());
                                cmd.Parameters.AddWithValue("@OverFlow", CommentsOverflow.Text.Trim());
                                cmd.Parameters.AddWithValue("@ByPass", CommentsBypass.Text.Trim());
                                cmd.Parameters.AddWithValue("@FinalRinsePsi", CommentsFinalRinsePsi.Text.Trim());
                                cmd.Parameters.AddWithValue("@PumpMotor", CommentsPumpMotor.Text.Trim());
                                cmd.Parameters.AddWithValue("@FillValve", CommentsFillValve.Text.Trim());
                                cmd.Parameters.AddWithValue("@Drains", CommentsDrains.Text.Trim());
                                cmd.Parameters.AddWithValue("@TempGauges", CommentsTempGauges.Text.Trim());
                                cmd.Parameters.AddWithValue("@Curtains", CommentsCurtains.Text.Trim());
                                cmd.Parameters.AddWithValue("@Racks", CommentRacks.Text.Trim());
                                cmd.Parameters.AddWithValue("@Doors", CommentsDoors.Text.Trim());
                                cmd.Parameters.AddWithValue("@Detergent", chemical_detergent.Text.Trim());
                                cmd.Parameters.AddWithValue("@DetergentDrops", detergent_drop.Text.Trim());
                                cmd.Parameters.AddWithValue("@RinseAid", chemical_rinse_aid.Text.Trim());
                                cmd.Parameters.AddWithValue("@RinseAidDrops", rinse_drop.Text.Trim());
                                cmd.Parameters.AddWithValue("@Sanitizer", chemical_sanitizer.Text.Trim());
                                cmd.Parameters.AddWithValue("@SanitizerDrops", sanitizer_drop.Text.Trim());
                                cmd.Parameters.AddWithValue("@WaterHardness", chemical_water_hardness.Text.Trim());
                                cmd.Parameters.AddWithValue("@Notes", warewashNotes.Text.Trim());
                                cmd.Parameters.AddWithValue("@CustomerSignature", saveSignature(warewashCustSignature));
                                cmd.Parameters.AddWithValue("@TechSignature", saveSignature(warewashTechSignature));
                                cmd.Parameters.AddWithValue("@AuditContact", warewashCustName.Content);
                                cmd.Parameters.AddWithValue("@AuditTech", warewashTechName.Content);

                                cmd.ExecuteNonQuery();
                            }
                        }

                        log.Info($"Warewash audit has completed : {AuditVisit.SelectedItem.ToString()} / {customerNo}");
                        MessageBox.Show("The audit form has been completed and saved");
                        if (AuditVisit.SelectedIndex != 2 || AuditVisit.SelectedIndex != 3)
                        {
                            var emailAuditReport = pdfMaker.generateWarewashPdf(customerNo, jpWorkOrderNo.Content.ToString());
                            var emailSubject = $"Warewash Audit - {jpWorkOrderNo.Content.ToString()}";
                            //audit, email body, contact email, subject line
                            auditEmail.isOutlook(emailAuditReport, "", custContactCombo.SelectedValue.ToString(), emailSubject);
                        }
                        clearGroup(this);
                        jpWorkOrderNo.Content = jpWO();
                    }
                    catch (Exception ex)
                    {
                        log.Error($"Warewash audit failed : {customerNo} / error : {ex.Message}");
                        MessageBox.Show(ex.Message);
                    }
                }
            else
                return;
        }


        #endregion

        private bool AuditSubmissionCheck(InkCanvas customerSignatureCanvas, InkCanvas techSignatureCanvas)
        {
            bool auditCheck = false;
            if (AuditVisit.SelectedIndex != 0 && techSignatureCanvas.Strokes.Count > 0 && customerSignatureCanvas.Strokes.Count > 0 && chemicalStatus.Content.ToString() != "Chemical Type Missing")
            {
                if (techSelection.SelectedIndex != -1)
                {
                    auditCheck = true;
                }
                else
                {
                    MessageBox.Show("Work Order not assigned to Audit. Please select Service Tech ");
                    ServiceLbl.Foreground = System.Windows.Media.Brushes.Crimson;
                }
            }
            else if (chemicalStatus.Content.ToString() == "Chemical Type Missing")
            {
                MessageBox.Show("Please Assign Chemical Type for Customer: " + customerNo);
                chemicalStatus.Foreground = System.Windows.Media.Brushes.Crimson;
            }
            else if (customerSignatureCanvas.Strokes.Count > 0 || techSignatureCanvas.Strokes.Count > 0)
            {
                VisitingReason.Foreground = System.Windows.Media.Brushes.Crimson;
                MessageBox.Show("Please select reason of visit for Warewash Audit");
            }
            else
            {
                VisitingReason.Foreground = System.Windows.Media.Brushes.Crimson;
                MessageBox.Show("Please select reason of visit for Warewash Audit");
            }

            return auditCheck;
        }

        private bool DoesCurrentMachineExist(int machineNo, string machineSerial, string checkDB)
        {
            bool machineRecordResult = false;
            if (checkDB == "LaundryMachineList")
            {
                var findMachine = $"SELECT ObjId FROM {checkDB} WHERE MachineNo = '{machineNo}' AND MachineSerial = '{machineSerial}' ";
                var updateLaundryMachine = $"UPDATE [dbo].[LaundryMachineList] SET [Level] = @Level, [Temp] = @Temp, [Valves] = @Valves, [Agitation] = @Agitation, [Timer] = @Timer, [Ph] = @Ph, [Bleach] = @Bleach, [Iron] = @Iron, [Chlorine] = @Chlorine, [WateHardness] = @WaterHardness WHERE MachineNo = '{machineNo}' AND MachineSerial = '{machineSerial}' AND CustNo = '{customerNo}' ";
                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(findMachine, connection))
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            machineRecordResult = true;
                        }
                    }

                    TextBox level = this.FindName($"machine{machineNo}_water") as TextBox;
                    TextBox valves = this.FindName($"machine{machineNo}_drain") as TextBox;
                    TextBox agitation = this.FindName($"machine{machineNo}_agitation") as TextBox;
                    TextBox timer = this.FindName($"machine{machineNo}_timer") as TextBox;
                    TextBox phWash = this.FindName($"machine{machineNo}_Ph") as TextBox;
                    TextBox bleach = this.FindName($"machine{machineNo}_BleachPh") as TextBox;
                    ComboBox iron = this.FindName($"machine{machineNo}_Iron") as ComboBox;
                    TextBox chlorine = this.FindName($"machine{machineNo}_chlorine") as TextBox;
                    TextBox hardness = this.FindName($"machine{machineNo}_WaterHardness") as TextBox;
                    TextBox temp = this.FindName($"machine{machineNo}_temp") as TextBox;
                    if (machineRecordResult)
                    {
                        using (SqlCommand command = new SqlCommand(updateLaundryMachine, connection))
                        {
                            command.Parameters.AddWithValue("@Level", level.Text.Trim());
                            command.Parameters.AddWithValue("@Valves", valves.Text.Trim());
                            command.Parameters.AddWithValue("@Agitation", agitation.Text.Trim());
                            command.Parameters.AddWithValue("@Timer", timer.Text.Trim());
                            command.Parameters.AddWithValue("@Ph", Convert.ToDecimal(phWash.Text.Trim()));
                            command.Parameters.AddWithValue("@Bleach", Convert.ToDecimal(bleach.Text.Trim()));
                            command.Parameters.AddWithValue("@Iron", iron.Text.Trim());
                            command.Parameters.AddWithValue("@Chlorine", chlorine.Text.Trim());
                            command.Parameters.AddWithValue("@WaterHardness", hardness.Text.Trim());
                            command.Parameters.AddWithValue("@Temp", temp.Text.Trim());

                            command.ExecuteNonQuery();
                        }
                    }
                }

            }
            else
            {
                var findMachine = $"SELECT ObjId FROM WarewashMachineList WHERE MachineNo = '{machineNo}' AND MachineSerial = '{machineSerial}' ";
                var updateWarewashMachine = $"UPDATE [dbo].[WarewashMachineList] SET [Scraping] = @Scraping,[Racking] = @Racking,[FlatwareSoak] = @FlatwareSoak,[MachineWater] = @MachineWater,[PrewashTank] = @PrewashTank,[WashTank] = @WashTank,[RinseTank] = @RinseTank,[TempType] = @TempType,[Temp] = @Temp WHERE MachineNo = '{machineNo}' AND MachineSerial = '{machineSerial}' AND CustNo = '{customerNo}' ";

                using (var connection = new SqlConnection(localCS))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(findMachine, connection))
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            machineRecordResult = true;
                        }
                    }

                    CheckBox scraping = this.FindName($"machine_type{machineNo}_Scraping") as CheckBox;
                    CheckBox racking = this.FindName($"machine_type{machineNo}_Racking") as CheckBox;
                    CheckBox soak = this.FindName($"machine_type{machineNo}_Flatware") as CheckBox;
                    CheckBox water = this.FindName($"machine_type{machineNo}_Water") as CheckBox;
                    TextBox preWash = this.FindName($"machine_type{machineNo}_Prewash") as TextBox;
                    TextBox mainWash = this.FindName($"machine_type{machineNo}_WashTank") as TextBox;
                    TextBox rinseWash = this.FindName($"machine_type{machineNo}_RinseTank") as TextBox;
                    CheckBox hiTemp = this.FindName($"machine_type{machineNo}_Hi") as CheckBox;
                    CheckBox lowTemp = this.FindName($"machine_type{machineNo}_Lo") as CheckBox;
                    TextBox temp = this.FindName($"machine_type{machineNo}_TempRinse") as TextBox;
                    if (machineRecordResult)
                    {
                        using (SqlCommand command = new SqlCommand(updateWarewashMachine, connection))
                        {
                            command.Parameters.AddWithValue("@Scraping", scraping.IsChecked);
                            command.Parameters.AddWithValue("@Racking", racking.IsChecked);
                            command.Parameters.AddWithValue("@FlatwareSoak", soak.IsChecked);
                            command.Parameters.AddWithValue("@MachineWater", water.IsChecked);
                            command.Parameters.AddWithValue("@PrewashTank", preWash.Text.Trim());
                            command.Parameters.AddWithValue("@WashTank", mainWash.Text.Trim());
                            command.Parameters.AddWithValue("@RinseTank", rinseWash.Text.Trim());
                            if (hiTemp.IsChecked == true)
                            {
                                command.Parameters.AddWithValue("@TempType", false);
                            }
                            else if (lowTemp.IsChecked == true)
                            {
                                command.Parameters.AddWithValue("@TempType", true);
                            }
                            command.Parameters.AddWithValue("@Temp", temp.Text.Trim());

                            command.ExecuteNonQuery();
                        }
                    }
                }
            }

            return machineRecordResult;
        }

        //autocomplete box
        private void CustSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                custSearch.Text.ToUpper();
                if (custSearch.Text.Trim() != "")
                {
                    string getCustData = $"SELECT CustNo from Customers where CustNo = '{custSearch.Text.Trim()}' OR Nickname = '{custSearch.Text.Trim()}' OR AccountName = '{custSearch.Text.Trim()}' ";
                    using (var sqlConnection = new SqlConnection(localCS))
                    {
                        //open sqlconnection
                        sqlConnection.Open();

                        using (SqlCommand sqlCommandCust = new SqlCommand(getCustData, sqlConnection))
                        using (SqlDataReader sqlDataReaderCust = sqlCommandCust.ExecuteReader())
                        {
                            if (sqlDataReaderCust.HasRows)
                            {
                                sqlDataReaderCust.Read();
                                customerNo = sqlDataReaderCust.GetString(0);

                                getCustomerData();
                            }
                        }

                    }
                }
            }
        }

        /*
            This will deal with validation 
            Consider in simplifying for the future
        */
        private static readonly Regex jpNumbers = new Regex("[^0-9]+"); //regex that matches disallowed text
        private static readonly Regex jpDecimal = new Regex("[^0-9.]+"); //regex that matches disallowed text
        private static readonly Regex jpYnN = new Regex("[^0-9.]+"); //regex that matches disallowed text

        private void UpdateContactBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateContactPanel updateContact = new UpdateContactPanel();
            updateContact.ShowDialog();
        }

        private void CompletedAudits_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Would you like to review completed pdf file?", "JohnstonIGo", MessageBoxButton.YesNoCancel);
            var completedAuditCustomer = "";
            var completedAuditWo = "";
            DataGrid dg = (DataGrid)sender;
            DataRowView dv = dg.SelectedItem as DataRowView;

            if (dv != null)
            {
                completedAuditCustomer = dv["CustNo"].ToString();
                completedAuditWo = dv["WorkOrder"].ToString();
            }

            var pdfDrainRecords = $"SELECT AuditPdf FROM DrainAudit WHERE CustNo = '{completedAuditCustomer}' AND WorkOrder = '{completedAuditWo}' ";
            var pdfLaundryRecords = $"SELECT AuditPdf FROM LaundryAudit WHERE CustNo = '{completedAuditCustomer}' AND WorkOrder = '{completedAuditWo}' ";
            var pdfWarewashRecords = $"SELECT AuditPdf FROM WarewashAudit WHERE CustNo = '{completedAuditCustomer}' AND WorkOrder = '{completedAuditWo}' ";
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        using (SqlCommand command = new SqlCommand(pdfDrainRecords, connection))
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                dr.Read();
                                System.Diagnostics.Process.Start(@"C:\temp\PDF\" + dr.GetString(0));

                            }
                        }

                        using (SqlCommand command = new SqlCommand(pdfLaundryRecords, connection))
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                dr.Read();
                                System.Diagnostics.Process.Start(@"C:\temp\PDF\" + dr.GetString(0));

                            }
                        }

                        using (SqlCommand command = new SqlCommand(pdfWarewashRecords, connection))
                        using (SqlDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                dr.Read();
                                System.Diagnostics.Process.Start(@"C:\temp\PDF\" + dr.GetString(0));

                            }
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Cancel:
                        break;
                }
            }

        }

        #region Serial maker

        private void Machine1_serial_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (machine1_serial.Text.Trim() == "jp")
            {
                machine1_serial.Text = SerialMaker();
            }
            if (machine2_serial.Text.Trim() == "jp")
            {
                machine2_serial.Text = SerialMaker();
            }
            if (machine3_serial.Text.Trim() == "jp")
            {
                machine3_serial.Text = SerialMaker();
            }
            if (machine4_serial.Text.Trim() == "jp")
            {
                machine4_serial.Text = SerialMaker();
            }

        }

        private void Machine1_dispenser_serial_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (machine1_dispenser_serial.Text.Trim() == "jp")
            {
                machine1_dispenser_serial.Text = SerialMaker();
            }
            if (machine2_dispenser_serial.Text.Trim() == "jp")
            {
                machine2_dispenser_serial.Text = SerialMaker();
            }
            if (machine3_dispenser_serial.Text.Trim() == "jp")
            {
                machine3_dispenser_serial.Text = SerialMaker();
            }
            if (machine4_dispenser_serial.Text.Trim() == "jp")
            {
                machine4_dispenser_serial.Text = SerialMaker();
            }
        }

        private void Machine_type1_serial_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (machine_type1_serial.Text.Trim() == "jp")
            {
                machine_type1_serial.Text = SerialMaker();
            }
            if (machine_type2_serial.Text.Trim() == "jp")
            {
                machine_type2_serial.Text = SerialMaker();
            }
            if (machine_type3_serial.Text.Trim() == "jp")
            {
                machine_type3_serial.Text = SerialMaker();
            }
            if (machine_type4_serial.Text.Trim() == "jp")
            {
                machine_type4_serial.Text = SerialMaker();
            }
        }

        private void Machine_type1_dispenser_serial_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (machine_type1_dispenser_serial.Text.Trim() == "jp")
            {
                machine_type1_dispenser_serial.Text = SerialMaker();
            }
            if (machine_type2_dispenser_serial.Text.Trim() == "jp")
            {
                machine_type2_dispenser_serial.Text = SerialMaker();
            }
            if (machine_type3_dispenser_serial.Text.Trim() == "jp")
            {
                machine_type3_dispenser_serial.Text = SerialMaker();
            }
            if (machine_type4_dispenser_serial.Text.Trim() == "jp")
            {
                machine_type4_dispenser_serial.Text = SerialMaker();
            }
        }

        #endregion

        #region numbers only
        //
        private void Machine1_pump_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine2_pump_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine3_pump_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine4_pump_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Machine1_wash_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }
        private void Machine2_wash_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }
        private void Machine3_wash_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }
        private void Machine4_wash_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }

        //
        private void Machine1_bleach_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }
        private void Machine2_bleach_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }
        private void Machine3_bleach_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }
        private void Machine4_bleach_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }

        //
        private void Machine1_chlorine_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine2_chlorine_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine3_chlorine_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine4_chlorine_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Machine1_hardness_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine2_hardness_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine3_hardness_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine4_hardness_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text); e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Machine1_temp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine2_temp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine3_temp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine4_temp_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Fabric_final_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpDecimal.IsMatch(e.Text);
        }

        //
        private void Machine_type1_pre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type2_pre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type3_pre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type4_pre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Machine_type1_wash_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type2_wash_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type3_wash_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type4_wash_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Machine_type1_rinse_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type2_rinse_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type3_rinse_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type4_rinse_tank_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Machine_type1_temp_rinse_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type2_temp_rinse_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type3_temp_rinse_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type4_temp_rinse_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        //
        private void Machine_type1_voltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type2_voltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type3_voltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }
        private void Machine_type4_voltage_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        private void Comments_final_rinse_psi_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        private void Chemical_water_hardness_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        private void Detergent_drop_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        private void Rinse_drop_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        private void Sanitizer_drop_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        private void Water_drop_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = jpNumbers.IsMatch(e.Text);
        }

        #endregion

        #region Image capture and save
        private void LaundryImg1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(1, machine1_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        private void LaundryImg2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name, MachineNo FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(2, machine2_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        private void LaundryImg3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name, MachineNo FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(3, machine3_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        private void LaundryImg4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name, MachineNo FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(4, machine4_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        private void WarewashMachine1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name, MachineNo FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(1, machine_type1_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        private void WarewashMachine2_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name, MachineNo FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(2, machine_type2_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        private void WarewashMachine3_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name, MachineNo FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(3, machine_type3_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        private void WarewashMachine4_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            string mSelect = "SELECT Name, MachineNo FROM MachineMedia WHERE CustNo  = '" + customerNo + "';";
            ImageSelection machineImages = new ImageSelection(4, machine_type4_serial.Text, customerNo);
            using (var connection = new SqlConnection(localCS))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(mSelect, connection))
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //machineImages.
                        machineImages.Show();
                    }
                    else
                    {
                        machineImages.Show();
                    }
                }
                connection.Close();
            }
        }

        #endregion

        private void AuditFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                using (var connection = new SqlConnection(remoteCS))
                {
                    try
                    {
                        var searchRemoteWO = $"SELECT FROM INNER JOIN ON WHERE ('{auditFilter.Text.ToString()}'OR {auditFilter.Text.ToString()} OR {auditFilter.Text.ToString()})";

                        var SelectByAccountName = "SELECT Customers.CustNo, Customers.AccountName, DrainAudit.WorkOrder, DrainAudit.Visit, DrainAudit.AuditCreation, Customers.Nickname, 'Drain Audit' as 'AuditType' FROM Customers INNER JOIN DrainAudit ON Customers.CustNo = DrainAudit.CustNo WHERE (Customers.AccountName = '" + auditFilter.Text + "' OR Customers.Nickname = '" + auditFilter.Text + "' OR DrainAudit.CustNo = '" + auditFilter.Text + "')" +
                            "UNION " +
                            "SELECT Customers.CustNo, Customers.AccountName, LaundryAudit.WorkOrder, LaundryAudit.Visit, LaundryAudit.AuditCreation, Customers.Nickname, 'Laundry Audit' as 'AuditType' FROM Customers INNER JOIN LaundryAudit ON Customers.CustNo = LaundryAudit.CustNo WHERE (Customers.AccountName = '" + auditFilter.Text + "' OR Customers.Nickname = '" + auditFilter.Text + "' OR LaundryAudit.CustNo = '" + auditFilter.Text + "')" +
                            "UNION " +
                            "SELECT Customers.CustNo, Customers.AccountName, WarewashAudit.WorkOrder, WarewashAudit.Visit, WarewashAudit.AuditCreation, Customers.Nickname, 'Warewash Audit' as 'AuditType' FROM Customers INNER JOIN WarewashAudit ON Customers.CustNo = WarewashAudit.CustNo WHERE (Customers.AccountName = '" + auditFilter.Text + "' OR Customers.Nickname = '" + auditFilter.Text + "' OR WarewashAudit.CustNo = '" + auditFilter.Text + "')";

                        connection.Open();

                        using (SqlCommand cmd = new SqlCommand(SelectByAccountName, connection))
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                DataTable dt = new DataTable();
                                dt.Load(dr);
                                completedAudits.ItemsSource = dt.DefaultView;
                            }
                            else
                            {
                                MessageBox.Show("There are no results with " + auditFilter.Text);
                                loadAudits();
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                    }
                }

            }

        }

        //future relase concept
        private void EmailPdf_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var completedAuditCustomer = "";
            var completedAuditDate = "";

            DataGrid dg = (DataGrid)sender;
            DataRowView dv = dg.SelectedItem as DataRowView;

            if (dv != null)
            {
                completedAuditCustomer = dv["CustNo"].ToString();
                completedAuditDate = dv["AuditCreation"].ToString();
            }

            try
            {
                MessageBox.Show(completedAuditDate);
            }
            catch (Exception ex)
            {
                log.Error("Error with eemail attempt: " + ex.Message);
            }
        }

        private void ViewPdf_MouseDown(object sender, MouseButtonEventArgs e)
        {

            DataRowView auditRow = (DataRowView)((Image)e.Source).DataContext;
            

            var aCustomer = auditRow.Row.ItemArray[0].ToString();
            var aWorkOrder = auditRow.Row.ItemArray[2].ToString();
            var aType= auditRow.Row.ItemArray[6].ToString();
            var auditName = auditRow.Row.ItemArray[7].ToString();

            MessageBoxResult regenerateAudit = MessageBox.Show("Are you sure you want to recreate audit?", "JohnstonIGo", MessageBoxButton.YesNoCancel);
            var removeFile = $@"C:\temp\PDF\{auditName}";
            var auditResults = string.Empty;
            try
            {
                switch (regenerateAudit)
                {
                    case MessageBoxResult.Yes:
                        if (File.Exists(removeFile))
                        {
                            File.Delete(removeFile);
                        }
                        switch (aType)
                        {
                            case "Drain Audit":
                                auditResults = pdfMaker.generateDrainPdf(aCustomer, aWorkOrder);
                                break;
                            case "Laundry Audit":
                                auditResults = pdfMaker.generateLaundryPdf(aCustomer, aWorkOrder);
                                break;
                            case "Emergency Audit":
                                auditResults = pdfMaker.GenerateEmergencyPdf(aCustomer, aWorkOrder);
                                break;
                            default:
                                auditResults = pdfMaker.generateWarewashPdf(aCustomer, aWorkOrder);
                                break;
                        }
                        break;
                    case MessageBoxResult.No:
                        break;
                }
                log.Info($"Audit Regenerated Successfully {regenerateAudit} : {auditResults}");
            }
            catch (Exception ex)
            {
                log.Error($"Error regenerating audit {regenerateAudit} : {ex.Message}");
            }

        }

        private async void SyncLocalData_Click(object sender, RoutedEventArgs e)
        {
            SyncLocalData.IsEnabled = false;
            syncProgress.Visibility = Visibility.Visible;
            syncProgress.IsIndeterminate = true;

            List<string> tblsPost = new List<string>()
            {
                "Contacts","MachineMedia","DrainAudit","LaundryAudit","LaundryMachineList","WarewashAudit","WarewashMachineList","Customers","EmergencyAudit"
            };
            foreach (var postTbl in tblsPost)
            {
                await dbMaker.ServicePostAsync($"{postTbl}");
            }
            dbMaker.UpdatePriorityDBAsync();
            List<string> tblsGet = new List<string>()
            {
                "Contacts","Customers","ServiceTechs","MachineMedia", "CustomerAddresses"
            };
            foreach (var getTbl in tblsGet)
            {
                await dbMaker.ServiceConsumeAsync(getTbl);
            }

            await s3Upload.UploadDirAsync();

            log.Info($"Johnston Cloud Sync Completed");
            SyncLocalData.IsEnabled = true;
            syncProgress.IsIndeterminate = false;
            syncProgress.Visibility = Visibility.Hidden;

        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Admin adminPanel = new Admin();
            adminPanel.ShowDialog();
        }

        private void AddLaundryMachine_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GroupBox laundryGB = new GroupBox();

                laundryGB.Margin = new Thickness(10, 442, 10, 0);
                laundryGB.Width = 827;
                laundryGB.Height = 383;
                laundryGB.Header = "Test";
                laundryGB.VerticalAlignment = VerticalAlignment.Top;
                laundryGB.Visibility = Visibility.Visible;

                List<string> laundryControls = new List<string>()
                {
                    "LaundryMake",
                    "LaundryModel",
                    "LaundryCapacity",
                    "LaundrySerial",
                    "LaundryDispenserMake",
                    "LaundryDispenserModel",
                    "LaundryDispenserSerial",
                    "LaundryPumpNo",
                    "LaundryWaterLevel",
                    "LaundryDrainValves",
                    "LaundryAgitation",
                    "LaundryTimer",
                    "LaundryWashPh",
                    "LaundryBleachPh",
                    "LaundryIron",
                    "LaundryChlorine",
                    "LaundryWaterHardness",
                    "LaundryWaterTemp"
                };
                foreach (var laundryControl in laundryControls)
                {
                    TextBox laundryTxt = new TextBox();
                    if (laundryControl == "LaundryIron")
                    {

                    }
                    else
                    {

                        laundryTxt.Name = laundryControl;
                        MessageBox.Show(laundryTxt.Name);
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AuditTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
               
        }

        private void AuditTabs_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
            
        }

        private void TabItem_GotFocus(object sender, RoutedEventArgs e)
        {
            loadAudits();
        }
    }
}
