﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JohnstoniGo_Windows
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            loadServiceUser();
        }

        private void loadServiceUser()
        {
            
        }

        private void LoginBtn_Click(object sender, RoutedEventArgs e)
        {
            var userSql = $"SELECT TechId FROM WHERE Name = {userSelect.SelectedItem} AND Password = {passwordTxt.Text}";
            using (SqlConnection connect = new SqlConnection())
            {
                connect.Open();
                using (SqlCommand command = new SqlCommand(userSql, connect))
                using(SqlDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        //Properties.Settings.Default.userLogin = dr.GetString(0);
                        //Properties.Settings.Default.Save();
                    }
                    else
                    {
                        MessageBox.Show("Incorrect user/password combination");
                    }
                }
            }
        }
    }
}
