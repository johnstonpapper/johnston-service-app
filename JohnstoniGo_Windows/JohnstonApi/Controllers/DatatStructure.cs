﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JohnstonApi
{
    public class ServiceTechs
    {
        public int ObjId { get; set; }
        public string TechId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public object Phone { get; set; }
        public bool Retired { get; set; }
    }

    public class Customers
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string AccountName { get; set; }
        public string Contact { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string InsideSales { get; set; }
        public string InsideSaleEmail { get; set; }
        public string SalesRep { get; set; }
        public string SaleRepEmail { get; set; }
        public string ServiceTech { get; set; }
        public string ServiceTechEmail { get; set; }
        public string CustomerServiceRep { get; set; }
        public string CustomerServiceRepEmail { get; set; }
        public string Nickname { get; set; }
    }

    public class CustomerAddresses
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string ShipNo { get; set; }
        public string Name { get; set; }
        public string Atn { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
    }

    public class Contacts
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string ChemicalType { get; set; }
    }

    public class MachineMedia
    {
        public int ObjId { get; set; }
        public int MachineNo { get; set; }
        public string SerialNo { get; set; }
        public string CustNo { get; set; }
        public string Name { get; set; }
        public string MediaType { get; set; }
        public DateTime Creation { get; set; }
    }

    #region Audit types
    public class DrainAudit
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string WorkOrder { get; set; }
        public string Visit { get; set; }
        public bool Pump { get; set; }
        public DateTime TubeChangeLast { get; set; }
        public DateTime BatteryChangeLast { get; set; }
        public bool Pail { get; set; }
        public bool Odors { get; set; }
        public bool Blockage { get; set; }
        public bool Floor { get; set; }
        public bool Sink { get; set; }
        public string Notes { get; set; }
        public string AuditPdf { get; set; }
        public string AuditTech { get; set; }
        public string TechSignature { get; set; }
        public string AuditContact { get; set; }
        public string CustSignature { get; set; }
        public DateTime AuditCreation { get; set; }
    }

    public class EmergencyAudit
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string WorkOrder { get; set; }
        public string Visit { get; set; }
        public string Notes { get; set; }
        public string AuditPdf { get; set; }
        public string AuditTech { get; set; }
        public string AuditTechSignature { get; set; }
        public string AuditContact { get; set; }
        public string AuditContactSignature { get; set; }
        public DateTime AuditCreation { get; set; }

    }
    public class LaundryAudit
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string WorkOrder { get; set; }
        public string Visit { get; set; }
        public string FabricAppearance { get; set; }
        public float FabricFinal { get; set; }
        public string FabricOdor { get; set; }
        public string FabricFeel { get; set; }
        public string FabricStain { get; set; }
        public string FabricColor { get; set; }
        public bool ProcedureCollection { get; set; }
        public bool ProcedureSorting { get; set; }
        public bool ProcedurePre { get; set; }
        public bool ProcedureLoading { get; set; }
        public bool ProcedureReclaim { get; set; }
        public bool ProcedureReject { get; set; }
        public string Notes { get; set; }
        public string AuditPdf { get; set; }
        public string AuditTech { get; set; }
        public string TechSignature { get; set; }
        public string AuditContact { get; set; }
        public string CustSignature { get; set; }
        public DateTime AuditCreation { get; set; }
    }

    public class WarewashAudit
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public string WorkOrder { get; set; }
        public string SerialNo { get; set; }
        public string Visit { get; set; }
        public string WashArms { get; set; }
        public string RinseJets { get; set; }
        public string RinseValves { get; set; }
        public string OverFlow { get; set; }
        public string ByPass { get; set; }
        public int FinalRinsePsi { get; set; }
        public string PumpMotor { get; set; }
        public string FillValve { get; set; }
        public string Drains { get; set; }
        public string TempGauges { get; set; }
        public string Curtains { get; set; }
        public string Racks { get; set; }
        public string Doors { get; set; }
        public string Detergent { get; set; }
        public int DetergentDrops { get; set; }
        public string RinseAid { get; set; }
        public int RinseAidDrops { get; set; }
        public string Sanitizer { get; set; }
        public int SanitizerDrops { get; set; }
        public int WaterHardness { get; set; }
        public string Notes { get; set; }
        public string AuditPdf { get; set; }
        public string AuditContact { get; set; }
        public string CustomerSignature { get; set; }
        public string AuditTech { get; set; }
        public string TechSignature { get; set; }
        public DateTime AuditCreation { get; set; }
    }

    #endregion

    #region Audit Machine

    public class WarewashMachineList
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public int MachineNo { get; set; }
        public string MachineMake { get; set; }
        public string MachineModel { get; set; }
        public string MachineSerial { get; set; }
        public string location { get; set; }
        public string DispenserMake { get; set; }
        public string DispenserModel { get; set; }
        public string DispenserSerial { get; set; }
        public int Voltage { get; set; }
        public bool Scraping { get; set; }
        public bool Racking { get; set; }
        public bool FlatwareSoak { get; set; }
        public bool MachineWater { get; set; }
        public int PrewashTank { get; set; }
        public int WashTank { get; set; }
        public int RinseTank { get; set; }
        public bool TempType { get; set; }
        public int Temp { get; set; }
        public object MachineExpiration { get; set; }
        public DateTime MachineCreation { get; set; }
    }

    public class LaundryMachineList
    {
        public int ObjId { get; set; }
        public string CustNo { get; set; }
        public int MachineNo { get; set; }
        public string MachineMake { get; set; }
        public string MachineModel { get; set; }
        public string MachineSerial { get; set; }
        public string MachineCapacity { get; set; }
        public string DispenserMake { get; set; }
        public string DispenserModel { get; set; }
        public string DispenserSerial { get; set; }
        public int NoPump { get; set; }
        public string Level { get; set; }
        public int Temp { get; set; }
        public string Valves { get; set; }
        public string Agitation { get; set; }
        public string Timer { get; set; }
        public float Ph { get; set; }
        public float Bleach { get; set; }
        public string Iron { get; set; }
        public int Chlorine { get; set; }
        public int WateHardness { get; set; }
        public object MachineExpirations { get; set; }
        public DateTime MachineCreation { get; set; }
    }

    #endregion
    
}
