﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JohnstonApi.Controllers
{
    public class ServiceController : ApiController
    {
        //string pegasusDB = ConfigurationManager.ConnectionStrings["ApiConntectionAzure"].ConnectionString;
        readonly string pegasusDB = ConfigurationManager.ConnectionStrings["ApiInternalConntection"].ConnectionString;

        // GET: api/JohnstonService/5
        public string GetSqlContent(string name)
        {
            var serviceContent = $"SELECT * FROM {name}";
            var dt = new DataTable();

            var sqlResponse = string.Empty;
            using (var connection = new SqlConnection(pegasusDB))
            {
                connection.Open();
                try
                {
                    //DataSet ds = new DataSet();
                    using (var da = new SqlDataAdapter(serviceContent, connection))
                    {
                        da.Fill(dt);
                    }

                    sqlResponse = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }
                catch (Exception ex)
                {
                    sqlResponse = ex.Message;
                }
            }

            return sqlResponse;
        }

        // POST: api/JohnstonService
        public string PostServiceUpdate(string name, [FromBody] JToken jsonCollection)
        {
            var sqlJsonResponse = string.Empty;
            var commaDelimited = string.Empty;
            var sqlParamaters = string.Empty;
            var responseMsg = string.Empty;

            var recordExist = $"SELECT * FROM [JP_Service].[dbo].{name}";

            StringBuilder sb = new StringBuilder();
            List<string> sqlSyntax = new List<string>();
            List<string> sqlParams = new List<string>();
            var dt = new DataTable();
            
            using (var connection = new SqlConnection(pegasusDB))
            {
                try
                {
                    connection.Open();
                    using (var da = new SqlDataAdapter(recordExist, connection))
                    {
                        da.Fill(dt);
                    }
                    var classObj = $"JohnstonApi.{name}";
                    Type type = Type.GetType(classObj, true);
                    var objName = Activator.CreateInstance(type);

                    foreach (var c in objName.GetType().GetProperties())
                    {
                        sqlSyntax.Add($"{c.Name}");
                        sqlParams.Add($"@{c.Name}");
                    }
                    sqlSyntax.RemoveAt(0);
                    sqlParams.RemoveAt(0);
                    commaDelimited = string.Join(",", sqlSyntax);
                    sqlParamaters = string.Join(",", sqlParams);
                    sb.Append($"INSERT INTO {name} ({commaDelimited}) VALUES ({sqlParamaters})");

                    switch (name)
                    {
                        case "Customers":
                            var customers = jsonCollection.ToObject<Customers[]>();
                            foreach (var customer in customers)
                            {
                                sqlParamaters = $"UPDATE [dbo].[{name}] SET [Nickname] = @Nickname WHERE CustNo = '{customer.CustNo}' ";

                                using (SqlCommand command = new SqlCommand(sqlParamaters, connection))
                                {
                                    command.Parameters.AddWithValue("@Nickname", customer.Nickname);

                                    command.ExecuteNonQuery();
                                    command.Parameters.Clear();
                                }
                            }
                            break;
                        case "Contacts":
                            var contacts = jsonCollection.ToObject<Contacts[]>();
                            
                            foreach (var contact in contacts)
                            {
                                var nothingFound = dt.Select($"CustNo = '{contact.CustNo}' AND Contact = '{contact.Contact}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", contact.CustNo);
                                        command.Parameters.AddWithValue("@Contact", contact.Contact);
                                        command.Parameters.AddWithValue("@Email", contact.Email);
                                        command.Parameters.AddWithValue("@Department", contact.Department);
                                        command.Parameters.AddWithValue("@Title", contact.Title);
                                        command.Parameters.AddWithValue("@Phone", contact.Phone);
                                        command.Parameters.AddWithValue("@ChemicalType", contact.ChemicalType);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "MachineMedia":
                            var images = jsonCollection.ToObject<MachineMedia[]>();

                            foreach (var media in images)
                            {
                                var nothingFound = dt.Select($"CustNo = '{media.CustNo}' AND SerialNo = '{media.SerialNo}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", media.CustNo);
                                        command.Parameters.AddWithValue("@MachineNo", media.MachineNo);
                                        command.Parameters.AddWithValue("@Name", media.Name);
                                        command.Parameters.AddWithValue("@MediaType", media.MediaType);
                                        command.Parameters.AddWithValue("@Creation", media.Creation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "DrainAudit":
                            var drainAudits = jsonCollection.ToObject<DrainAudit[]>();

                            foreach (var drainAudit in drainAudits)
                            {
                                var nothingFound = dt.Select($"CustNo = '{drainAudit.CustNo}' AND WorkOrder = '{drainAudit.WorkOrder}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", drainAudit.CustNo);
                                        command.Parameters.AddWithValue("@WorkOrder", drainAudit.WorkOrder);
                                        command.Parameters.AddWithValue("@Visit", drainAudit.Visit);
                                        command.Parameters.AddWithValue("@Pump", drainAudit.Pump);
                                        command.Parameters.AddWithValue("@TubeChangeLast", drainAudit.TubeChangeLast);
                                        command.Parameters.AddWithValue("@BatteryChangeLast", drainAudit.BatteryChangeLast);
                                        command.Parameters.AddWithValue("@Pail", drainAudit.Pail);
                                        command.Parameters.AddWithValue("@Odors", drainAudit.Odors);
                                        command.Parameters.AddWithValue("@Blockage", drainAudit.Blockage);
                                        command.Parameters.AddWithValue("@Floor", drainAudit.Floor);
                                        command.Parameters.AddWithValue("@Sink", drainAudit.Sink);
                                        command.Parameters.AddWithValue("@Notes", drainAudit.Notes);
                                        command.Parameters.AddWithValue("@AuditPdf", drainAudit.AuditPdf);
                                        command.Parameters.AddWithValue("@AuditTech", drainAudit.AuditTech);
                                        command.Parameters.AddWithValue("@AuditContact", drainAudit.AuditContact);
                                        command.Parameters.AddWithValue("@TechSignature", drainAudit.TechSignature);
                                        command.Parameters.AddWithValue("@CustSignature", drainAudit.CustSignature);
                                        command.Parameters.AddWithValue("@AuditCreation", drainAudit.AuditCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "LaundryAudit":
                            var laundryAudits = jsonCollection.ToObject<LaundryAudit[]>();

                            foreach (var laundryAudit in laundryAudits)
                            {
                                var nothingFound = dt.Select($"CustNo = '{laundryAudit.CustNo}' AND WorkOrder = '{laundryAudit.WorkOrder}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", laundryAudit.CustNo);
                                        command.Parameters.AddWithValue("@WorkOrder", laundryAudit.WorkOrder);
                                        command.Parameters.AddWithValue("@Visit", laundryAudit.Visit);
                                        command.Parameters.AddWithValue("@FabricAppearance", laundryAudit.FabricAppearance);
                                        command.Parameters.AddWithValue("@FabricFinal", laundryAudit.FabricFinal);
                                        command.Parameters.AddWithValue("@FabricOdor", laundryAudit.FabricOdor);
                                        command.Parameters.AddWithValue("@FabricFeel", laundryAudit.FabricFeel);
                                        command.Parameters.AddWithValue("@FabricStain", laundryAudit.FabricStain);
                                        command.Parameters.AddWithValue("@FabricColor", laundryAudit.FabricColor);
                                        command.Parameters.AddWithValue("@ProcedureCollection", laundryAudit.ProcedureCollection);
                                        command.Parameters.AddWithValue("@ProcedureSorting", laundryAudit.ProcedureSorting);
                                        command.Parameters.AddWithValue("@ProcedureLoading", laundryAudit.ProcedureLoading);
                                        command.Parameters.AddWithValue("@ProcedureReclaim", laundryAudit.ProcedureReclaim);
                                        command.Parameters.AddWithValue("@ProcedureReject", laundryAudit.ProcedureReject);
                                        command.Parameters.AddWithValue("@ProcedurePre", laundryAudit.ProcedurePre);
                                        command.Parameters.AddWithValue("@AuditPdf", laundryAudit.AuditPdf);
                                        command.Parameters.AddWithValue("@Notes", laundryAudit.Notes);
                                        command.Parameters.AddWithValue("@AuditTech", laundryAudit.AuditTech);
                                        command.Parameters.AddWithValue("@AuditContact", laundryAudit.AuditContact);
                                        command.Parameters.AddWithValue("@TechSignature", laundryAudit.TechSignature);
                                        command.Parameters.AddWithValue("@CustSignature", laundryAudit.CustSignature);
                                        command.Parameters.AddWithValue("@AuditCreation", laundryAudit.AuditCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "WarewashAudit":
                            var warewashAudits = jsonCollection.ToObject<WarewashAudit[]>();

                            foreach (var warewashAudit in warewashAudits)
                            {
                                var nothingFound = dt.Select($"CustNo = '{warewashAudit.CustNo}' AND WorkOrder = '{warewashAudit.WorkOrder}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", warewashAudit.CustNo);
                                        command.Parameters.AddWithValue("@WorkOrder", warewashAudit.WorkOrder);
                                        command.Parameters.AddWithValue("@Visit", warewashAudit.Visit);
                                        command.Parameters.AddWithValue("@SerialNo", warewashAudit.SerialNo);
                                        command.Parameters.AddWithValue("@WashArms", warewashAudit.WashArms);
                                        command.Parameters.AddWithValue("@RinseJets", warewashAudit.RinseJets);
                                        command.Parameters.AddWithValue("@RinseValves", warewashAudit.RinseValves);
                                        command.Parameters.AddWithValue("@OverFlow", warewashAudit.OverFlow);
                                        command.Parameters.AddWithValue("@ByPass", warewashAudit.ByPass);
                                        command.Parameters.AddWithValue("@FinalRinsePsi", warewashAudit.FinalRinsePsi);
                                        command.Parameters.AddWithValue("@PumpMotor", warewashAudit.PumpMotor);
                                        command.Parameters.AddWithValue("@FillValve", warewashAudit.FillValve);
                                        command.Parameters.AddWithValue("@Drains", warewashAudit.Drains);
                                        command.Parameters.AddWithValue("@TempGauges", warewashAudit.TempGauges);
                                        command.Parameters.AddWithValue("@Curtains", warewashAudit.Curtains);
                                        command.Parameters.AddWithValue("@Racks", warewashAudit.Racks);
                                        command.Parameters.AddWithValue("@Doors", warewashAudit.Doors);
                                        command.Parameters.AddWithValue("@Detergent", warewashAudit.Detergent);
                                        command.Parameters.AddWithValue("@DetergentDrops", warewashAudit.DetergentDrops);
                                        command.Parameters.AddWithValue("@RinseAid", warewashAudit.RinseAid);
                                        command.Parameters.AddWithValue("@RinseAidDrops", warewashAudit.RinseAidDrops);
                                        command.Parameters.AddWithValue("@Sanitizer", warewashAudit.Sanitizer);
                                        command.Parameters.AddWithValue("@SanitizerDrops", warewashAudit.SanitizerDrops);
                                        command.Parameters.AddWithValue("@WaterHardness", warewashAudit.WaterHardness);
                                        command.Parameters.AddWithValue("@Notes", warewashAudit.Notes);
                                        command.Parameters.AddWithValue("@AuditPdf", warewashAudit.AuditPdf);
                                        command.Parameters.AddWithValue("@AuditTech", warewashAudit.AuditTech);
                                        command.Parameters.AddWithValue("@AuditContact", warewashAudit.AuditContact);
                                        command.Parameters.AddWithValue("@TechSignature", warewashAudit.TechSignature);
                                        command.Parameters.AddWithValue("@CustomerSignature", warewashAudit.CustomerSignature);
                                        command.Parameters.AddWithValue("@AuditCreation", warewashAudit.AuditCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "LaundryMachineList":
                            var laundryMachineLists = jsonCollection.ToObject<LaundryMachineList[]>();
                            
                            foreach (var laundryMachineList in laundryMachineLists)
                            {
                                var nothingFound = dt.Select($"CustNo = '{laundryMachineList.CustNo}' AND MachineSerial = '{laundryMachineList.MachineSerial}' AND MachineCreation = '{laundryMachineList.MachineCreation}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", laundryMachineList.CustNo);
                                        command.Parameters.AddWithValue("@MachineSerial", laundryMachineList.MachineSerial);
                                        command.Parameters.AddWithValue("@MachineNo", laundryMachineList.MachineNo);
                                        command.Parameters.AddWithValue("@MachineMake", laundryMachineList.MachineMake);
                                        command.Parameters.AddWithValue("@MachineModel", laundryMachineList.MachineModel);
                                        command.Parameters.AddWithValue("@MachineCapacity", laundryMachineList.MachineCapacity);
                                        command.Parameters.AddWithValue("@DispenserMake", laundryMachineList.DispenserMake);
                                        command.Parameters.AddWithValue("@DispenserModel", laundryMachineList.DispenserModel);
                                        command.Parameters.AddWithValue("@DispenserSerial", laundryMachineList.DispenserSerial);
                                        command.Parameters.AddWithValue("@NoPump", laundryMachineList.NoPump);
                                        command.Parameters.AddWithValue("@Level", laundryMachineList.Level);
                                        command.Parameters.AddWithValue("@Valves", laundryMachineList.Valves);
                                        command.Parameters.AddWithValue("@Agitation", laundryMachineList.Agitation);
                                        command.Parameters.AddWithValue("@Timer", laundryMachineList.Timer);
                                        command.Parameters.AddWithValue("@Ph", laundryMachineList.Ph);
                                        command.Parameters.AddWithValue("@Bleach", laundryMachineList.Bleach);
                                        command.Parameters.AddWithValue("@Iron", laundryMachineList.Iron);
                                        command.Parameters.AddWithValue("@Chlorine", laundryMachineList.Chlorine);
                                        command.Parameters.AddWithValue("@WateHardness", laundryMachineList.WateHardness);
                                        command.Parameters.AddWithValue("@Temp", laundryMachineList.Temp);
                                        command.Parameters.AddWithValue("@MachineExpirations", laundryMachineList.MachineExpirations ?? DBNull.Value);
                                        command.Parameters.AddWithValue("@MachineCreation", laundryMachineList.MachineCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                    sb.Clear();
                                }
                                else
                                {
                                    sqlParamaters = $"UPDATE [dbo].[{name}] SET [MachineExpiration] = @MachineExpiration WHERE CustNo = '{laundryMachineList.CustNo}' AND MachineCreation = '{laundryMachineList.MachineCreation}' ";
                                    using (var command = new SqlCommand(sqlParamaters,connection))
                                    {
                                        command.Parameters.AddWithValue("@MachineExpirations", laundryMachineList.MachineExpirations ?? DBNull.Value);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "WarewashMachineList":
                            var warewashMachineLists = jsonCollection.ToObject<WarewashMachineList[]>();
                            
                            foreach (var parsedJson in warewashMachineLists)
                            {
                                var nothingFound = dt.Select($"CustNo = '{parsedJson.CustNo}' AND MachineSerial = '{parsedJson.MachineSerial}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", parsedJson.CustNo);
                                        command.Parameters.AddWithValue("@MachineSerial", parsedJson.MachineSerial);
                                        command.Parameters.AddWithValue("@MachineNo", parsedJson.MachineNo);
                                        command.Parameters.AddWithValue("@MachineMake", parsedJson.MachineMake);
                                        command.Parameters.AddWithValue("@MachineModel", parsedJson.MachineModel);
                                        command.Parameters.AddWithValue("@location", parsedJson.location);
                                        command.Parameters.AddWithValue("@DispenserMake", parsedJson.DispenserMake);
                                        command.Parameters.AddWithValue("@DispenserModel", parsedJson.DispenserModel);
                                        command.Parameters.AddWithValue("@DispenserSerial", parsedJson.DispenserSerial);
                                        command.Parameters.AddWithValue("@Voltage", parsedJson.Voltage);
                                        command.Parameters.AddWithValue("@Scraping", parsedJson.Scraping);
                                        command.Parameters.AddWithValue("@Racking", parsedJson.Racking);
                                        command.Parameters.AddWithValue("@FlatwareSoak", parsedJson.FlatwareSoak);
                                        command.Parameters.AddWithValue("@MachineWater", parsedJson.MachineWater);
                                        command.Parameters.AddWithValue("@PrewashTank", parsedJson.PrewashTank);
                                        command.Parameters.AddWithValue("@WashTank", parsedJson.WashTank);
                                        command.Parameters.AddWithValue("@RinseTank", parsedJson.RinseTank);
                                        command.Parameters.AddWithValue("@TempType", parsedJson.TempType);
                                        command.Parameters.AddWithValue("@Temp", parsedJson.Temp);
                                        command.Parameters.AddWithValue("@MachineExpiration", parsedJson.MachineExpiration ?? DBNull.Value);
                                        command.Parameters.AddWithValue("@MachineCreation", parsedJson.MachineCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                                else
                                {
                                    sqlParamaters = $"UPDATE [dbo].[{name}] SET [MachineExpiration] = @MachineExpiration WHERE CustNo = '{parsedJson.CustNo}' AND MachineCreation = '{parsedJson.MachineCreation}' ";
                                    using (var command = new SqlCommand(sqlParamaters, connection))
                                    {
                                        command.Parameters.AddWithValue("@MachineExpirations", parsedJson.MachineExpiration ?? DBNull.Value);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                        case "EmergencyAudit":
                            var emergencyAudits = jsonCollection.ToObject<EmergencyAudit[]>();

                            foreach (var emergencyAudit in emergencyAudits)
                            {
                                var nothingFound = dt.Select($"CustNo = '{emergencyAudit.CustNo}' AND WorkOrder = '{emergencyAudit.WorkOrder}' ");
                                if (nothingFound.Length == 0)
                                {
                                    using (var command = new SqlCommand(sb.ToString(), connection))
                                    {
                                        command.Parameters.AddWithValue("@CustNo", emergencyAudit.CustNo);
                                        command.Parameters.AddWithValue("@WorkOrder", emergencyAudit.WorkOrder);
                                        command.Parameters.AddWithValue("@Visit", emergencyAudit.Visit);
                                        command.Parameters.AddWithValue("@Notes", emergencyAudit.Notes);
                                        command.Parameters.AddWithValue("@AuditPdf", emergencyAudit.AuditPdf);
                                        command.Parameters.AddWithValue("@AuditTech", emergencyAudit.AuditTech);
                                        command.Parameters.AddWithValue("@AuditTechSignature", emergencyAudit.AuditTechSignature);
                                        command.Parameters.AddWithValue("@AuditContact", emergencyAudit.AuditContact);
                                        command.Parameters.AddWithValue("@AuditContactSignature", emergencyAudit.AuditContactSignature);
                                        command.Parameters.AddWithValue("@AuditCreation", emergencyAudit.AuditCreation);

                                        command.ExecuteNonQuery();
                                        command.Parameters.Clear();
                                    }
                                }
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    responseMsg = ex.Message;
                }
            }
            return responseMsg;
        }

    }
}
