﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace JohnstonApi.Controllers
{
    public class GodController : ApiController
    {
        // GET: api/God
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/God/5
        public string Get(string name)
        {
            DataTable dt = new DataTable();
            var sqlCapture = name;
            using (SqlConnection connection = new SqlConnection())
            {
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    da.Fill(dt);
                }
                
            }
            var dataResponse = JsonConvert.SerializeObject(dt, Formatting.Indented);
            return dataResponse;
        }

        // POST: api/God
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/God/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/God/5
        public void Delete(int id)
        {
        }
    }
}
